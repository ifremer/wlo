/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.common.collect.Lists;
import fr.ifremer.tutti.ichtyometer.feed.record.FeedReaderMeasureRecord;
import fr.ifremer.tutti.ichtyometer.feed.record.FeedReaderRecordFactory;
import fr.ifremer.tutti.ichtyometer.feed.record.FeedReaderRecordSupport;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

/**
 * This class does all the work for setting up and managing Bluetooth
 * connections with other devices. It has a thread that listens for
 * incoming connections, a thread for connecting with a device, and a
 * thread for performing data transmissions when connected.
 */
public class BigFinCommunicationService extends Service {

    private static final String TAG = "BigFinCommunicationService";

    // Unique UUID for this application
    private static final UUID MY_UUID =
        UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
    private static final int NOIFICATION_ID = 42;

    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device

    // OUTGOING MESSAGE
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_CONNECTION_LOST = 5;
    public static final int MESSAGE_CONNECTION_FAILED = 6;

    // INCOMING MESSAGE
    public static final int MESSAGE_REGISTER_CLIENT = 1;
    public static final int MESSAGE_UNREGISTER_CLIENT = 2;
    public static final int MESSAGE_CONNECT_DEVICE = 3;
    public static final int MESSAGE_SEND_DATA = 4;
    public static final int MESSAGE_DISCONNECT_DEVICE = 5;

    //BOTH
    public static final int MESSAGE_CONNECTION_STATE = 7;

    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    public static final String DEVICE_ADDRESS = "device_address";
    public static final String DATA_TO_SEND = "dataToSend";
    public static final String EXTRA_DISCONNECT = "disconnect";

    // Member fields
    protected BluetoothAdapter mAdapter;
    protected AcceptThread mAcceptThread;
    protected ConnectThread mConnectThread;
    protected ConnectedThread mConnectedThread;
    protected int mState;
    protected FeedReaderRecordFactory recordFactory;

    // Keeps track of all current registered clients.
    protected List<Messenger> mClients = Lists.newArrayList();
    // Target we publish for clients to send messages to IncomingHandler.
    protected final Messenger mMessenger = new Messenger(new IncomingHandler());

    protected NotificationCompat.Builder notificationBuilder;

    @Override
    public void onCreate() {
        super.onCreate();

        mAdapter = BluetoothAdapter.getDefaultAdapter();

        notificationBuilder = new NotificationCompat.Builder(this);

        recordFactory = new FeedReaderRecordFactory();

        setState(STATE_NONE);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(NOIFICATION_ID, notificationBuilder.build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getBooleanExtra(EXTRA_DISCONNECT, false)) {
            if (mClients.isEmpty()) {
                stopSelf();
            } else {
                stop();
            }
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stop();
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.cancel(NOIFICATION_ID);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // start the service for it to keep living after all activities unbound it
        startService(intent);
        return mMessenger.getBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // stop the service only if it is not connected
        if (mState != STATE_CONNECTED) {
            stopSelf();
        }
        return true;
    }

    protected void updateNotification(boolean connected) {
        int title;
        int text;
        int icon;
        PendingIntent pendingIntent;

        if (connected) {
            title = R.string.bigfin_ichtyometer_connected_title;
            text = R.string.bigfin_ichtyometer_connected_text;
            icon = R.drawable.wlo_ico_small_connected;
            Intent intent = new Intent(this, BigFinCommunicationService.class);
            intent.putExtra(EXTRA_DISCONNECT, true);
            pendingIntent = PendingIntent.getService(this,
                                                      -1,
                                                      intent,
                                                      PendingIntent.FLAG_CANCEL_CURRENT);

        } else {
            title = R.string.bigfin_no_ichtyometer_connected_title;
            text = R.string.bigfin_no_ichtyometer_connected_text;
            icon = R.drawable.wlo_ico_small;
            Intent intent = new Intent(this, DeviceListActivity.class);
            pendingIntent = PendingIntent.getActivity(this,
                                                     -1,
                                                     intent,
                                                     PendingIntent.FLAG_CANCEL_CURRENT);
        }

        notificationBuilder.setContentTitle(getText(title))
                .setSmallIcon(icon)
                .setContentText(getText(text))
                .setContentIntent(pendingIntent);

        startForeground(NOIFICATION_ID, notificationBuilder.build());
    }

    protected void sendMessage(int what, int arg1) {
        sendMessage(what, arg1, -1, null);
    }

    protected void sendMessage(int what, int arg1, int arg2) {
        sendMessage(what, arg1, arg2, null);
    }

    protected void sendMessage(int what, Object obj) {
        sendMessage(what, -1, -1, obj);
    }

    protected void sendMessage(int what, int arg1, int arg2, Object obj) {
        for (Messenger messenger : mClients) {
            try {
                Message message = Message.obtain(null, what, arg1, arg2, obj);
                messenger.send(message);

            } catch (RemoteException e) {
                mClients.remove(messenger);
            }
        }
    }

    protected void sendMessage(int what, String key, String value) {
        for (Messenger messenger : mClients) {
            try {
                Message message = Message.obtain(null, what);
                Bundle bundle = new Bundle();
                bundle.putString(key, value);
                message.setData(bundle);
                messenger.send(message);

            } catch (RemoteException e) {
                mClients.remove(messenger);
            }
        }
    }

    /**
     * Set the current state of the chat connection
     * @param state  An integer defining the current connection state
     */
    protected synchronized void setState(int state) {
        updateNotification(state == STATE_CONNECTED);

        mState = state;
        sendMessage(MESSAGE_STATE_CHANGE, state);
    }

    /**
     * Stop all threads
     */
    protected synchronized void stop() {
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        if (mAcceptThread != null) {
            mAcceptThread.cancel();
            mAcceptThread = null;
        }

        setState(STATE_NONE);

    }

    protected synchronized void reset() {
        stop();

        setState(STATE_LISTEN);

        // Start the thread to listen on a BluetoothServerSocket
        mAcceptThread = new AcceptThread();
        mAcceptThread.start();
    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     * @param device  The BluetoothDevice to connect
     */
    protected synchronized void connect(BluetoothDevice device) {
        // Cancel any thread attempting to make a connection
        if (mState == STATE_CONNECTING) {
            if (mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to connect with the given device
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();
        setState(STATE_CONNECTING);
    }

    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     * @param socket  The BluetoothSocket on which the connection was made
     * @param device  The BluetoothDevice that has been connected
     */
    protected synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {
        // Cancel the thread that completed the connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Cancel the accept thread because we only want to connect to one device
        if (mAcceptThread != null) {
            mAcceptThread.cancel();
            mAcceptThread = null;
        }

        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();

        // Send the name of the connected device back to the UI Activity
        sendMessage(MESSAGE_DEVICE_NAME, DEVICE_NAME, device.getName());

        Log.d(TAG, "conected");
        setState(STATE_CONNECTED);
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     * @param out The bytes to write
     * @see ConnectedThread#write(byte[])
     */
    protected void write(byte[] out) {
        // Create temporary object
        ConnectedThread r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != STATE_CONNECTED) return;
            r = mConnectedThread;
        }
        // Perform the write unsynchronized
        r.write(out);
    }

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    protected void connectionFailed() {
        // Send a failure message back to the Activity
        sendMessage(MESSAGE_CONNECTION_FAILED, TOAST, "Unable to connect device");

        // Start the service over to restart listening mode
        reset();
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    protected void connectionLost() {
        // Send a failure message back to the Activity
        sendMessage(MESSAGE_CONNECTION_LOST, TOAST, "Device connection was lost");

        // Start the service over to restart listening mode
        reset();
    }

    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted
     * (or until cancelled).
     */
    protected class AcceptThread extends Thread {
        // The local server socket
        private BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            BluetoothServerSocket tmp = null;

            // Create a new listening server socket
            try {
                tmp = mAdapter.listenUsingRfcommWithServiceRecord("BigFinBtSocket", MY_UUID);

            } catch (IOException e) {
                Log.e(TAG, "Socket listen() failed", e);
            }
            mmServerSocket = tmp;
        }

        public void run() {
            setName("AcceptThread");

            BluetoothSocket socket = null;

            // Listen to the server socket if we're not connected
            while (mState != STATE_CONNECTED && mmServerSocket != null) {
                try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    Log.e(TAG, "Socket accept() failed");
                    break;
                }

                // If a connection was accepted
                if (socket != null) {
                    synchronized (BigFinCommunicationService.this) {
                        switch (mState) {
                        case STATE_LISTEN:
                        case STATE_CONNECTING:
                            // Situation normal. Start the connected thread.
                            connected(socket, socket.getRemoteDevice());
                            break;
                        case STATE_NONE:
                        case STATE_CONNECTED:
                            // Either not ready or already connected. Terminate new socket.
                            try {
                                socket.close();
                            } catch (IOException e) {
                                Log.e(TAG, "Could not close unwanted socket", e);
                            }
                            break;
                        }
                    }
                }
            }
        }

        public void cancel() {
            try {
                mmServerSocket.close();
                mmServerSocket = null;
            } catch (IOException e) {
                Log.e(TAG, "Socket close() of server failed", e);
            }
        }
    }


    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    protected class ConnectThread extends Thread {
        protected final BluetoothSocket mmSocket;
        protected final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            mmDevice = device;
            BluetoothSocket tmp = null;

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                Log.e(TAG, "Socket create() failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            setName("ConnectThread");

            // Make a connection to the BluetoothSocket
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                mmSocket.connect();

            } catch (IOException e) {
                Log.e(TAG, "Error while connecting", e);
                // Close the socket
                try {
                    mmSocket.close();
                } catch (IOException e2) {
                    Log.e(TAG, "unable to close() socket during connection failure", e2);
                }
                connectionFailed();
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (BigFinCommunicationService.this) {
                mConnectThread = null;
            }

            // Start the connected thread
            connected(mmSocket, mmDevice);
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    protected class ConnectedThread extends Thread {
        protected final BluetoothSocket mmSocket;
        protected final InputStream mmInStream;
        protected final OutputStream mmOutStream;
        protected boolean stop;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "temp sockets not created", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;

            stop = false;
        }

        public void run() {
            // Keep listening to the InputStream while connected
            while (!stop) {
                try {

                    String result = "";

                    // wait until got a #

                    boolean complete = false;
                    while (!complete) {
                        while (mmInStream.available() > 0) {

                            if (complete || stop) {
                                break;
                            }

                            int c = mmInStream.read();

                            result += (char) c;

                            if ('#' == (char) c) {

                                complete = true;
                            }
                        }
                    }

                    FeedReaderRecordSupport readerRecord = null;
                    if (!stop) {

                        Log.i(TAG, "New raw record: " + result);
                        readerRecord = recordFactory.newRecord(result);
                        Log.i(TAG, "New feed record: " + readerRecord);
                    }

                    if (!stop && readerRecord != null && readerRecord instanceof FeedReaderMeasureRecord) {

                        // Send the obtained bytes to the UI Activity
                        sendMessage(MESSAGE_READ, readerRecord);
                    }


                } catch (IOException e) {
                    Log.e(TAG, "disconnected");
                    connectionLost();
                }
            }
        }

        /**
         * Write to the connected OutStream.
         * @param buffer  The bytes to write
         */
        public void write(byte[] buffer) {
            try {
                mmOutStream.write(buffer);

                // Share the sent message back to the UI Activity
                sendMessage(MESSAGE_WRITE, buffer);

            } catch (IOException e) {
                Log.e(TAG, "Exception during write", e);
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
            stop = true;
        }
    }

    // Handler of incoming messages from clients.
    protected class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_REGISTER_CLIENT:
                    mClients.add(msg.replyTo);
                    break;

                case MESSAGE_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;

                case MESSAGE_CONNECT_DEVICE:
                    reset();

                    String deviceAddress = msg.getData().getString(DEVICE_ADDRESS);
                    // Get the BluetoothDevice object
                    BluetoothDevice device = mAdapter.getRemoteDevice(deviceAddress);
                    connect(device);
                    break;

                case MESSAGE_SEND_DATA:
                    String dataToSend = msg.getData().getString(DATA_TO_SEND);
                    write(dataToSend.getBytes());
                    break;

                case MESSAGE_DISCONNECT_DEVICE:
                    stop();
                    break;

                case MESSAGE_CONNECTION_STATE:
                    BigFinCommunicationService.this.sendMessage(MESSAGE_CONNECTION_STATE, mState);

                default:
                    super.handleMessage(msg);
            }
        }
    }
}

package fr.ifremer.wlo.storage;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.Context;
import android.database.Cursor;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.models.categorization.CategoryModel;
import fr.ifremer.wlo.models.categorization.QualitativeValueModel;
import fr.ifremer.wlo.models.referentials.CommercialSpecies;
import fr.ifremer.wlo.models.referentials.HasCode;
import fr.ifremer.wlo.models.referentials.Location;
import fr.ifremer.wlo.models.referentials.Mensuration;
import fr.ifremer.wlo.models.referentials.Metier;
import fr.ifremer.wlo.models.referentials.Presentation;
import fr.ifremer.wlo.models.referentials.ScientificSpecies;
import fr.ifremer.wlo.models.referentials.State;
import fr.ifremer.wlo.models.referentials.Vessel;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class DataCache {

    public static final String DEFAULT_MENSURATION_CODE = "LT";

    protected static Map<String, CommercialSpecies> commercialSpecies;
    protected static Map<String, Location> locations;
    protected static Map<String, Mensuration> mensurations;
    protected static Mensuration defaultMensuration;
    protected static Map<String, Metier> metiers;
    protected static Map<String, Presentation> presentations;
    protected static Map<String, ScientificSpecies> scientificSpecies;
    protected static Map<String, State> states;
    protected static Map<String, Vessel> vessels;
    protected static Map<String, CategoryModel> categories;
    protected static Set<String> sortCategories;

    public static Collection<CommercialSpecies> getAllCommercialSpecies(Context context) {
        initCommercialSpecies(context);
        List<CommercialSpecies> result = Lists.newArrayList(commercialSpecies.values());
        Collections.sort(result, HasCode.GET_CODE_COMPARATOR);
        return result;
    }

    public static CommercialSpecies getCommercialSpeciesById(Context context, String id) {
        initCommercialSpecies(context);
        return commercialSpecies.get(id);
    }

    public static void invalidateCommercialSpecies() {
        commercialSpecies = null;
    }

    public static Collection<Location> getAllLocations(Context context) {
        initLocations(context);
        List<Location> result = Lists.newArrayList(locations.values());
        Collections.sort(result, HasCode.GET_CODE_COMPARATOR);
        return result;
    }

    public static Location getLocationById(Context context, String id) {
        initLocations(context);
        return locations.get(id);
    }

    public static void invalidateLocations() {
        locations = null;
    }

    public static Collection<Mensuration> getAllMensurations(Context context) {
        initMensurations(context);
        List<Mensuration> result = Lists.newArrayList(mensurations.values());
        Collections.sort(result, HasCode.GET_CODE_COMPARATOR);
        return result;
    }

    public static Mensuration getMensurationById(Context context, String id) {
        initMensurations(context);
        return mensurations.get(id);
    }

    public static Mensuration getDefaultMensuration(Context context) {
        if (defaultMensuration == null) {
            initMensurations(context);
            Map<String, Mensuration> mensurationsByCode = Maps.uniqueIndex(mensurations.values(), HasCode.GET_CODE_FUNCTION);
            defaultMensuration = mensurationsByCode.get(DEFAULT_MENSURATION_CODE);
        }
        return defaultMensuration;
    }

    public static void invalidateMensurations() {
        mensurations = null;
    }

    public static Collection<Metier> getAllMetiers(Context context) {
        initMetiers(context);
        List<Metier> result = Lists.newArrayList(metiers.values());
        Collections.sort(result, HasCode.GET_CODE_COMPARATOR);
        return result;
    }

    public static Metier getMetierById(Context context, String id) {
        initMetiers(context);
        return metiers.get(id);
    }

    public static void invalidateMetiers() {
        metiers = null;
    }

    public static Collection<Presentation> getAllPresentations(Context context) {
        initPresentations(context);
        List<Presentation> result = Lists.newArrayList(presentations.values());
        Collections.sort(result, HasCode.GET_CODE_COMPARATOR);
        return result;
    }

    public static Presentation getPresentationById(Context context, String id) {
        initPresentations(context);
        return presentations.get(id);
    }

    public static void invalidatePresentations() {
        presentations = null;
    }

    public static Collection<ScientificSpecies> getAllScientificSpecies(Context context) {
        initScientificSpecies(context);
        List<ScientificSpecies> result = Lists.newArrayList(scientificSpecies.values());
        Collections.sort(result, HasCode.GET_CODE_COMPARATOR);
        return result;
    }

    public static ScientificSpecies getScientificSpeciesById(Context context, String id) {
        initScientificSpecies(context);
        return scientificSpecies.get(id);
    }

    public static void invalidateScientificSpecies() {
        scientificSpecies = null;
    }

    public static Set<String> getAllSortCategories(Context context) {
        initSortCategories(context);
        return sortCategories;
    }

    public static void invalidateSortCategories() {
        sortCategories = null;
    }

    public static Collection<State> getAllStates(Context context) {
        initStates(context);
        List<State> result = Lists.newArrayList(states.values());
        Collections.sort(result, HasCode.GET_CODE_COMPARATOR);
        return result;
    }

    public static State getStateById(Context context, String id) {
        initStates(context);
        return states.get(id);
    }

    public static void invalidateStates() {
        states = null;
    }

    public static Collection<Vessel> getAllVessels(Context context) {
        initVessels(context);
        List<Vessel> result = Lists.newArrayList(vessels.values());
        Collections.sort(result, HasCode.GET_CODE_COMPARATOR);
        return result;
    }

    public static Vessel getVesselById(Context context, String id) {
        initVessels(context);
        return vessels.get(id);
    }

    public static void invalidateVessels() {
        vessels = null;
    }

    public static Collection<CategoryModel> getAllCategories(Context context) {
        initCategories(context);
        List<CategoryModel> result = Lists.newArrayList(categories.values());
        return result;
    }

    public static CategoryModel getCategoryById(Context context, String id) {
        initCategories(context);
        CategoryModel result = categories.get(id);
        if (result != null && result.getQualitativeValues() == null) {
            WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
            Cursor cursor = soh.getAllQualitativeValues(id);
            List<QualitativeValueModel> qualitativeValues =
                    WloSqlOpenHelper.transformCursorIntoCollection(cursor, new Function<Cursor, QualitativeValueModel>() {
                        @Override
                        public QualitativeValueModel apply(Cursor input) {
                            return new QualitativeValueModel(input);
                        }
                    });
            result.setQualitativeValues(qualitativeValues);
            soh.close();
        }
        return result;
    }

    public static void addCategory(CategoryModel categoryModel) {
        Preconditions.checkNotNull(categoryModel);
        categories.put(categoryModel.getId(), categoryModel);
    }

    protected static void initCommercialSpecies(Context context) {
        if (commercialSpecies == null) {
            WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
            Cursor cursor = soh.getAllRefCommercialSpecies();
            List<CommercialSpecies> data = WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                                          new Function<Cursor, CommercialSpecies>() {
                                                                                              @Override
                                                                                              public CommercialSpecies apply(Cursor input) {
                                                                                                  return new CommercialSpecies(input);
                                                                                              }
                                                                                          });

            commercialSpecies = Maps.uniqueIndex(data, BaseModel.GET_ID_FUNCTION);
            soh.close();
        }
    }

    protected static void initLocations(Context context) {
        if (locations == null) {
            WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
            Cursor cursor = soh.getAllRefLocations();
            List<Location> data = WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                                 new Function<Cursor, Location>() {
                                                                                     @Override
                                                                                     public Location apply(Cursor input) {
                                                                                         return new Location(input);
                                                                                     }
                                                                                 });
            locations = Maps.uniqueIndex(data, BaseModel.GET_ID_FUNCTION);
            soh.close();
        }
    }

    protected static void initMensurations(Context context) {
        if (mensurations == null) {
            WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
            Cursor cursor = soh.getAllRefMensurations();
            List<Mensuration> data = WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                                    new Function<Cursor, Mensuration>() {
                                                                                        @Override
                                                                                        public Mensuration apply(Cursor input) {
                                                                                            return new Mensuration(input);
                                                                                        }
                                                                                    });

            mensurations = Maps.uniqueIndex(data, BaseModel.GET_ID_FUNCTION);
            soh.close();
        }
    }

    protected static void initMetiers(Context context) {
        if (metiers == null) {
            WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
            Cursor cursor = soh.getAllRefMetiers();
            List<Metier> data = WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                               new Function<Cursor, Metier>() {
                                                                                   @Override
                                                                                   public Metier apply(Cursor input) {
                                                                                       return new Metier(input);
                                                                                   }
                                                                               });

            metiers = Maps.uniqueIndex(data, BaseModel.GET_ID_FUNCTION);
            soh.close();
        }
    }

    protected static void initPresentations(Context context) {
        if (presentations == null) {
            WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
            Cursor cursor = soh.getAllRefPresentations();
            List<Presentation> data = WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                                     new Function<Cursor, Presentation>() {
                                                                                         @Override
                                                                                         public Presentation apply(Cursor input) {
                                                                                             return new Presentation(input);
                                                                                         }
                                                                                     });

            presentations = Maps.uniqueIndex(data, BaseModel.GET_ID_FUNCTION);
            soh.close();
        }
    }

    protected static void initScientificSpecies(Context context) {
        if (scientificSpecies == null) {
            WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
            Cursor cursor = soh.getAllRefScientificSpecies();
            List<ScientificSpecies> data = WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                                          new Function<Cursor, ScientificSpecies>() {
                                                                                              @Override
                                                                                              public ScientificSpecies apply(Cursor input) {
                                                                                                  return new ScientificSpecies(input);
                                                                                              }
                                                                                          });

            scientificSpecies = Maps.uniqueIndex(data, BaseModel.GET_ID_FUNCTION);
            soh.close();
        }
    }

    protected static void initStates(Context context) {
        if (states == null) {
            WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
            Cursor cursor = soh.getAllRefStates();
            List<State> data = WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                              new Function<Cursor, State>() {
                                                                                  @Override
                                                                                  public State apply(Cursor input) {
                                                                                      return new State(input);
                                                                                  }
                                                                              });

            states = Maps.uniqueIndex(data, BaseModel.GET_ID_FUNCTION);
            soh.close();
        }
    }

    protected static void initVessels(Context context) {
        if (vessels == null) {
            WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
            Cursor cursor = soh.getAllRefVessels();
            List<Vessel> data = WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                               new Function<Cursor, Vessel>() {
                                                                                   @Override
                                                                                   public Vessel apply(Cursor input) {
                                                                                       return new Vessel(input);
                                                                                   }
                                                                               });

            vessels = Maps.uniqueIndex(data, BaseModel.GET_ID_FUNCTION);
            soh.close();
        }
    }

    protected static void initCategories(Context context) {
        if (categories == null) {
            WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
            Cursor cursor = soh.getAllCategories();
            List<CategoryModel> data = WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                                       new Function<Cursor, CategoryModel>() {
                                                                                           @Override
                                                                                           public CategoryModel apply(Cursor input) {
                                                                                               return new CategoryModel(input);
                                                                                           }
                                                                                       });

            categories = new HashMap<>(Maps.uniqueIndex(data, BaseModel.GET_ID_FUNCTION));
            soh.close();
        }
    }

    protected static void initSortCategories(Context context) {
        if (sortCategories == null) {
            WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
            Cursor cursor = soh.getAllSortCategories();
            List<String> sc = WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                             new Function<Cursor, String>() {
                                                                                 @Override
                                                                                 public String apply(Cursor cursor) {
                                                                                     return cursor.getString(0);
                                                                                 }
                                                                             });
            sortCategories = Sets.newHashSet(sc);
            soh.close();
        }
    }

}

package fr.ifremer.wlo.storage;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.models.CategoryWeightModel;
import fr.ifremer.wlo.models.CommercialSpeciesModel;
import fr.ifremer.wlo.models.ContextModel;
import fr.ifremer.wlo.models.LocationModel;
import fr.ifremer.wlo.models.MeasurementModel;
import fr.ifremer.wlo.models.MetierModel;
import fr.ifremer.wlo.models.ScientificSpeciesModel;
import fr.ifremer.wlo.models.VesselModel;
import fr.ifremer.wlo.models.categorization.CategoryModel;
import fr.ifremer.wlo.models.categorization.QualitativeValueModel;
import fr.ifremer.wlo.models.referentials.CalcifiedPartTaking;
import fr.ifremer.wlo.models.referentials.CommercialSpecies;
import fr.ifremer.wlo.models.referentials.Location;
import fr.ifremer.wlo.models.referentials.Mensuration;
import fr.ifremer.wlo.models.referentials.Metier;
import fr.ifremer.wlo.models.referentials.Presentation;
import fr.ifremer.wlo.models.referentials.ScientificSpecies;
import fr.ifremer.wlo.models.referentials.State;
import fr.ifremer.wlo.models.referentials.Vessel;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class WloSqlOpenHelper extends SQLiteOpenHelper {

    private static final String TAG = "WloOpenHelper";

    public static final String DATABASE_NAME = "wlo.db";
    // if you change the version, do not forget to migrate the data in the onUpgrade method
    public static final int DATABASE_VERSION = 19;

    public static final String TEXT_TYPE = " TEXT";
    public static final String INTEGER_TYPE = " INTEGER";
    public static final String NUMERIC_TYPE = " NUMERIC";
    public static final String REAL_TYPE = " REAL";
    public static final String COMMA_SEP = ",";
    public static final String NOT_NULL = " NOT NULL";

    //CONTEXT
    protected static final String SQL_CREATE_CONTEXTS =
            "CREATE TABLE " + ContextModel.TABLE_NAME + " (" +
                    ContextModel._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    ContextModel.COLUMN_NAME + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    ContextModel.COLUMN_COMMENT + TEXT_TYPE +
                    " )";

    protected static final String SQL_DELETE_CONTEXTS =
            "DROP TABLE IF EXISTS " + ContextModel.TABLE_NAME;


    //LOCATION
    protected static final String SQL_CREATE_LOCATIONS =
            "CREATE TABLE " + LocationModel.TABLE_NAME + " (" +
                    LocationModel._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    LocationModel.COLUMN_OPERATOR + TEXT_TYPE + COMMA_SEP +
                    LocationModel.COLUMN_START_DATE + INTEGER_TYPE + COMMA_SEP +
                    LocationModel.COLUMN_END_DATE + INTEGER_TYPE + COMMA_SEP +
                    LocationModel.COLUMN_LOCATION + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    LocationModel.COLUMN_COMMENT + TEXT_TYPE + COMMA_SEP +
                    LocationModel.COLUMN_CONTEXT_ID + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    "FOREIGN KEY(" + LocationModel.COLUMN_CONTEXT_ID + ") REFERENCES " +
                        ContextModel.TABLE_NAME + "(" + ContextModel._ID + ") ON DELETE CASCADE" + COMMA_SEP +
                    "FOREIGN KEY(" + LocationModel.COLUMN_LOCATION + ") REFERENCES " +
                        Location.TABLE_NAME + "(" + Location._ID + ")" +
                    " )";

    protected static final String SQL_DELETE_LOCATIONS =
            "DROP TABLE IF EXISTS " + LocationModel.TABLE_NAME;

    // VESSEL
    protected static final String SQL_CREATE_VESSELS =
            "CREATE TABLE " + VesselModel.TABLE_NAME + " (" +
                    VesselModel._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    VesselModel.COLUMN_REGISTRATION_NUMBER + TEXT_TYPE + COMMA_SEP +
                    VesselModel.COLUMN_NAME + TEXT_TYPE + COMMA_SEP +
                    VesselModel.COLUMN_LANDING_DATE + INTEGER_TYPE + COMMA_SEP +
                    VesselModel.COLUMN_LANDING_LOCATION + TEXT_TYPE + COMMA_SEP +
                    VesselModel.COLUMN_COMMENT + TEXT_TYPE + COMMA_SEP +
                    VesselModel.COLUMN_LOCATION_ID + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    "FOREIGN KEY(" + VesselModel.COLUMN_LOCATION_ID + ") REFERENCES " +
                        LocationModel.TABLE_NAME + "(" + LocationModel._ID + ") ON DELETE CASCADE" + COMMA_SEP +
                    "FOREIGN KEY(" + VesselModel.COLUMN_LANDING_LOCATION + ") REFERENCES " +
                        Location.TABLE_NAME + "(" + Location._ID + ")" +
                    " )";

    protected static final String SQL_DELETE_VESSELS =
            "DROP TABLE IF EXISTS " + VesselModel.TABLE_NAME;

    // METIER
    protected static final String SQL_CREATE_METIERS =
            "CREATE TABLE " + MetierModel.TABLE_NAME + " (" +
                    MetierModel._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    MetierModel.COLUMN_GEAR_SPECIES + TEXT_TYPE + COMMA_SEP +
                    MetierModel.COLUMN_ZONE + TEXT_TYPE + COMMA_SEP +
                    MetierModel.COLUMN_SAMPLE_ROW_CODE + TEXT_TYPE + COMMA_SEP +
                    MetierModel.COLUMN_COMMENT + TEXT_TYPE + COMMA_SEP +
                    MetierModel.COLUMN_VESSEL_ID + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    "FOREIGN KEY(" + MetierModel.COLUMN_VESSEL_ID + ") REFERENCES " +
                        VesselModel.TABLE_NAME + "(" + VesselModel._ID + ") ON DELETE CASCADE" + COMMA_SEP +
                    "FOREIGN KEY(" + MetierModel.COLUMN_GEAR_SPECIES + ") REFERENCES " +
                        Metier.TABLE_NAME + "(" + Metier._ID + ")" +
                    " )";

    protected static final String SQL_DELETE_METIERS =
            "DROP TABLE IF EXISTS " + MetierModel.TABLE_NAME;

    // COMMERCIAL SPECIES
    protected static final String SQL_CREATE_COMMERCIAL_SPECIES =
            "CREATE TABLE " + CommercialSpeciesModel.TABLE_NAME + " (" +
                    CommercialSpeciesModel._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    CommercialSpeciesModel.COLUMN_FAO_CODE + TEXT_TYPE + COMMA_SEP +
                    CommercialSpeciesModel.COLUMN_MEASUREMENT_METHOD + TEXT_TYPE + COMMA_SEP +
                    CommercialSpeciesModel.COLUMN_PRECISION + TEXT_TYPE + COMMA_SEP +
                    CommercialSpeciesModel.COLUMN_SPECIES_MIX + NUMERIC_TYPE + COMMA_SEP +
                    CommercialSpeciesModel.COLUMN_SORT_CATEGORY + TEXT_TYPE + COMMA_SEP +
                    CommercialSpeciesModel.COLUMN_STATE + TEXT_TYPE + COMMA_SEP +
                    CommercialSpeciesModel.COLUMN_PRESENTATION + TEXT_TYPE + COMMA_SEP +
                    CommercialSpeciesModel.COLUMN_CATEGORY1 + TEXT_TYPE + COMMA_SEP +
                    CommercialSpeciesModel.COLUMN_CATEGORY2 + TEXT_TYPE + COMMA_SEP +
                    CommercialSpeciesModel.COLUMN_CATEGORY3 + TEXT_TYPE + COMMA_SEP +
                    CommercialSpeciesModel.COLUMN_COMMENT + TEXT_TYPE + COMMA_SEP +
                    CommercialSpeciesModel.COLUMN_TOTAL_UNLOADED_WEIGHT + INTEGER_TYPE + COMMA_SEP +
                    CommercialSpeciesModel.COLUMN_METIER_ID + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    "FOREIGN KEY(" + CommercialSpeciesModel.COLUMN_METIER_ID + ") REFERENCES " +
                        MetierModel.TABLE_NAME + "(" + MetierModel._ID + ") ON DELETE CASCADE" + COMMA_SEP +
                    "FOREIGN KEY(" + CommercialSpeciesModel.COLUMN_FAO_CODE + ") REFERENCES " +
                        CommercialSpecies.TABLE_NAME + "(" + CommercialSpecies._ID + ")" + COMMA_SEP +
                    "FOREIGN KEY(" + CommercialSpeciesModel.COLUMN_MEASUREMENT_METHOD + ") REFERENCES " +
                        Mensuration.TABLE_NAME + "(" + Mensuration._ID + ")" + COMMA_SEP +
                    "FOREIGN KEY(" + CommercialSpeciesModel.COLUMN_STATE + ") REFERENCES " +
                        State.TABLE_NAME + "(" + State._ID + ")" + COMMA_SEP +
                    "FOREIGN KEY(" + CommercialSpeciesModel.COLUMN_PRESENTATION + ") REFERENCES " +
                        Presentation.TABLE_NAME + "(" + Presentation._ID + ")" + COMMA_SEP +
                    "FOREIGN KEY(" + CommercialSpeciesModel.COLUMN_CATEGORY1 + ") REFERENCES " +
                        CategoryModel.TABLE_NAME + "(" + CategoryModel._ID + ")" + COMMA_SEP +
                    "FOREIGN KEY(" + CommercialSpeciesModel.COLUMN_CATEGORY2 + ") REFERENCES " +
                        CategoryModel.TABLE_NAME + "(" + CategoryModel._ID + ")" + COMMA_SEP +
                    "FOREIGN KEY(" + CommercialSpeciesModel.COLUMN_CATEGORY3 + ") REFERENCES " +
                        CategoryModel.TABLE_NAME + "(" + CategoryModel._ID + ")" +
                    " )";

    protected static final String SQL_DELETE_COMMERCIAL_SPECIES =
            "DROP TABLE IF EXISTS " + CommercialSpeciesModel.TABLE_NAME;

    // SCIENTIFIC SPECIES
    protected static final String SQL_CREATE_SCIENTIFIC_SPECIES =
            "CREATE TABLE " + ScientificSpeciesModel.TABLE_NAME + " (" +
                    ScientificSpeciesModel._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    ScientificSpeciesModel.COLUMN_NAME + TEXT_TYPE + COMMA_SEP +
                    ScientificSpeciesModel.COLUMN_TAKING_ACTIVATION + NUMERIC_TYPE + COMMA_SEP +
                    ScientificSpeciesModel.COLUMN_COMMENT + TEXT_TYPE + COMMA_SEP +
                    ScientificSpeciesModel.COLUMN_SORTED_WEIGHT + INTEGER_TYPE + COMMA_SEP +
                    ScientificSpeciesModel.COLUMN_SAMPLE_WEIGHT + INTEGER_TYPE + COMMA_SEP +
                    ScientificSpeciesModel.COLUMN_COMMERCIAL_SPECIES_ID + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    "FOREIGN KEY(" + ScientificSpeciesModel.COLUMN_COMMERCIAL_SPECIES_ID + ") REFERENCES " +
                        CommercialSpeciesModel.TABLE_NAME + "(" + CommercialSpeciesModel._ID + ") ON DELETE CASCADE" + COMMA_SEP +
                    "FOREIGN KEY(" + ScientificSpeciesModel.COLUMN_NAME + ") REFERENCES " +
                        ScientificSpecies.TABLE_NAME + "(" + ScientificSpecies._ID + ")" +
                    " )";

    protected static final String SQL_DELETE_SCIENTIFIC_SPECIES =
            "DROP TABLE IF EXISTS " + ScientificSpeciesModel.TABLE_NAME;

    // MEASUREMENTS
    protected static final String SQL_CREATE_MEASUREMENTS =
            "CREATE TABLE " + MeasurementModel.TABLE_NAME + " (" +
                    MeasurementModel._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    MeasurementModel.COLUMN_SIZE + INTEGER_TYPE + COMMA_SEP +
                    MeasurementModel.COLUMN_DATE + INTEGER_TYPE + COMMA_SEP +
                    MeasurementModel.COLUMN_CATEGORY_1 + TEXT_TYPE + COMMA_SEP +
                    MeasurementModel.COLUMN_CATEGORY_2 + TEXT_TYPE + COMMA_SEP +
                    MeasurementModel.COLUMN_CATEGORY_3 + TEXT_TYPE + COMMA_SEP +
                    MeasurementModel.COLUMN_SCIENTIFIC_SPECIES_ID + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    "FOREIGN KEY(" + MeasurementModel.COLUMN_SCIENTIFIC_SPECIES_ID + ") REFERENCES " +
                        ScientificSpeciesModel.TABLE_NAME + "(" + ScientificSpeciesModel._ID + ") ON DELETE CASCADE" +
                    " )";

    protected static final String SQL_DELETE_MEASUREMENTS =
            "DROP TABLE IF EXISTS " + MeasurementModel.TABLE_NAME;

    // CATEGORY WEIGHTS
    protected static final String SQL_CREATE_CATEGORY_WEIGHTS =
            "CREATE TABLE " + CategoryWeightModel.TABLE_NAME + " (" +
                    CategoryWeightModel._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    CategoryWeightModel.COLUMN_CATEGORY_1 + TEXT_TYPE + COMMA_SEP +
                    CategoryWeightModel.COLUMN_CATEGORY_2 + TEXT_TYPE + COMMA_SEP +
                    CategoryWeightModel.COLUMN_CATEGORY_3 + TEXT_TYPE + COMMA_SEP +
                    CategoryWeightModel.COLUMN_WEIGHT + INTEGER_TYPE + COMMA_SEP +
                    CategoryWeightModel.COLUMN_SCIENTIFIC_SPECIES_ID + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    "FOREIGN KEY(" + CategoryWeightModel.COLUMN_SCIENTIFIC_SPECIES_ID + ") REFERENCES " +
                    ScientificSpeciesModel.TABLE_NAME + "(" + ScientificSpeciesModel._ID + ") ON DELETE CASCADE" +
                    " )";

    protected static final String SQL_DELETE_CATEGORY_WEIGHTS =
            "DROP TABLE IF EXISTS " + CategoryWeightModel.TABLE_NAME;

    // CALCIFIED PART TAKING
    protected static final String SQL_CREATE_CALCIFIED_PART_TAKINGS =
            "CREATE TABLE " + CalcifiedPartTaking.TABLE_NAME + " (" +
                    CalcifiedPartTaking._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    CalcifiedPartTaking.COLUMN_START_SIZE + INTEGER_TYPE + COMMA_SEP +
                    CalcifiedPartTaking.COLUMN_END_SIZE + INTEGER_TYPE + COMMA_SEP +
                    CalcifiedPartTaking.COLUMN_SIZE_STEP + INTEGER_TYPE + COMMA_SEP +
                    CalcifiedPartTaking.COLUMN_STEP + INTEGER_TYPE + COMMA_SEP +
                    CalcifiedPartTaking.COLUMN_STOP + INTEGER_TYPE + COMMA_SEP +
                    CalcifiedPartTaking.COLUMN_SCIENTIFIC_SPECIES_ID + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    "FOREIGN KEY(" + CalcifiedPartTaking.COLUMN_SCIENTIFIC_SPECIES_ID + ") REFERENCES " +
                    ScientificSpecies.TABLE_NAME + "(" + ScientificSpecies._ID + ") ON DELETE CASCADE" +
                    " )";

    protected static final String SQL_DELETE_CALCIFIED_PART_TAKINGS =
            "DROP TABLE IF EXISTS " + CalcifiedPartTaking.TABLE_NAME;

    // CATEGORIES
    protected static final String SQL_CREATE_CATEGORIES =
            "CREATE TABLE " + CategoryModel.TABLE_NAME + " (" +
                    CategoryModel._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    CategoryModel.COLUMN_LABEL + TEXT_TYPE + NOT_NULL +
                    " )";

    protected static final String SQL_DELETE_CATEGORIES =
            "DROP TABLE IF EXISTS " + CategoryModel.TABLE_NAME;

    protected static final String SQL_CREATE_QUALITATIVE_VALUES =
            "CREATE TABLE " + QualitativeValueModel.TABLE_NAME + " (" +
                    QualitativeValueModel._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    QualitativeValueModel.COLUMN_VALUE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    QualitativeValueModel.COLUMN_LABEL + TEXT_TYPE + COMMA_SEP +
                    QualitativeValueModel.COLUMN_CATEGORY_ID + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                            "FOREIGN KEY(" + QualitativeValueModel.COLUMN_CATEGORY_ID + ") REFERENCES " +
                    CategoryModel.TABLE_NAME + "(" + CategoryModel._ID + ") ON DELETE CASCADE" +
            " )";

    protected static final String SQL_DELETE_QUALITATIVE_VALUES =
            "DROP TABLE IF EXISTS " + QualitativeValueModel.TABLE_NAME;

    // Referentials

    // Commercial Species
    protected static final String SQL_CREATE_REF_COMMERCIAL_SPECIES =
            "CREATE TABLE " + CommercialSpecies.TABLE_NAME + " (" +
                    CommercialSpecies._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    CommercialSpecies.COLUMN_CODE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    CommercialSpecies.COLUMN_ISSCAP + TEXT_TYPE + COMMA_SEP +
                    CommercialSpecies.COLUMN_TAXON_CODE + TEXT_TYPE + COMMA_SEP +
                    CommercialSpecies.COLUMN_SCIENTIFIC_LABEL + TEXT_TYPE + COMMA_SEP +
                    CommercialSpecies.COLUMN_FRENCH_LABEL + TEXT_TYPE + COMMA_SEP +
                    CommercialSpecies.COLUMN_FAMILY + TEXT_TYPE + COMMA_SEP +
                    CommercialSpecies.COLUMN_SPECIES_ORDER + TEXT_TYPE + COMMA_SEP +
                    CommercialSpecies.COLUMN_ACTIVE + NUMERIC_TYPE +
                    " )";

    protected static final String SQL_DELETE_REF_COMMERCIAL_SPECIES =
            "DROP TABLE IF EXISTS " + CommercialSpecies.TABLE_NAME;

    // Locations
    protected static final String SQL_CREATE_REF_LOCATIONS =
        "CREATE TABLE " + Location.TABLE_NAME + " (" +
                Location._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                Location.COLUMN_TYPE_LABEL + TEXT_TYPE + COMMA_SEP +
                Location.COLUMN_CODE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                Location.COLUMN_LABEL + TEXT_TYPE +
                " )";

    protected static final String SQL_DELETE_REF_LOCATIONS =
            "DROP TABLE IF EXISTS " + Location.TABLE_NAME;

    // Mensurations
    protected static final String SQL_CREATE_REF_MENSURATIONS  =
            "CREATE TABLE " + Mensuration.TABLE_NAME + " (" +
                    Mensuration._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    Mensuration.COLUMN_CODE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    Mensuration.COLUMN_LABEL + TEXT_TYPE +
                    " )";

    protected static final String SQL_DELETE_REF_MENSURATIONS =
            "DROP TABLE IF EXISTS " + Mensuration.TABLE_NAME;

    // Metiers
    protected static final String SQL_CREATE_REF_METIERS =
            "CREATE TABLE " + Metier.TABLE_NAME + " (" +
                    Metier._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    Metier.COLUMN_ID + TEXT_TYPE + COMMA_SEP +
                    Metier.COLUMN_CODE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    Metier.COLUMN_LABEL + TEXT_TYPE + COMMA_SEP +
                    Metier.COLUMN_GEAR_CODE + TEXT_TYPE + COMMA_SEP +
                    Metier.COLUMN_GEAR_LABEL + TEXT_TYPE + COMMA_SEP +
                    Metier.COLUMN_SPECIES_CODE + TEXT_TYPE + COMMA_SEP +
                    Metier.COLUMN_SPECIES_LABEL + TEXT_TYPE + COMMA_SEP +
                    Metier.COLUMN_FISHING + NUMERIC_TYPE + COMMA_SEP +
                    Metier.COLUMN_ACTIVE + NUMERIC_TYPE +
                    " )";

    protected static final String SQL_DELETE_REF_METIERS =
            "DROP TABLE IF EXISTS " + Metier.TABLE_NAME;

    // Presentations
    protected static final String SQL_CREATE_REF_PRESENTATIONS =
            "CREATE TABLE " + Presentation.TABLE_NAME + " (" +
                    Presentation._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    Presentation.COLUMN_CODE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    Presentation.COLUMN_LABEL + TEXT_TYPE +
                    " )";

    protected static final String SQL_DELETE_REF_PRESENTATIONS =
            "DROP TABLE IF EXISTS " + Presentation.TABLE_NAME;

    // Scientific species
    protected static final String SQL_CREATE_REF_SCIENTIFIC_SPECIES =
            "CREATE TABLE " + ScientificSpecies.TABLE_NAME + " (" +
                    ScientificSpecies._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    ScientificSpecies.COLUMN_PERM_CODE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    ScientificSpecies.COLUMN_CODE + TEXT_TYPE + COMMA_SEP +
                    ScientificSpecies.COLUMN_LABEL + TEXT_TYPE +
                    " )";

    protected static final String SQL_DELETE_REF_SCIENTIFIC_SPECIES =
            "DROP TABLE IF EXISTS " + ScientificSpecies.TABLE_NAME;

    // States
    protected static final String SQL_CREATE_REF_STATES =
            "CREATE TABLE " + State.TABLE_NAME + " (" +
                    State._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    State.COLUMN_CODE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    State.COLUMN_LABEL + TEXT_TYPE +
                    " )";

    protected static final String SQL_DELETE_REF_STATES =
            "DROP TABLE IF EXISTS " + State.TABLE_NAME;

    // Vessels
    protected static final String SQL_CREATE_REF_VESSELS =
            "CREATE TABLE " + Vessel.TABLE_NAME + " (" +
                    Vessel._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    Vessel.COLUMN_CODE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    Vessel.COLUMN_NAME + TEXT_TYPE + COMMA_SEP +
                    Vessel.COLUMN_QUARTER_CODE + TEXT_TYPE +
                    " )";

    protected static final String SQL_DELETE_REF_VESSELS =
            "DROP TABLE IF EXISTS " + Vessel.TABLE_NAME;


    public WloSqlOpenHelper(android.content.Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // referentials
        db.execSQL(SQL_CREATE_REF_COMMERCIAL_SPECIES);
        db.execSQL(SQL_CREATE_REF_LOCATIONS);
        db.execSQL(SQL_CREATE_REF_MENSURATIONS);
        db.execSQL(SQL_CREATE_REF_METIERS);
        db.execSQL(SQL_CREATE_REF_PRESENTATIONS);
        db.execSQL(SQL_CREATE_REF_SCIENTIFIC_SPECIES);
        db.execSQL(SQL_CREATE_REF_STATES);
        db.execSQL(SQL_CREATE_REF_VESSELS);

        db.execSQL(SQL_CREATE_CATEGORIES);
        db.execSQL(SQL_CREATE_QUALITATIVE_VALUES);

        // models
        db.execSQL(SQL_CREATE_CONTEXTS);
        db.execSQL(SQL_CREATE_LOCATIONS);
        db.execSQL(SQL_CREATE_VESSELS);
        db.execSQL(SQL_CREATE_METIERS);
        db.execSQL(SQL_CREATE_COMMERCIAL_SPECIES);
        db.execSQL(SQL_CREATE_SCIENTIFIC_SPECIES);
        db.execSQL(SQL_CREATE_MEASUREMENTS);
        db.execSQL(SQL_CREATE_CATEGORY_WEIGHTS);
        db.execSQL(SQL_CREATE_CALCIFIED_PART_TAKINGS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // models
        db.execSQL(SQL_DELETE_CALCIFIED_PART_TAKINGS);
        db.execSQL(SQL_DELETE_CATEGORY_WEIGHTS);
        db.execSQL(SQL_DELETE_MEASUREMENTS);
        db.execSQL(SQL_DELETE_SCIENTIFIC_SPECIES);
        db.execSQL(SQL_DELETE_COMMERCIAL_SPECIES);
        db.execSQL(SQL_DELETE_METIERS);
        db.execSQL(SQL_DELETE_VESSELS);
        db.execSQL(SQL_DELETE_LOCATIONS);
        db.execSQL(SQL_DELETE_CONTEXTS);

        db.execSQL(SQL_DELETE_CATEGORIES);
        db.execSQL(SQL_DELETE_QUALITATIVE_VALUES);

        // referentials
        db.execSQL(SQL_DELETE_REF_COMMERCIAL_SPECIES);
        db.execSQL(SQL_DELETE_REF_LOCATIONS);
        db.execSQL(SQL_DELETE_REF_MENSURATIONS);
        db.execSQL(SQL_DELETE_REF_METIERS);
        db.execSQL(SQL_DELETE_REF_PRESENTATIONS);
        db.execSQL(SQL_DELETE_REF_SCIENTIFIC_SPECIES);
        db.execSQL(SQL_DELETE_REF_STATES);
        db.execSQL(SQL_DELETE_REF_VESSELS);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public synchronized void close() {
        super.close();
        getReadableDatabase().close();
        getWritableDatabase().close();
    }

    // CONTEXTS

    public Cursor getAllContexts() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(ContextModel.TABLE_NAME, ContextModel.ALL_COLUMNS, null, null, null, null, null);
        return cursor;
    }

    //LOCATION

    public Cursor getAllLocations(String contextId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(LocationModel.TABLE_NAME, LocationModel.ALL_COLUMNS,
                                 LocationModel.COLUMN_CONTEXT_ID + " = ?", new String[]{ contextId },
                                 null, null, null);
        return cursor;
    }

    //VESSEL

    public Cursor getAllVessels(String locationId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(VesselModel.TABLE_NAME, VesselModel.ALL_COLUMNS,
                                 VesselModel.COLUMN_LOCATION_ID + " = ?", new String[]{ locationId },
                                 null, null, null);
        return cursor;
    }

    //METIERS

    public Cursor getAllMetiers(String vesselId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(MetierModel.TABLE_NAME, MetierModel.ALL_COLUMNS,
                                 MetierModel.COLUMN_VESSEL_ID + " = ?", new String[]{ vesselId },
                                 null, null, null);
        return cursor;
    }

    //COMMERCIAL SPECIES

    public Cursor getAllCommercialSpecies(String metierId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(CommercialSpeciesModel.TABLE_NAME, CommercialSpeciesModel.ALL_COLUMNS,
                                 CommercialSpeciesModel.COLUMN_METIER_ID + " = ?", new String[]{ metierId },
                                 null, null, null);
        return cursor;
    }

    public Cursor getAllSortCategories() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(CommercialSpeciesModel.TABLE_NAME, new String[]{ CommercialSpeciesModel.COLUMN_SORT_CATEGORY },
                                 null, null, null, null, null);
        return cursor;
    }

    public Cursor getAllMeasurementsForCommercialSpecies(String commercialSpeciesId) {
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM " + MeasurementModel.TABLE_NAME + " m INNER JOIN " +
                            ScientificSpeciesModel.TABLE_NAME + " s " +
                            "ON m." + MeasurementModel.COLUMN_SCIENTIFIC_SPECIES_ID + " = " +
                            "s." + ScientificSpeciesModel._ID +
                        " WHERE s." + ScientificSpeciesModel.COLUMN_COMMERCIAL_SPECIES_ID + "=?";
        Cursor cursor = db.rawQuery(query, new String[]{ commercialSpeciesId });
        return cursor;
    }

    //SCIENTIFIC SPECIES

    public Cursor getAllScientificSpecies(String commercialSpeciesId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(ScientificSpeciesModel.TABLE_NAME, ScientificSpeciesModel.ALL_COLUMNS,
                                 ScientificSpeciesModel.COLUMN_COMMERCIAL_SPECIES_ID + " = ?", new String[]{ commercialSpeciesId },
                                 null, null, null);
        return cursor;
    }

    //MEASUREMENTS

    public Cursor getAllMeasurements(String scientificSpeciesId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(MeasurementModel.TABLE_NAME, MeasurementModel.ALL_COLUMNS,
                                 MeasurementModel.COLUMN_SCIENTIFIC_SPECIES_ID + " = ?", new String[]{ scientificSpeciesId },
                                 null, null, MeasurementModel.COLUMN_DATE + " ASC");
        return cursor;
    }

    public void deleteMeasurement(MeasurementModel measurement) {
        if (!measurement.isNew()) {
            SQLiteDatabase db = getWritableDatabase();
            db.delete(MeasurementModel.TABLE_NAME, MeasurementModel._ID + " = ?", new String[]{ measurement.getId() });
        }
    }

    // CATEGORY WEIGHTS

    public Cursor getAllCategoryWeigths(String scientificSpeciesId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(CategoryWeightModel.TABLE_NAME, CategoryWeightModel.ALL_COLUMNS,
                                 CategoryWeightModel.COLUMN_SCIENTIFIC_SPECIES_ID + " = ?", new String[]{ scientificSpeciesId },
                                 null, null, null);
        return cursor;
    }

    public Cursor getCategoryWeight(String scientificSpeciesId, String category1Id,
                                    String category2Id, String category3Id) {
        SQLiteDatabase db = getReadableDatabase();
        String cat1Condition = category1Id == null ? " IS NULL" : " = '" + category1Id + "'";
        String cat2Condition = category2Id == null ? " IS NULL" : " = '" + category2Id + "'";
        String cat3Condition = category3Id == null ? " IS NULL" : " = '" + category3Id + "'";
        Cursor cursor = db.query(CategoryWeightModel.TABLE_NAME, CategoryWeightModel.ALL_COLUMNS,
                                 CategoryWeightModel.COLUMN_SCIENTIFIC_SPECIES_ID + " = ? AND " +
                                         CategoryWeightModel.COLUMN_CATEGORY_1 + cat1Condition + " AND " +
                                         CategoryWeightModel.COLUMN_CATEGORY_2 + cat2Condition + " AND " +
                                         CategoryWeightModel.COLUMN_CATEGORY_3 + cat3Condition,
                                 new String[]{ scientificSpeciesId },
                                 null, null, null);
        return cursor;
    }

    // CALCIFIED PART TAKING

    public Cursor getAllCalcifiedPartTakings() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(CalcifiedPartTaking.TABLE_NAME, CalcifiedPartTaking.ALL_COLUMNS,
                                 null, null, null, null, null);
        return cursor;
    }

    public Cursor getAllCalcifiedPartTakings(String scientificSpeciesId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(CalcifiedPartTaking.TABLE_NAME, CalcifiedPartTaking.ALL_COLUMNS,
                                 CalcifiedPartTaking.COLUMN_SCIENTIFIC_SPECIES_ID + " = ?", new String[]{ scientificSpeciesId },
                                 null, null, CalcifiedPartTaking.COLUMN_START_SIZE + " ASC");
        return cursor;
    }

    public void clearCalcifiedPartTakings() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(CalcifiedPartTaking.TABLE_NAME, null, null);
    }

    // CATEGORIES

    public Cursor getAllCategories() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(CategoryModel.TABLE_NAME, CategoryModel.ALL_COLUMNS,
                                 null, null, null, null, null);
        return cursor;
    }

    public Cursor getAllQualitativeValues() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(QualitativeValueModel.TABLE_NAME, QualitativeValueModel.ALL_COLUMNS,
                                 null, null, null, null, null);
        return cursor;
    }

    public Cursor getAllQualitativeValues(String categoryId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(QualitativeValueModel.TABLE_NAME, QualitativeValueModel.ALL_COLUMNS,
                                 QualitativeValueModel.COLUMN_CATEGORY_ID + " = ?", new String[]{ categoryId },
                                 null, null, null, null);
        return cursor;
    }

    // Referentials

    public Cursor getAllRefCommercialSpecies() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(CommercialSpecies.TABLE_NAME, CommercialSpecies.ALL_COLUMNS,
                                 null, null, null, null, null);
        return cursor;
    }

    public Cursor getAllRefLocations() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(Location.TABLE_NAME, Location.ALL_COLUMNS,
                                 null, null, null, null, Location.COLUMN_CODE);
        return cursor;
    }

    public Cursor getAllRefMensurations() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(Mensuration.TABLE_NAME, Mensuration.ALL_COLUMNS,
                                 null, null, null, null, null);
        return cursor;
    }

    public Cursor getAllRefMetiers() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(Metier.TABLE_NAME, Metier.ALL_COLUMNS,
                                 null, null, null, null, null);
        return cursor;
    }

    public Cursor getAllRefPresentations() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(Presentation.TABLE_NAME, Presentation.ALL_COLUMNS,
                                 null, null, null, null, null);
        return cursor;
    }

    public Cursor getAllRefScientificSpecies() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(ScientificSpecies.TABLE_NAME, ScientificSpecies.ALL_COLUMNS,
                                 null, null, null, null, null);
        return cursor;
    }

    public Cursor getAllRefStates() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(State.TABLE_NAME, State.ALL_COLUMNS,
                                 null, null, null, null, null);
        return cursor;
    }

    public Cursor getAllRefVessels() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(Vessel.TABLE_NAME, Vessel.ALL_COLUMNS,
                                 null, null, null, null, null);
        return cursor;
    }

    public <M extends BaseModel> void saveData(M model) {
        saveData(Lists.newArrayList(model));
    }

    public <M extends BaseModel> void saveData(Collection<M> models) {
        Preconditions.checkNotNull(models);

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        for (BaseModel model : models) {
            String tableName = model.getTableName();
//            Log.d(TAG, "saving data in " + tableName);

            boolean newSession = model.isNew();
            if (newSession) {
                String id = UUID.randomUUID().toString();
                model.setId(id);
            }
            ContentValues values = model.convertIntoContentValues();

            if (newSession) {
                db.insert(tableName, null, values);

            } else {
                db.update(tableName, values, BaseModel._ID + " = ?", new String[]{ model.getId() });
            }
            model.setModified(false);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public <M extends BaseModel> void deleteData(M model) {
        Preconditions.checkNotNull(model);
        if (model.isNew()) {
            return;
        }
        SQLiteDatabase db = getWritableDatabase();
        db.delete(model.getTableName(), BaseModel._ID + " = ?", new String[]{ model.getId() });
    }

    public static <E> List<E> transformCursorIntoCollection(Cursor cursor,
                                                            Function<Cursor, E> function) {

        List<E> result = Lists.newArrayList();
        boolean cont = cursor.moveToFirst();
        while (cont) {
            E e = function.apply(cursor);
            result.add(e);
            cont = cursor.moveToNext();
        }
        return result;
    }
}

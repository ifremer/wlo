package fr.ifremer.wlo.storage;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.Context;
import android.database.Cursor;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.models.CategoryWeightModel;
import fr.ifremer.wlo.models.CommercialSpeciesModel;
import fr.ifremer.wlo.models.ContextModel;
import fr.ifremer.wlo.models.LocationModel;
import fr.ifremer.wlo.models.MeasurementModel;
import fr.ifremer.wlo.models.MetierModel;
import fr.ifremer.wlo.models.ScientificSpeciesModel;
import fr.ifremer.wlo.models.VesselModel;
import fr.ifremer.wlo.models.categorization.CategoryModel;
import fr.ifremer.wlo.models.categorization.QualitativeValueModel;
import fr.ifremer.wlo.models.referentials.CommercialSpecies;
import fr.ifremer.wlo.models.referentials.Location;
import fr.ifremer.wlo.models.referentials.Mensuration;
import fr.ifremer.wlo.models.referentials.Metier;
import fr.ifremer.wlo.models.referentials.Presentation;
import fr.ifremer.wlo.models.referentials.ScientificSpecies;
import fr.ifremer.wlo.models.referentials.State;
import fr.ifremer.wlo.utils.UIUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class JsonExporter {

    protected static Map<String, QualitativeValueModel> qualitativeValuesById;

    public static String exportData(Context context) throws JSONException {
        WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
        JSONArray root = new JSONArray();
        try {

            Cursor qualitativeValuesCursor = soh.getAllQualitativeValues();
            List<QualitativeValueModel> qualitativeValues = WloSqlOpenHelper.transformCursorIntoCollection(qualitativeValuesCursor,
                                                           new Function<Cursor, QualitativeValueModel>() {
                @Override
                public QualitativeValueModel apply(Cursor input) {
                    return new QualitativeValueModel(input);
                }
            });
            qualitativeValuesById = Maps.uniqueIndex(qualitativeValues, BaseModel.GET_ID_FUNCTION);

            Cursor cursor = soh.getAllContexts();
            List<ContextModel> contexts = WloSqlOpenHelper.transformCursorIntoCollection(cursor, new Function<Cursor, ContextModel>() {
                @Override
                public ContextModel apply(Cursor input) {
                    return new ContextModel(input);
                }
            });
            for (ContextModel contextModel : contexts) {
                JSONObject jsonContext = createJSONContextModel(contextModel, context, soh);
                root.put(jsonContext);
            }

        } finally {
            soh.close();
        }
        return root.toString(2);
    }

    public static JSONObject createJSONContextModel(ContextModel model,
                                               final Context context,
                                               WloSqlOpenHelper soh) throws JSONException {
        Preconditions.checkNotNull(model);
        Preconditions.checkNotNull(soh);

        JSONObject jsonContext = new JSONObject();
        jsonContext.put("contexte", model.getName());
        jsonContext.put("commentaire", model.getComment());

        Cursor cursor = soh.getAllLocations(model.getId());
        List<LocationModel> locations = WloSqlOpenHelper.transformCursorIntoCollection(cursor, new Function<Cursor, LocationModel>() {
            @Override
            public LocationModel apply(Cursor input) {
                return new LocationModel(context, input);
            }
        });
        JSONArray jsonLocations = new JSONArray();
        for (LocationModel location : locations) {
            JSONObject jsonLocation = createJSONLocationModel(location, context, soh);
            jsonLocations.put(jsonLocation);
        }
        jsonContext.put("lieux", jsonLocations);
        cursor.close();

        return jsonContext;
    }

    public static JSONObject createJSONLocationModel(LocationModel model,
                                                final Context context,
                                                WloSqlOpenHelper soh) throws JSONException {
        Preconditions.checkNotNull(model);
        Preconditions.checkNotNull(soh);

        JSONObject jsonLocation = new JSONObject();

        jsonLocation.put("lieu", createJSONLocation(model.getLocation()));
        if (model.getStartDate() != null) {
            jsonLocation.put("date-début", UIUtils.UTC_DATE_FORMAT.format(model.getStartDate().getTime()));
        }
        if (model.getEndDate() != null) {
            jsonLocation.put("date-fin", UIUtils.UTC_DATE_FORMAT.format(model.getEndDate().getTime()));
        }
        jsonLocation.put("observateur", model.getOperator());
        jsonLocation.put("commentaire", model.getComment());

        Cursor cursor = soh.getAllVessels(model.getId());
        List<VesselModel> vessels = WloSqlOpenHelper.transformCursorIntoCollection(cursor, new Function<Cursor, VesselModel>() {
            @Override
            public VesselModel apply(Cursor input) {
                return new VesselModel(context, input);
            }
        });
        JSONArray jsonVessels = new JSONArray();
        for (VesselModel vessel : vessels) {
            JSONObject jsonVessel = createJSONVesselModel(vessel, context, soh);
            jsonVessels.put(jsonVessel);
        }
        jsonLocation.put("navires", jsonVessels);
        cursor.close();

        return jsonLocation;
    }

    public static JSONObject createJSONVesselModel(VesselModel model,
                                                   final Context context,
                                                   WloSqlOpenHelper soh) throws JSONException {
        Preconditions.checkNotNull(model);
        Preconditions.checkNotNull(soh);

        JSONObject jsonVessel = new JSONObject();

        jsonVessel.put("immatriculation", model.getRegistrationNumber());
        jsonVessel.put("nom", model.getRegistrationNumber());
        if (model.getLandingDate() != null) {
            jsonVessel.put("date-débarquement", UIUtils.UTC_DATE_FORMAT.format(model.getLandingDate().getTime()));
        }
        jsonVessel.put("lieu-débarquement", createJSONLocation(model.getLandingLocation()));
        jsonVessel.put("commentaire", model.getComment());

        Cursor cursor = soh.getAllMetiers(model.getId());
        List<MetierModel> metiers = WloSqlOpenHelper.transformCursorIntoCollection(cursor, new Function<Cursor, MetierModel>() {
            @Override
            public MetierModel apply(Cursor input) {
                return new MetierModel(context, input);
            }
        });
        JSONArray jsonMetiers = new JSONArray();
        for (MetierModel metier : metiers) {
            JSONObject jsonMetier = createJSONMetierModel(metier, context, soh);
            jsonMetiers.put(jsonMetier);
        }
        jsonVessel.put("métiers", jsonMetiers);
        cursor.close();

        return jsonVessel;
    }

    public static JSONObject createJSONMetierModel(MetierModel model,
                                                   final Context context,
                                                   WloSqlOpenHelper soh) throws JSONException {
        Preconditions.checkNotNull(model);
        Preconditions.checkNotNull(soh);

        JSONObject jsonMetier = new JSONObject();

        jsonMetier.put("engin-espèce", createJSONMetier(model.getGearSpecies()));
        jsonMetier.put("secteur", model.getZone());
        jsonMetier.put("référence-plan", model.getSampleRowCode());
        jsonMetier.put("commentaire", model.getComment());

        Cursor cursor = soh.getAllCommercialSpecies(model.getId());
        List<CommercialSpeciesModel> commercialSpecies = WloSqlOpenHelper.transformCursorIntoCollection(cursor, new Function<Cursor, CommercialSpeciesModel>() {
            @Override
            public CommercialSpeciesModel apply(Cursor input) {
                return new CommercialSpeciesModel(context, input);
            }
        });
        JSONArray jsonCommercialSpeciesArray = new JSONArray();
        for (CommercialSpeciesModel commercialSpeciesModel : commercialSpecies) {
            JSONObject jsonCommercialSpecies = createJSONComercialSpeciesModel(commercialSpeciesModel, context, soh);
            jsonCommercialSpeciesArray.put(jsonCommercialSpecies);
        }
        jsonMetier.put("espèces-fao", jsonCommercialSpeciesArray);
        cursor.close();

        return jsonMetier;
    }

    public static JSONObject createJSONComercialSpeciesModel(CommercialSpeciesModel model,
                                                   final Context context,
                                                   WloSqlOpenHelper soh) throws JSONException {
        Preconditions.checkNotNull(model);
        Preconditions.checkNotNull(soh);

        JSONObject jsonCommercialSpecies = new JSONObject();

        jsonCommercialSpecies.put("espèce-fao", createJSONCommercialSpecies(model.getFaoCode()));
        jsonCommercialSpecies.put("méthode-mensuration", createJSONMensuration(model.getMeasurementMethod()));
        jsonCommercialSpecies.put("état", createJSONState(model.getState()));
        jsonCommercialSpecies.put("présentation", createJSONPresentation(model.getPresentation()));
        jsonCommercialSpecies.put("précision", model.getPrecision().getValue());
        jsonCommercialSpecies.put("mélange-espèces", model.isSpeciesMix());
        jsonCommercialSpecies.put("catégorie-tri", model.getSortCategory());
        jsonCommercialSpecies.put("poids-total-déchargé", model.getTotalUnloadedWeight());
        jsonCommercialSpecies.put("commentaire", model.getComment());

        Cursor cursor = soh.getAllScientificSpecies(model.getId());
        List<ScientificSpeciesModel> scientificSpecies = WloSqlOpenHelper.transformCursorIntoCollection(cursor, new Function<Cursor, ScientificSpeciesModel>() {
            @Override
            public ScientificSpeciesModel apply(Cursor input) {
                return new ScientificSpeciesModel(context, input);
            }
        });
        JSONArray jsonScientificSpeciesArray = new JSONArray();
        for (ScientificSpeciesModel scientificSpeciesModel : scientificSpecies) {
            scientificSpeciesModel.setParent(model);
            JSONObject jsonScientificSpecies = createJSONScientificSpeciesModel(scientificSpeciesModel, context, soh);
            jsonScientificSpeciesArray.put(jsonScientificSpecies);
        }
        jsonCommercialSpecies.put("espèces-scientifiques", jsonScientificSpeciesArray);
        cursor.close();

        return jsonCommercialSpecies;
    }

    public static JSONObject createJSONScientificSpeciesModel(ScientificSpeciesModel model,
                                                             final Context context,
                                                             WloSqlOpenHelper soh) throws JSONException {
        Preconditions.checkNotNull(model);
        Preconditions.checkNotNull(soh);

        JSONObject jsonScientificSpeciesModel = new JSONObject();
        jsonScientificSpeciesModel.put("espèce-scientifique", createJSONScientificSpecies(model.getName()));
        jsonScientificSpeciesModel.put("prélèvement-pièces-calcifiées", model.isTakingActivation());
        jsonScientificSpeciesModel.put("commentaire", model.getComment());
        jsonScientificSpeciesModel.put("poids-trié", model.getSortedWeight());
        jsonScientificSpeciesModel.put("poids-échantillon", model.getSampleWeight());

        Cursor cursor = soh.getAllMeasurements(model.getId());
        List<MeasurementModel> measurements = WloSqlOpenHelper.transformCursorIntoCollection(cursor, new Function<Cursor, MeasurementModel>() {
            @Override
            public MeasurementModel apply(Cursor input) {
                return new MeasurementModel(input);
            }
        });

        List<String> categories = new ArrayList<>();
        CommercialSpeciesModel parent = model.getParent();
        CategoryModel cat1 = parent.getCategory1();
        categories.add(cat1 != null ? cat1.getLabel() : null);
        CategoryModel cat2 = parent.getCategory2();
        categories.add(cat2 != null ? cat2.getLabel() : null);
        CategoryModel cat3 = parent.getCategory3();
        categories.add(cat3 != null ? cat3.getLabel() : null);

        JSONArray jsonMeasurements = new JSONArray();
        for (MeasurementModel measurement : measurements) {
            JSONObject jsonMeasurement = createJSONMeasurementModel(measurement, categories);
            jsonMeasurements.put(jsonMeasurement);
        }
        jsonScientificSpeciesModel.put("observations", jsonMeasurements);
        cursor.close();

        cursor = soh.getAllCategoryWeigths(model.getId());
        List<CategoryWeightModel> categoryWeights = WloSqlOpenHelper.transformCursorIntoCollection(cursor, new Function<Cursor, CategoryWeightModel>() {
            @Override
            public CategoryWeightModel apply(Cursor input) {
                return new CategoryWeightModel(input);
            }
        });
        JSONArray jsonCategoryWeightModels = new JSONArray();
        for (CategoryWeightModel categoryWeight : categoryWeights) {
            JSONObject jsonCategoryWeightModel = createJSONCategoryWeightModel(categoryWeight, categories);
            jsonCategoryWeightModels.put(jsonCategoryWeightModel);
        }
        jsonScientificSpeciesModel.put("poids-par-categorie", jsonCategoryWeightModels);
        cursor.close();

        return jsonScientificSpeciesModel;
    }

    public static JSONObject createJSONMeasurementModel(MeasurementModel model,
                                                        List<String> categories) throws JSONException {
        Preconditions.checkNotNull(model);
        JSONObject jsonMeasurementModel = new JSONObject();
        for (int i = 0 ; i < categories.size() ; i++) {
            String categoryLabel = categories.get(i);
            if (categoryLabel != null) {
                String measurementCategory = null;
                switch (i) {
                    case 0:
                        measurementCategory = model.getCategory1();
                        break;
                    case 1:
                        measurementCategory = model.getCategory2();
                        break;
                    case 2:
                        measurementCategory = model.getCategory3();
                        break;
                }
                if (measurementCategory != null) {
                    QualitativeValueModel value = qualitativeValuesById.get(measurementCategory);
                    if (value == null) {
                        jsonMeasurementModel.put(categoryLabel, measurementCategory);

                    } else {
                        JSONObject jsonCategory = new JSONObject();
                        jsonCategory.put("code", value.getValue());
                        jsonCategory.put("libellé", value.getLabel());
                        jsonMeasurementModel.put(categoryLabel, jsonCategory);
                    }
                }
            }
        }
        jsonMeasurementModel.put("taille", model.getSize());
        jsonMeasurementModel.put("date", UIUtils.UTC_DATE_FORMAT.format(model.getDate().getTime()));

        return jsonMeasurementModel;
    }

    public static JSONObject createJSONCategoryWeightModel(CategoryWeightModel model,
                                                           List<String> categories) throws JSONException {
        Preconditions.checkNotNull(model);
        JSONObject jsonCategoryWeightModel = new JSONObject();
        for (int i = 0 ; i < categories.size() ; i++) {
            String categoryLabel = categories.get(i);
            if (categoryLabel != null) {
                String measurementCategory = null;
                switch (i) {
                    case 0:
                        measurementCategory = model.getCategory1();
                        break;
                    case 1:
                        measurementCategory = model.getCategory2();
                        break;
                    case 2:
                        measurementCategory = model.getCategory3();
                        break;
                }
                if (measurementCategory != null) {
                    QualitativeValueModel value = qualitativeValuesById.get(measurementCategory);
                    if (value == null) {
                        jsonCategoryWeightModel.put(categoryLabel, measurementCategory);

                    } else {
                        JSONObject jsonCategory = new JSONObject();
                        jsonCategory.put("code", value.getValue());
                        jsonCategory.put("libellé", value.getLabel());
                        jsonCategoryWeightModel.put(categoryLabel, jsonCategory);
                    }
                }
            }
        }
        jsonCategoryWeightModel.put("poids", model.getWeight());

        return jsonCategoryWeightModel;
    }

    public static JSONObject createJSONLocation(Location location) throws JSONException {
        if (location == null) {
            return null;
        }
        JSONObject jsonLocation = new JSONObject();
        jsonLocation.put("type", location.getTypeLabel());
        jsonLocation.put("code", location.getCode());
        jsonLocation.put("libellé", location.getLabel());
        return jsonLocation;
    }

    public static JSONObject createJSONMetier(Metier metier) throws JSONException {
        if (metier == null) {
            return null;
        }
        JSONObject jsonMetier = new JSONObject();
        jsonMetier.put("id", metier.getId());
        jsonMetier.put("code", metier.getCode());
        jsonMetier.put("libellé", metier.getLabel());
        jsonMetier.put("engin-code", metier.getGearCode());
        jsonMetier.put("engin-libellé", metier.getGearLabel());
        jsonMetier.put("espece-code", metier.getSpeciesCode());
        jsonMetier.put("espece-libellé", metier.getSpeciesLabel());
        jsonMetier.put("pêche", metier.getFishing());
        jsonMetier.put("actif", metier.getActive());
        return jsonMetier;
    }

    public static JSONObject createJSONCommercialSpecies(CommercialSpecies commercialSpecies) throws JSONException {
        if (commercialSpecies == null) {
            return null;
        }
        JSONObject jsonCommercialSpecies = new JSONObject();
        jsonCommercialSpecies.put("code", commercialSpecies.getCode());
        jsonCommercialSpecies.put("isscap", commercialSpecies.getIsscap());
        jsonCommercialSpecies.put("code-taxon", commercialSpecies.getTaxonCode());
        jsonCommercialSpecies.put("libellé-scientifique", commercialSpecies.getScientificLabel());
        jsonCommercialSpecies.put("libellé-français", commercialSpecies.getFrenchLabel());
        jsonCommercialSpecies.put("famille", commercialSpecies.getFamily());
        jsonCommercialSpecies.put("ordre", commercialSpecies.getSpeciesOrder());
        jsonCommercialSpecies.put("actif", commercialSpecies.getActive());
        return jsonCommercialSpecies;
    }

    public static JSONObject createJSONMensuration(Mensuration mensuration) throws JSONException {
        if (mensuration == null) {
            return null;
        }
        JSONObject jsonMensuration = new JSONObject();
        jsonMensuration.put("code", mensuration.getCode());
        jsonMensuration.put("libellé", mensuration.getLabel());
        return jsonMensuration;
    }

    public static JSONObject createJSONState(State state) throws JSONException {
        if (state == null) {
            return null;
        }
        JSONObject jsonState = new JSONObject();
        jsonState.put("code", state.getCode());
        jsonState.put("libellé", state.getLabel());
        return jsonState;
    }

    public static JSONObject createJSONPresentation(Presentation presentation) throws JSONException {
        if (presentation == null) {
            return null;
        }
        JSONObject jsonPresentation = new JSONObject();
        jsonPresentation.put("code", presentation.getCode());
        jsonPresentation.put("libellé", presentation.getLabel());
        return jsonPresentation;
    }

    public static JSONObject createJSONScientificSpecies(ScientificSpecies scientificSpecies) throws JSONException {
        if (scientificSpecies == null) {
            return null;
        }
        JSONObject jsonScientificSpecies = new JSONObject();
        jsonScientificSpecies.put("code", scientificSpecies.getCode());
        jsonScientificSpecies.put("libellé", scientificSpecies.getLabel());
        jsonScientificSpecies.put("perm-code", scientificSpecies.getPermCode());
        return jsonScientificSpecies;
    }

}

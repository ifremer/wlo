package fr.ifremer.wlo.storage;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.Context;
import android.database.Cursor;
import com.google.common.base.Function;
import com.google.common.collect.Maps;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.models.CommercialSpeciesModel;
import fr.ifremer.wlo.models.ContextModel;
import fr.ifremer.wlo.models.LocationModel;
import fr.ifremer.wlo.models.MeasurementModel;
import fr.ifremer.wlo.models.MetierModel;
import fr.ifremer.wlo.models.ScientificSpeciesModel;
import fr.ifremer.wlo.models.VesselModel;
import fr.ifremer.wlo.models.categorization.CategoryModel;
import fr.ifremer.wlo.models.categorization.QualitativeValueModel;
import fr.ifremer.wlo.models.referentials.CommercialSpecies;
import fr.ifremer.wlo.models.referentials.Location;
import fr.ifremer.wlo.models.referentials.Mensuration;
import fr.ifremer.wlo.models.referentials.Metier;
import fr.ifremer.wlo.models.referentials.Presentation;
import fr.ifremer.wlo.models.referentials.ScientificSpecies;
import fr.ifremer.wlo.models.referentials.State;
import fr.ifremer.wlo.utils.UIUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.nuiton.csv.ValueGetter;
import org.nuiton.csv.ext.AbstractExportModel;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class CsvExporter extends AbstractExportModel<MeasurementModel> {

    private static final String TAG = "CsvExporter";

    protected Map<String, QualitativeValueModel> qualitativeValuesById;

    public CsvExporter(char separator, Context context) {
        super(separator);

        WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
        Cursor qualitativeValuesCursor = soh.getAllQualitativeValues();
        List<QualitativeValueModel> qualitativeValues =
                WloSqlOpenHelper.transformCursorIntoCollection(qualitativeValuesCursor,
                                                               new Function<Cursor, QualitativeValueModel>() {
                                                                   @Override
                                                                   public QualitativeValueModel apply(Cursor input) {
                                                                       return new QualitativeValueModel(input);
                                                                   }
                                                               });
        qualitativeValuesById = Maps.uniqueIndex(qualitativeValues, BaseModel.GET_ID_FUNCTION);

        // context
        newColumnForExport("contexte_nom", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                ContextModel contextModel = object.getParent(ContextModel.class);
                return contextModel.getName();
            }
        });

        newColumnForExport("contexte_commentaire", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                ContextModel contextModel = object.getParent(ContextModel.class);
                return contextModel.getComment();
            }
        });

        // location
        newColumnForExport("lieu_code", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                LocationModel locationModel = object.getParent(LocationModel.class);
                return locationModel.getLocation().getCode();
            }
        });

        newColumnForExport("lieu_libelle", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                LocationModel locationModel = object.getParent(LocationModel.class);
                return locationModel.getLocation().getLabel();
            }
        });

        newColumnForExport("lieu_type", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                LocationModel locationModel = object.getParent(LocationModel.class);
                return locationModel.getLocation().getTypeLabel();
            }
        });

        newColumnForExport("lieu_date_debut", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                LocationModel locationModel = object.getParent(LocationModel.class);
                Calendar startDate = locationModel.getStartDate();
                return formatDate(startDate);
            }
        });

        newColumnForExport("lieu_date_fin", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                LocationModel locationModel = object.getParent(LocationModel.class);
                Calendar endDate = locationModel.getEndDate();
                return formatDate(endDate);
            }
        });

        newColumnForExport("lieu_observateur", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                LocationModel locationModel = object.getParent(LocationModel.class);
                return locationModel.getOperator();
            }
        });

        newColumnForExport("lieu_commentaire", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                LocationModel locationModel = object.getParent(LocationModel.class);
                return locationModel.getComment();
            }
        });

        // vessel

        newColumnForExport("navire_immatriculation", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                VesselModel vesselModel = object.getParent(VesselModel.class);
                return vesselModel.getRegistrationNumber();
            }
        });

        newColumnForExport("navire_nom", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                VesselModel vesselModel = object.getParent(VesselModel.class);
                return vesselModel.getName();
            }
        });

        newColumnForExport("navire_date_debarquement", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                VesselModel vesselModel = object.getParent(VesselModel.class);
                Calendar landingDate = vesselModel.getLandingDate();
                return formatDate(landingDate);
            }
        });

        newColumnForExport("navire_lieu_debarquement_code", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                VesselModel vesselModel = object.getParent(VesselModel.class);
                Location landingLocation = vesselModel.getLandingLocation();
                return landingLocation != null ? landingLocation.getCode() : null;
            }
        });

        newColumnForExport("navire_lieu_debarquement_libelle", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                VesselModel vesselModel = object.getParent(VesselModel.class);
                Location landingLocation = vesselModel.getLandingLocation();
                return landingLocation != null ? landingLocation.getLabel() : null;
            }
        });

        newColumnForExport("navire_lieu_debarquement_type", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                VesselModel vesselModel = object.getParent(VesselModel.class);
                Location landingLocation = vesselModel.getLandingLocation();
                return landingLocation != null ? landingLocation.getTypeLabel() : null;
            }
        });

        newColumnForExport("navire_commentaire", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                VesselModel vesselModel = object.getParent(VesselModel.class);
                return vesselModel.getComment();
            }
        });

        // metier

        newColumnForExport("metier_engin_id", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                MetierModel metierModel = object.getParent(MetierModel.class);
                Metier gearSpecies = metierModel.getGearSpecies();
                return gearSpecies != null ? gearSpecies.getMetierId() : null;
            }
        });

        newColumnForExport("metier_engin_code", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                MetierModel metierModel = object.getParent(MetierModel.class);
                Metier gearSpecies = metierModel.getGearSpecies();
                return gearSpecies != null ? gearSpecies.getCode() : null;
            }
        });

        newColumnForExport("metier_engin_libelle", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                MetierModel metierModel = object.getParent(MetierModel.class);
                Metier gearSpecies = metierModel.getGearSpecies();
                return gearSpecies != null ? gearSpecies.getLabel() : null;
            }
        });

        newColumnForExport("metier_secteur_peche", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                MetierModel metierModel = object.getParent(MetierModel.class);
                return metierModel.getZone();
            }
        });

        newColumnForExport("metier_plan", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                MetierModel metierModel = object.getParent(MetierModel.class);
                return metierModel.getSampleRowCode();
            }
        });

        newColumnForExport("metier_commentaire", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                MetierModel metierModel = object.getParent(MetierModel.class);
                return metierModel.getComment();
            }
        });

        // commercial species

        newColumnForExport("espece_commerciale_code", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                CommercialSpeciesModel commercialSpeciesModel = object.getParent(CommercialSpeciesModel.class);
                CommercialSpecies species = commercialSpeciesModel.getFaoCode();
                return species != null ? species.getCode() : null;
            }
        });

        newColumnForExport("espece_commerciale_label", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                CommercialSpeciesModel commercialSpeciesModel = object.getParent(CommercialSpeciesModel.class);
                CommercialSpecies species = commercialSpeciesModel.getFaoCode();
                return species != null ? species.getFrenchLabel() : null;
            }
        });

        newColumnForExport("espece_commerciale_methode_mensuration_code", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                CommercialSpeciesModel commercialSpeciesModel = object.getParent(CommercialSpeciesModel.class);
                Mensuration measurementMethod = commercialSpeciesModel.getMeasurementMethod();
                return measurementMethod != null ? measurementMethod.getCode() : null;
            }
        });

        newColumnForExport("espece_commerciale_methode_mensuration_libelle", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                CommercialSpeciesModel commercialSpeciesModel = object.getParent(CommercialSpeciesModel.class);
                Mensuration measurementMethod = commercialSpeciesModel.getMeasurementMethod();
                return measurementMethod != null ? measurementMethod.getLabel() : null;
            }
        });

        newColumnForExport("espece_commerciale_precision", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                CommercialSpeciesModel commercialSpeciesModel = object.getParent(CommercialSpeciesModel.class);
                return commercialSpeciesModel.getPrecision().getLabel();
            }
        });

        newColumnForExport("espece_commerciale_melange_especes", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                CommercialSpeciesModel commercialSpeciesModel = object.getParent(CommercialSpeciesModel.class);
                return String.valueOf(commercialSpeciesModel.isSpeciesMix());
            }
        });

        newColumnForExport("espece_commerciale_categorie_tri", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                CommercialSpeciesModel commercialSpeciesModel = object.getParent(CommercialSpeciesModel.class);
                return commercialSpeciesModel.getSortCategory();
            }
        });

        newColumnForExport("espece_commerciale_etat_code", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                CommercialSpeciesModel commercialSpeciesModel = object.getParent(CommercialSpeciesModel.class);
                State state = commercialSpeciesModel.getState();
                return state != null ? state.getCode() : null;
            }
        });

        newColumnForExport("espece_commerciale_etat_libelle", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                CommercialSpeciesModel commercialSpeciesModel = object.getParent(CommercialSpeciesModel.class);
                State state = commercialSpeciesModel.getState();
                return state != null ? state.getLabel() : null;
            }
        });

        newColumnForExport("espece_commerciale_presentation_code", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                CommercialSpeciesModel commercialSpeciesModel = object.getParent(CommercialSpeciesModel.class);
                Presentation presentation = commercialSpeciesModel.getPresentation();
                return presentation != null ? presentation.getCode() : null;
            }
        });

        newColumnForExport("espece_commerciale_presentation_libelle", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                CommercialSpeciesModel commercialSpeciesModel = object.getParent(CommercialSpeciesModel.class);
                Presentation presentation = commercialSpeciesModel.getPresentation();
                return presentation != null ? presentation.getLabel() : null;
            }
        });

        newColumnForExport("espece_commerciale_categorie1", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                CommercialSpeciesModel commercialSpeciesModel = object.getParent(CommercialSpeciesModel.class);
                CategoryModel category = commercialSpeciesModel.getCategory1();
                return category != null ? category.getLabel() : null;
            }
        });

        newColumnForExport("espece_commerciale_categorie2", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                CommercialSpeciesModel commercialSpeciesModel = object.getParent(CommercialSpeciesModel.class);
                CategoryModel category = commercialSpeciesModel.getCategory2();
                return category != null ? category.getLabel() : null;
            }
        });

        newColumnForExport("espece_commerciale_categorie3", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                CommercialSpeciesModel commercialSpeciesModel = object.getParent(CommercialSpeciesModel.class);
                CategoryModel category = commercialSpeciesModel.getCategory3();
                return category != null ? category.getLabel() : null;
            }
        });

        newColumnForExport("espece_commerciale_poids_total", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                CommercialSpeciesModel commercialSpeciesModel = object.getParent(CommercialSpeciesModel.class);
                Integer totalUnloadedWeight = commercialSpeciesModel.getTotalUnloadedWeight();
                return totalUnloadedWeight != null ? totalUnloadedWeight.toString() : null;
            }
        });

        newColumnForExport("espece_commerciale_commentaire", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                CommercialSpeciesModel commercialSpeciesModel = object.getParent(CommercialSpeciesModel.class);
                return commercialSpeciesModel.getComment();
            }
        });

        // scientific species

        newColumnForExport("espece_scientifique_code", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                ScientificSpeciesModel speciesModel = object.getParent();
                ScientificSpecies species = speciesModel.getName();
                return species != null ? species.getCode() : null;
            }
        });

        newColumnForExport("espece_scientifique_name", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                ScientificSpeciesModel speciesModel = object.getParent();
                ScientificSpecies species = speciesModel.getName();
                return species != null ? species.getLabel() : null;
            }
        });

        newColumnForExport("espece_scientifique_comment", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                ScientificSpeciesModel speciesModel = object.getParent();
                return speciesModel.getComment();
            }
        });

        newColumnForExport("espece_scientifique_sorted_weight", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                ScientificSpeciesModel speciesModel = object.getParent();
                Integer sortedWeight = speciesModel.getSortedWeight();
                return sortedWeight != null ? sortedWeight.toString() : null;
            }
        });

        newColumnForExport("espece_scientifique_sample_weight", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                ScientificSpeciesModel speciesModel = object.getParent();
                Integer sampleWeight = speciesModel.getSampleWeight();
                return sampleWeight != null ? sampleWeight.toString() : null;
            }
        });

        // observations

        newColumnForExport("observation_categorie1_code", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                return getCategoryValue(object.getCategory1());
            }
        });

        newColumnForExport("observation_categorie1_label", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                return getCategoryLabel(object.getCategory1());
            }
        });

        newColumnForExport("observation_categorie2_code", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                return getCategoryValue(object.getCategory2());
            }
        });

        newColumnForExport("observation_categorie2_label", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                return getCategoryLabel(object.getCategory2());
            }
        });

        newColumnForExport("observation_categorie3_code", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                return getCategoryValue(object.getCategory3());
            }
        });

        newColumnForExport("observation_categorie3_label", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                return getCategoryLabel(object.getCategory3());
            }
        });

        newColumnForExport("observation_categories_poids", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                ScientificSpeciesModel speciesModel = object.getParent();
                Map<Triple<String, String, String>, Integer> categoryWeights = speciesModel.getCategoryWeights();
                Triple<String, String, String> key = Triple.of(object.getCategory1(), object.getCategory2(), object.getCategory3());
                Integer weight = categoryWeights.get(key);
                return weight != null ? weight.toString() : null;
            }
        });

        newColumnForExport("observation_date", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                return formatDate(object.getDate());
            }
        });

        newColumnForExport("observation_taille", new ValueGetter<MeasurementModel, String>() {
            @Override
            public String get(MeasurementModel object) throws Exception {
                return object.getSize().toString();
            }
        });
    }

    protected String getCategoryValue(String measurementCategory) {
        QualitativeValueModel value = qualitativeValuesById.get(measurementCategory);
        if (value == null) {
            return measurementCategory;

        }
        return value.getValue();
    }

    protected String getCategoryLabel(String measurementCategory) {
        QualitativeValueModel value = qualitativeValuesById.get(measurementCategory);
        if (value == null) {
            return measurementCategory;

        }
        return value.getLabel();
    }

    protected String formatDate(Calendar calendar) {
        return calendar != null ? UIUtils.UTC_DATE_FORMAT.format(calendar.getTime()) : null;
    }
}

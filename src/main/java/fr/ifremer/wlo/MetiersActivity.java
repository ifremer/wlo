package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import com.google.common.collect.Maps;
import fr.ifremer.wlo.models.MetierModel;
import fr.ifremer.wlo.utils.WloItemListViewBinder;

import java.util.Map;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class MetiersActivity extends WloBaseListActivity<MetierModel> {

    private static final String TAG = "MetierActivity";

    /*  Activity methods  */

    @Override
    protected SimpleCursorAdapter createAdapter() {
        return new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, null,
                                       new String[] { MetierModel.COLUMN_GEAR_SPECIES },
                                       new int[] { android.R.id.text1 }, 0);
    }

    @Override
    protected Cursor getAllData() {
        Cursor cursor = woh.getAllMetiers(parentModel.getId());
        return cursor;
    }

    @Override
    protected MetierModel createNewModel(Cursor cursor) {
        return new MetierModel(this, cursor);
    }

    @Override
    protected Class<? extends WloModelEditionActivity> getEditionActivity() {
        return MetierFormActivity.class;
    }

    @Override
    protected Integer getSubtitle() {
        return R.string.metiers_subtitle;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return VesselsActivity.class;
    }

    @Override
    protected Class<? extends WloBaseActivity> getNextActivity() {
        return CommercialSpeciesActivity.class;
    }

    @Override
    protected SimpleCursorAdapter.ViewBinder getAdapterBinder() {
        Map<Integer, WloItemListViewBinder.DataType> types = Maps.newHashMap();
        types.put(1, WloItemListViewBinder.DataType.METIER);
        return new WloItemListViewBinder(this, types);
    }
}

/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.models.CommercialSpeciesModel;
import fr.ifremer.wlo.models.ContextModel;
import fr.ifremer.wlo.models.HierarchicalModel;
import fr.ifremer.wlo.models.LocationModel;
import fr.ifremer.wlo.models.MetierModel;
import fr.ifremer.wlo.models.ScientificSpeciesModel;
import fr.ifremer.wlo.models.VesselModel;
import fr.ifremer.wlo.storage.WloSqlOpenHelper;
import fr.ifremer.wlo.utils.UIUtils;
import fr.ifremer.wlo.utils.WloItemListViewBinder;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * An activity that displays a list of items by binding to a data source such as
 * an array or Cursor, and exposes event handlers when the user selects an item.
 * <p>
 * ListActivity hosts a {@link android.widget.ListView ListView} object that can
 * be bound to different data sources, typically either an array or a Cursor
 * holding query results. Binding, screen layout, and row layout are discussed
 * in the following sections.
 * <p>
 * <strong>Screen Layout</strong>
 * </p>
 * <p>
 * ListActivity has a default layout that consists of a single, full-screen list
 * in the center of the screen. However, if you desire, you can customize the
 * screen layout by setting your own view layout with setContentView() in
 * onCreate(). To do this, your own view MUST contain a ListView object with the
 * id "@android:id/list" (or {@link android.R.id#list} if it's in code)
 * <p>
 * Optionally, your custom view can contain another view object of any type to
 * display when the list view is empty. This "empty list" notifier must have an
 * id "android:empty". Note that when an empty view is present, the list view
 * will be hidden when there is no data to display.
 * <p>
 * The following code demonstrates an (ugly) custom screen layout. It has a list
 * with a green background, and an alternate red "no data" message.
 * </p>
 *
 * <pre>
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;
 * &lt;LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
 *         android:orientation=&quot;vertical&quot;
 *         android:layout_width=&quot;fill_parent&quot;
 *         android:layout_height=&quot;fill_parent&quot;
 *         android:paddingLeft=&quot;8dp&quot;
 *         android:paddingRight=&quot;8dp&quot;&gt;
 *
 *     &lt;ListView android:id=&quot;@id/android:list&quot;
 *               android:layout_width=&quot;fill_parent&quot;
 *               android:layout_height=&quot;fill_parent&quot;
 *               android:background=&quot;#00FF00&quot;
 *               android:layout_weight=&quot;1&quot;
 *               android:drawSelectorOnTop=&quot;false&quot;/&gt;
 *
 *     &lt;TextView id=&quot;@id/android:empty&quot;
 *               android:layout_width=&quot;fill_parent&quot;
 *               android:layout_height=&quot;fill_parent&quot;
 *               android:background=&quot;#FF0000&quot;
 *               android:text=&quot;No data&quot;/&gt;
 * &lt;/LinearLayout&gt;
 * </pre>
 *
 * <p>
 * <strong>Row Layout</strong>
 * </p>
 * <p>
 * You can specify the layout of individual rows in the list. You do this by
 * specifying a layout resource in the ListAdapter object hosted by the activity
 * (the ListAdapter binds the ListView to the data; more on this later).
 * <p>
 * A ListAdapter constructor takes a parameter that specifies a layout resource
 * for each row. It also has two additional parameters that let you specify
 * which data field to associate with which object in the row layout resource.
 * These two parameters are typically parallel arrays.
 * </p>
 * <p>
 * Android provides some standard row layout resources. These are in the
 * {@link android.R.layout} class, and have names such as simple_list_item_1,
 * simple_list_item_2, and two_line_list_item. The following layout XML is the
 * source for the resource two_line_list_item, which displays two data
 * fields,one above the other, for each list row.
 * </p>
 *
 * <pre>
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;
 * &lt;LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
 *     android:layout_width=&quot;fill_parent&quot;
 *     android:layout_height=&quot;wrap_content&quot;
 *     android:orientation=&quot;vertical&quot;&gt;
 *
 *     &lt;TextView android:id=&quot;@+id/text1&quot;
 *         android:textSize=&quot;16sp&quot;
 *         android:textStyle=&quot;bold&quot;
 *         android:layout_width=&quot;fill_parent&quot;
 *         android:layout_height=&quot;wrap_content&quot;/&gt;
 *
 *     &lt;TextView android:id=&quot;@+id/text2&quot;
 *         android:textSize=&quot;16sp&quot;
 *         android:layout_width=&quot;fill_parent&quot;
 *         android:layout_height=&quot;wrap_content&quot;/&gt;
 * &lt;/LinearLayout&gt;
 * </pre>
 *
 * <p>
 * You must identify the data bound to each TextView object in this layout. The
 * syntax for this is discussed in the next section.
 * </p>
 * <p>
 * <strong>Binding to Data</strong>
 * </p>
 * <p>
 * You bind the ListActivity's ListView object to data using a class that
 * implements the {@link android.widget.ListAdapter ListAdapter} interface.
 * Android provides two standard list adapters:
 * {@link android.widget.SimpleAdapter SimpleAdapter} for static data (Maps),
 * and {@link android.widget.SimpleCursorAdapter SimpleCursorAdapter} for Cursor
 * query results.
 * </p>
 * <p>
 * The following code from a custom ListActivity demonstrates querying the
 * Contacts provider for all contacts, then binding the Name and Company fields
 * to a two line row layout in the activity's ListView.
 * </p>
 *
 * <pre>
 * public class MyListAdapter extends ListActivity {
 *
 *     &#064;Override
 *     protected void onCreate(Bundle savedInstanceState){
 *         super.onCreate(savedInstanceState);
 *
 *         // We'll define a custom screen layout here (the one shown above), but
 *         // typically, you could just use the standard ListActivity layout.
 *         setContentView(R.layout.custom_list_activity_view);
 *
 *         // Query for all people contacts using the {@link android.provider.Contacts.People} convenience class.
 *         // Put a managed wrapper around the retrieved cursor so we don't have to worry about
 *         // requerying or closing it as the activity changes state.
 *         mCursor = People.query(this.getContentResolver(), null);
 *         startManagingCursor(mCursor);
 *
 *         // Now create a new list adapter bound to the cursor.
 *         // SimpleListAdapter is designed for binding to a Cursor.
 *         ListAdapter adapter = new SimpleCursorAdapter(
 *                 this, // Context.
 *                 android.R.layout.two_line_list_item,  // Specify the row template to use (here, two columns bound to the two retrieved cursor
 * rows).
 *                 mCursor,                                    // Pass in the cursor to bind to.
 *                 new String[] {People.NAME, People.COMPANY}, // Array of cursor columns to bind to.
 *                 new int[]);                                 // Parallel array of which template objects to bind to those columns.
 *
 *         // Bind to our new adapter.
 *         setListAdapter(adapter);
 *     }
 * }
 * </pre>
 *
 * @see #setListAdapter
 * @see android.widget.ListView
 *
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public abstract class WloBaseListActivity<M extends BaseModel> extends WloBaseActivity {

    private static final String TAG = "WloBaseListActivity";

    public static final String INTENT_EXTRA_PARENT_MODEL = "parentModel";

    public static final int REQUEST_EDIT_MODEL = 0;

    protected final static Map<Class, BaseModel> drawerItems = new LinkedHashMap<>();

    protected BaseModel parentModel;

    protected WloSqlOpenHelper woh;
    protected SimpleCursorAdapter adapter;

    protected ActionBarDrawerToggle mDrawerToggle;
    protected ListView mDrawerList;

    /**
     * This field should be made private, so it is hidden from the SDK.
     * {@hide}
     */
    protected ListView mList;

    private Handler mHandler = new Handler();
    private boolean mFinishedStart = false;

    private Runnable mRequestFocus = new Runnable() {
        public void run() {
            mList.focusableViewAvailable(mList);
        }
    };

    protected abstract SimpleCursorAdapter createAdapter();

    protected abstract Cursor getAllData();

    protected abstract M createNewModel(Cursor cursor);

    protected M createNewModel(AdapterView l, int position) {
        Cursor cursor = (Cursor) l.getItemAtPosition(position);
        M model = createNewModel(cursor);
        if (HierarchicalModel.class.isAssignableFrom(model.getClass())) {
            HierarchicalModel hModel = (HierarchicalModel) model;
            hModel.setParent(parentModel);
        }
        return model;
    }

    protected abstract Class<? extends WloBaseActivity> getNextActivity();

    protected abstract Class<? extends WloModelEditionActivity> getEditionActivity();

    protected abstract Integer getSubtitle();

    @Override
    protected Integer getContentView() {
        return R.layout.wlo_list;
    }

    protected SimpleCursorAdapter.ViewBinder getAdapterBinder() {
        return new WloItemListViewBinder(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        woh = new WloSqlOpenHelper(this);
        adapter = createAdapter();

        adapter.setViewBinder(getAdapterBinder());

        setListAdapter(adapter);
        registerForContextMenu(mList);

        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                selectItem(position);
            }
        });
        mDrawerList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                editItem(position);
                return true;
            }
        });
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        );

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    protected void onResume() {
        super.onResume();

        parentModel = (BaseModel) getIntent().getSerializableExtra(INTENT_EXTRA_PARENT_MODEL);
        if (parentModel != null) {
            Cursor cursor = getAllData();
            adapter.swapCursor(cursor);

            Integer subtitleId = getSubtitle();
            if (subtitleId != null) {
                String subtitle = getString(subtitleId, parentModel.toString(this));
                getSupportActionBar().setSubtitle(subtitle);
            }

        }
        // Set the adapter for the list view
        setDrawerListAdapter();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        woh.close();
    }

    @Override
    public Intent getSupportParentActivityIntent() {
        Intent intent = super.getSupportParentActivityIntent();
        if (intent != null && parentModel != null
                && HierarchicalModel.class.isAssignableFrom(parentModel.getClass())) {
            HierarchicalModel hParentModel = (HierarchicalModel) parentModel;
            intent.putExtra(WloBaseListActivity.INTENT_EXTRA_PARENT_MODEL, hParentModel.getParent());
        }
        return intent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.model_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.add_item:
                Intent intent = new Intent(this, getEditionActivity());
                intent.putExtra(WloModelEditionActivity.INTENT_EXTRA_PARENT_MODEL, parentModel);
                intent.putExtra(WloModelEditionActivity.INTENT_EXTRA_SUBTITLE, getSupportActionBar().getSubtitle());
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_EDIT_MODEL && resultCode == RESULT_OK) {
            BaseModel editedModel = (BaseModel) data.getSerializableExtra(WloModelEditionActivity.INTENT_EXTRA_MODEL);
            if (editedModel != null) {
                drawerItems.put(editedModel.getClass(), editedModel);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    protected void setDrawerListAdapter() {
        List<String> drawerItemsLabels = new ArrayList<>();
        drawerItemsLabels.add(getString(R.string.home_title));
        List<BaseModel> values = new ArrayList<>(drawerItems.values());
        drawerItemsLabels.addAll(Lists.transform(values, new Function<BaseModel, String>() {
            @Override
            public String apply(BaseModel input) {
                return input.toString(WloBaseListActivity.this);
            }
        }));
        mDrawerList.setAdapter(new ArrayAdapter<>(this,
                                                  android.R.layout.simple_list_item_1,
                                                  drawerItemsLabels));
    }

    /**
     * This method will be called when an item in the list is selected.
     * Subclasses should override. Subclasses can call
     * getListView().getItemAtPosition(position) if they need to access the
     * data associated with the selected item.
     *
     * @param l The ListView where the click happened
     * @param v The view that was clicked within the ListView
     * @param position The position of the view in the list
     * @param id The row id of the item that was clicked
     */
    protected void onListItemClick(ListView l, View v, int position, long id) {
        M model = createNewModel(l, position);

        updateDrawerItems(model);

        Intent intent = new Intent(this, getNextActivity());
        intent.putExtra(INTENT_EXTRA_PARENT_MODEL, model);
        startActivity(intent);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (mList.equals(v)) {
            int[] menuItems = new int[]{R.string.edit, R.string.delete};
            for (int i = 0; i<menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        AdapterView<?> parent = (AdapterView<?>) info.targetView.getParent();
        final M model = createNewModel(parent, info.position);

        switch (item.getItemId()) {
            case 0:
                Intent intent = new Intent(this, getEditionActivity());
                intent.putExtra(WloModelEditionActivity.INTENT_EXTRA_MODEL, model);
                intent.putExtra(WloModelEditionActivity.INTENT_EXTRA_SUBTITLE, getSupportActionBar().getSubtitle());
                startActivity(intent);
                break;

            case 1:
                new AlertDialog.Builder(this).setTitle(R.string.deletion)
                        .setMessage(getString(R.string.item_deletion_confirmation_message, model.toString(this)))
                        .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                WloSqlOpenHelper soh = new WloSqlOpenHelper(WloBaseListActivity.this);
                                soh.deleteData(model);
                                soh.close();
                                Cursor cursor = getAllData();
                                adapter.swapCursor(cursor);
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, UIUtils.getCancelClickListener())
                        .create()
                        .show();

                break;
        }
        return true;
    }

    /**
     * Ensures the list view has been created before Activity restores all
     * of the view states.
     *
     */
    @Override
    protected void onRestoreInstanceState(Bundle state) {
        ensureList();
        super.onRestoreInstanceState(state);
    }

    /**
     * Updates the screen state (current list and other views) when the
     * content changes.
     *
     */
    @Override
    public void onSupportContentChanged() {
        super.onSupportContentChanged();
        View emptyView = findViewById(android.R.id.empty);
        mList = (ListView)findViewById(android.R.id.list);
        if (mList == null) {
            throw new RuntimeException(
                    "Your content must have a ListView whose id attribute is " +
                            "'android.R.id.list'");
        }
        if (emptyView != null) {
            mList.setEmptyView(emptyView);
        }
        mList.setOnItemClickListener(mOnClickListener);
        if (mFinishedStart) {
            setListAdapter(adapter);
        }
        mHandler.post(mRequestFocus);
        mFinishedStart = true;
    }

    /**
     * Provide the cursor for the list view.
     */
    public void setListAdapter(SimpleCursorAdapter adapter) {
        synchronized (this) {
            ensureList();
            this.adapter = adapter;
            mList.setAdapter(adapter);
        }
    }

    /**
     * Set the currently selected list item to the specified
     * position with the adapter's data
     *
     * @param position
     */
    public void setSelection(int position) {
        mList.setSelection(position);
    }

    /**
     * Get the position of the currently selected list item.
     */
    public int getSelectedItemPosition() {
        return mList.getSelectedItemPosition();
    }

    /**
     * Get the cursor row ID of the currently selected list item.
     */
    public long getSelectedItemId() {
        return mList.getSelectedItemId();
    }

    /**
     * Get the activity's list view widget.
     */
    public ListView getListView() {
        ensureList();
        return mList;
    }

    /**
     * Get the ListAdapter associated with this activity's ListView.
     */
    public ListAdapter getListAdapter() {
        return adapter;
    }

    private void ensureList() {
        if (mList != null) {
            return;
        }
        setContentView(android.R.layout.list_content);

    }

    private AdapterView.OnItemClickListener mOnClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView parent, View v, int position, long id)
        {
            onListItemClick((ListView)parent, v, position, id);
        }
    };

    protected void updateDrawerItems(M model) {
        Class<? extends BaseModel> itemClass = model.getClass();
        BaseModel currentItem = drawerItems.get(itemClass);
        if (!model.equals(currentItem)) {
            drawerItems.put(itemClass, model);
            String itemTable = model.getTableName();
            switch (itemTable) {
                case ContextModel.TABLE_NAME:
                    drawerItems.remove(LocationModel.class);
                case LocationModel.TABLE_NAME:
                    drawerItems.remove(VesselModel.class);
                case VesselModel.TABLE_NAME:
                    drawerItems.remove(MetierModel.class);
                case MetierModel.TABLE_NAME:
                    drawerItems.remove(CommercialSpeciesModel.class);
                case CommercialSpeciesModel.TABLE_NAME:
                    drawerItems.remove(ScientificSpeciesModel.class);
            }
        }
    }

    protected void selectItem(int position) {
        Class activityClass;
        Class parentModelClass = null;
        switch (position) {
            case 0:
                activityClass = MainActivity.class;
                break;
            case 1:
                activityClass = ContextsActivity.class;
                break;
            case 2:
                activityClass = LocationsActivity.class;
                parentModelClass = ContextModel.class;
                break;
            case 3:
                activityClass = VesselsActivity.class;
                parentModelClass = LocationModel.class;
                break;
            case 4:
                activityClass = MetiersActivity.class;
                parentModelClass = VesselModel.class;
                break;
            case 5:
                activityClass = CommercialSpeciesActivity.class;
                parentModelClass = MetierModel.class;
                break;
            case 6:
                activityClass = ScientificSpeciesActivity.class;
                parentModelClass = CommercialSpeciesModel.class;
                break;
            default:
                activityClass = null;
        }
        BaseModel parentModel = drawerItems.get(parentModelClass);
        Intent intent = new Intent(this, activityClass);
        intent.putExtra(WloBaseListActivity.INTENT_EXTRA_PARENT_MODEL, parentModel);
        startActivity(intent);
    }

    protected void editItem(int position) {
        Class activityClass = null;
        Class modelClass = null;

        switch (position) {
            case 1:
                activityClass = ContextFormActivity.class;
                modelClass = ContextModel.class;
                break;
            case 2:
                activityClass = LocationFormActivity.class;
                modelClass = LocationModel.class;
                break;
            case 3:
                activityClass = VesselFormActivity.class;
                modelClass = VesselModel.class;
                break;
            case 4:
                activityClass = MetierFormActivity.class;
                modelClass = MetierModel.class;
                break;
            case 5:
                activityClass = CommercialSpeciesFormActivity.class;
                modelClass = CommercialSpeciesModel.class;
                break;
            case 6:
                activityClass = ScientificSpeciesFormActivity.class;
                modelClass = ScientificSpeciesModel.class;
                break;
        }
        if (activityClass != null) {
            BaseModel model = drawerItems.get(modelClass);
            Intent intent = new Intent(this, activityClass);
            intent.putExtra(WloModelEditionActivity.INTENT_EXTRA_MODEL, model);
            startActivityForResult(intent, REQUEST_EDIT_MODEL);
        }
    }
}

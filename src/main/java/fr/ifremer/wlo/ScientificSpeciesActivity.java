package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.Intent;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import com.google.common.collect.Maps;
import fr.ifremer.wlo.measurement.MeasurementActivity;
import fr.ifremer.wlo.models.CommercialSpeciesModel;
import fr.ifremer.wlo.models.ScientificSpeciesModel;
import fr.ifremer.wlo.utils.WloItemListViewBinder;

import java.util.Map;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class ScientificSpeciesActivity extends WloBaseListActivity<ScientificSpeciesModel> {

    private static final String TAG = "ScientificSpeciesActivity";

    protected static final int REQUEST_EDIT_WEIGHTS = 0;

    @Override
    protected SimpleCursorAdapter createAdapter() {
        return new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, null,
                                       new String[] { ScientificSpeciesModel.COLUMN_NAME },
                                       new int[] { android.R.id.text1 }, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        CommercialSpeciesModel parent = (CommercialSpeciesModel) parentModel;
        MenuInflater inflater = getMenuInflater();
        if (parent != null && parent.isSpeciesMix() || adapter.getCount() == 0) {
            inflater.inflate(R.menu.model_list_menu, menu);
        }
        inflater.inflate(R.menu.weight_menu, menu);
        return true;
    }

    @Override
    protected Cursor getAllData() {
        Cursor cursor = woh.getAllScientificSpecies(parentModel.getId());
        return cursor;
    }

    @Override
    protected ScientificSpeciesModel createNewModel(Cursor cursor) {
        return new ScientificSpeciesModel(this, cursor);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.weights:
                Intent intent = new Intent(this, WeightsActivity.class);
                intent.putExtra(WeightsActivity.INTENT_COMMERCIAL_SPECIES, parentModel);
                startActivityForResult(intent, REQUEST_EDIT_WEIGHTS);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected Class<? extends WloModelEditionActivity> getEditionActivity() {
        return ScientificSpeciesFormActivity.class;
    }

    @Override
    protected Integer getSubtitle() {
        return R.string.scientific_species_subtitle;
    }

    @Override
    protected Class<? extends WloBaseActivity> getNextActivity() {
        return MeasurementActivity.class;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return CommercialSpeciesActivity.class;
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        ScientificSpeciesModel model = createNewModel(l, position);

        updateDrawerItems(model);

        Intent intent = new Intent(this, getNextActivity());
        intent.putExtra(MeasurementActivity.INTENT_EXTRA_SCIENTIFIC_SPECIES, model);
        startActivity(intent);
    }

    @Override
    protected SimpleCursorAdapter.ViewBinder getAdapterBinder() {
        Map<Integer, WloItemListViewBinder.DataType> types = Maps.newHashMap();
        types.put(1, WloItemListViewBinder.DataType.SCIENTIFIC_SPECIES);
        return new WloItemListViewBinder(this, types);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_EDIT_WEIGHTS && resultCode == RESULT_OK) {
            getIntent().putExtra(INTENT_EXTRA_PARENT_MODEL, data.getSerializableExtra(WeightsActivity.INTENT_COMMERCIAL_SPECIES));

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}

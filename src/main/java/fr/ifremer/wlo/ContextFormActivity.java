package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.os.Bundle;
import fr.ifremer.wlo.models.ContextModel;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class ContextFormActivity extends WloModelEditionActivity<ContextModel> {

    private static final String TAG = "ContextFormActivity";

    @Override
    protected Integer getContentView() {
        return R.layout.context_form;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return ContextsActivity.class;
    }

    @Override
    protected Class<? extends WloModelEditionActivity> getNextEditionActivity() {
        return LocationFormActivity.class;
    }

    @Override
    protected Class<? extends WloBaseListActivity> getNextListActivity() {
        return LocationsActivity.class;
    }

    @Override
    protected ContextModel createNewModel() {
        return new ContextModel();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initEditText(R.id.context_form_name, ContextModel.COLUMN_NAME);
        initEditText(R.id.form_comment, ContextModel.COLUMN_COMMENT);

    }

}

package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.google.common.collect.Lists;
import fr.ifremer.wlo.models.VesselModel;
import fr.ifremer.wlo.models.referentials.Location;
import fr.ifremer.wlo.models.referentials.Vessel;
import fr.ifremer.wlo.storage.DataCache;
import fr.ifremer.wlo.utils.DatePickerFragment;
import fr.ifremer.wlo.utils.UIUtils;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class VesselFormActivity extends WloModelEditionActivity<VesselModel> {

    private static final String TAG = "VesselFormActivity";

    @Override
    protected Integer getContentView() {
        return R.layout.vessel_form;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return VesselsActivity.class;
    }

    @Override
    protected Class<? extends WloModelEditionActivity> getNextEditionActivity() {
        return MetierFormActivity.class;
    }

    @Override
    protected Class<? extends WloBaseListActivity> getNextListActivity() {
        return MetiersActivity.class;
    }

    @Override
    protected VesselModel createNewModel() {
        return new VesselModel();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // init editors
        Collection<Vessel> vessels = DataCache.getAllVessels(this);
        initAutoCompleteTextView(R.id.vessel_form_registration_number, VesselModel.COLUMN_REGISTRATION_NUMBER, vessels);

        initEditText(R.id.vessel_form_name, VesselModel.COLUMN_NAME);

        List<Location> locations = Lists.newArrayList(DataCache.getAllLocations(this));
        initAutoCompleteTextView(R.id.vessel_form_landing_location, VesselModel.COLUMN_LANDING_LOCATION, locations);

        EditText landingDateEditor = (EditText) findViewById(R.id.vessel_form_landing_date);
        // landing date
        Calendar landingDate = model.getLandingDate();
        if (landingDate != null) {
            String dateFormat = UIUtils.getDateFormat(this);
            landingDateEditor.setText(String.format(dateFormat, landingDate.getTime()));
        }

        initEditText(R.id.form_comment, VesselModel.COLUMN_COMMENT);
    }

    /* Method called by the view */

    public void pickLandingDate(View v) {
        DialogFragment newFragment = new DatePickerFragment(model, VesselModel.COLUMN_LANDING_DATE, (TextView) v);
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

}

package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import com.google.common.collect.Maps;
import fr.ifremer.wlo.models.CommercialSpeciesModel;
import fr.ifremer.wlo.utils.WloItemListViewBinder;

import java.util.Map;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class CommercialSpeciesActivity extends WloBaseListActivity<CommercialSpeciesModel> {

    private static final String TAG = "CommercialSpeciesActivity";

    /*  Activity methods  */

    @Override
    protected SimpleCursorAdapter createAdapter() {
        return new SimpleCursorAdapter(this, R.layout.commercial_species_list_item, null,
                                       new String[] {
                                               CommercialSpeciesModel.COLUMN_FAO_CODE,
                                               CommercialSpeciesModel.COLUMN_SORT_CATEGORY,
                                               CommercialSpeciesModel.COLUMN_STATE,
                                               CommercialSpeciesModel.COLUMN_PRESENTATION
                                       },
                                       new int[] {
                                           R.id.commercial_species_fao_code,
                                           R.id.commercial_species_sort_category,
                                           R.id.commercial_species_state,
                                           R.id.commercial_species_presentation
                                       }, 0);
    }

    @Override
    protected Cursor getAllData() {
        Cursor cursor = woh.getAllCommercialSpecies(parentModel.getId());
        return cursor;
    }

    @Override
    protected CommercialSpeciesModel createNewModel(Cursor cursor) {
        return new CommercialSpeciesModel(this, cursor);
    }

    @Override
    protected Class<? extends WloBaseActivity> getNextActivity() {
        return ScientificSpeciesActivity.class;
    }

    @Override
    protected Class<? extends WloModelEditionActivity> getEditionActivity() {
        return CommercialSpeciesFormActivity.class;
    }

    @Override
    protected Integer getSubtitle() {
        return R.string.commercial_species_subtitle;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return MetiersActivity.class;
    }

    @Override
    protected SimpleCursorAdapter.ViewBinder getAdapterBinder() {
        Map<Integer, WloItemListViewBinder.DataType> types = Maps.newHashMap();
        types.put(1, WloItemListViewBinder.DataType.COMMERCIAL_SPECIES);
        types.put(6, WloItemListViewBinder.DataType.STATE);
        types.put(7, WloItemListViewBinder.DataType.PRESENTATION);
        return new WloItemListViewBinder(this, types);
    }
}

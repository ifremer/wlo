package fr.ifremer.wlo.preferences;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.Context;
import com.google.common.collect.Lists;
import fr.ifremer.wlo.R;

import java.util.List;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public enum ListItemPreference {

    WEIGHT_UNIT("preferences_weight_unit", R.array.preferences_weight_unit_entries, R.array.preferences_weight_unit_values),
    DATE_FORMAT("preferences_date_format", R.array.preferences_date_format_entries, R.array.preferences_date_format_values),
    USE_PLACE("preferences_use_place", R.array.preferences_use_place_entries, R.array.preferences_use_place_values);

    private static final String TAG = "ListItemPreference";

    private String key;
    private int entriesArrayId;
    private int valuesArrayId;

    private ListItemPreference(String key, int entriesArrayId, int valuesArrayId) {
        this.key = key;
        this.entriesArrayId = entriesArrayId;
        this.valuesArrayId = valuesArrayId;
    }

    public String getKey() {
        return key;
    }

    public int getEntriesArrayId() {
        return entriesArrayId;
    }

    public int getValuesArrayId() {
        return valuesArrayId;
    }

    public String getEntryForValue(Context context, String value) {
        List<String> values = Lists.newArrayList(context.getResources().getStringArray(valuesArrayId));
        String[] entries = context.getResources().getStringArray(entriesArrayId);
        int index = values.indexOf(value);
        if (index < 0) {
            return null;
        }
        return entries[index];
    }

    public static ListItemPreference getListItemPreference(String key) {
        for (ListItemPreference pref : values()) {
            if (pref.getKey().equals(key)) {
                return pref;
            }
        }
        return null;
    }

}

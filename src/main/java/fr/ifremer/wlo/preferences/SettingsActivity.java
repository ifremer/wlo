package fr.ifremer.wlo.preferences;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.util.Log;
import android.widget.Toast;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Maps;
import fr.ifremer.wlo.MainActivity;
import fr.ifremer.wlo.R;
import fr.ifremer.wlo.WloBaseActivity;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.models.referentials.CommercialSpecies;
import fr.ifremer.wlo.models.referentials.ScientificSpecies;
import fr.ifremer.wlo.storage.DataCache;
import fr.ifremer.wlo.storage.StorageUtils;
import fr.ifremer.wlo.storage.WloSqlOpenHelper;
import fr.ifremer.wlo.utils.ImportExportUtil;
import fr.ifremer.wlo.utils.UIUtils;
import fr.ifremer.wlo.utils.UpdateCheckTask;
import fr.ifremer.wlo.utils.filechooser.FileDialog;
import fr.ifremer.wlo.utils.filechooser.SelectionMode;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class SettingsActivity extends WloBaseActivity {

    private static final String TAG = "SettingsActivity";

    protected static final int REQUEST_IMPORT_COMMERCIAL_SPECIES = 0;
    protected static final int REQUEST_IMPORT_LOCATIONS = 1;
    protected static final int REQUEST_IMPORT_MENSURATIONS = 2;
    protected static final int REQUEST_IMPORT_METIERS = 3;
    protected static final int REQUEST_IMPORT_PRESENTATIONS = 4;
    protected static final int REQUEST_IMPORT_SCIENTIFIC_SPECIES = 5;
    protected static final int REQUEST_IMPORT_STATES = 6;
    protected static final int REQUEST_IMPORT_VESSELS = 7;
    protected static final int REQUEST_IMPORT_CALCIFIED_PARTS_TAKINGS = 8;

    protected static final int REQUEST_EXPORT_COMMERCIAL_SPECIES = 10;
    protected static final int REQUEST_EXPORT_LOCATIONS = 11;
    protected static final int REQUEST_EXPORT_MENSURATIONS = 12;
    protected static final int REQUEST_EXPORT_METIERS = 13;
    protected static final int REQUEST_EXPORT_PRESENTATIONS = 14;
    protected static final int REQUEST_EXPORT_SCIENTIFIC_SPECIES = 15;
    protected static final int REQUEST_EXPORT_STATES = 16;
    protected static final int REQUEST_EXPORT_VESSELS = 17;
    protected static final int REQUEST_EXPORT_CALCIFIED_PARTS_TAKINGS = 18;

    protected static final int REQUEST_CREATE_CATEGORY = 20;
    protected static final int REQUEST_FAVORITES_COMMERCIAL_SPECIES = 21;
    protected static final int REQUEST_FAVORITES_SCIENTIFIC_SPECIES = 22;

    @Override
    protected Integer getContentView() {
        return null;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return MainActivity.class;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();

    }

    public static class SettingsFragment extends PreferenceFragment
                    implements SharedPreferences.OnSharedPreferenceChangeListener {

        // map pref_key / request code
        protected BiMap<String, Integer> requestCodeByPrefKey = HashBiMap.create();

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.preferences);

            // create map pref_key / request code
            requestCodeByPrefKey.put("import_commercial_species", REQUEST_IMPORT_COMMERCIAL_SPECIES);
            requestCodeByPrefKey.put("import_locations", REQUEST_IMPORT_LOCATIONS);
            requestCodeByPrefKey.put("import_mensurations", REQUEST_IMPORT_MENSURATIONS);
            requestCodeByPrefKey.put("import_metiers", REQUEST_IMPORT_METIERS);
            requestCodeByPrefKey.put("import_presentations", REQUEST_IMPORT_PRESENTATIONS);
            requestCodeByPrefKey.put("import_scientific_species", REQUEST_IMPORT_SCIENTIFIC_SPECIES);
            requestCodeByPrefKey.put("import_states", REQUEST_IMPORT_STATES);
            requestCodeByPrefKey.put("import_vessels", REQUEST_IMPORT_VESSELS);
            requestCodeByPrefKey.put("import_calcified_parts_takings", REQUEST_IMPORT_CALCIFIED_PARTS_TAKINGS);

            requestCodeByPrefKey.put("export_commercial_species", REQUEST_EXPORT_COMMERCIAL_SPECIES);
            requestCodeByPrefKey.put("export_locations", REQUEST_EXPORT_LOCATIONS);
            requestCodeByPrefKey.put("export_mensurations", REQUEST_EXPORT_MENSURATIONS);
            requestCodeByPrefKey.put("export_metiers", REQUEST_EXPORT_METIERS);
            requestCodeByPrefKey.put("export_presentations", REQUEST_EXPORT_PRESENTATIONS);
            requestCodeByPrefKey.put("export_scientific_species", REQUEST_EXPORT_SCIENTIFIC_SPECIES);
            requestCodeByPrefKey.put("export_states", REQUEST_EXPORT_STATES);
            requestCodeByPrefKey.put("export_vessels", REQUEST_EXPORT_VESSELS);
            requestCodeByPrefKey.put("export_calcified_parts_takings", REQUEST_EXPORT_CALCIFIED_PARTS_TAKINGS);

            Map<String, Integer> nbElementsByPrefKey = Maps.newHashMap();
            Context context = getActivity();
            nbElementsByPrefKey.put("import_commercial_species", DataCache.getAllCommercialSpecies(context).size());
            nbElementsByPrefKey.put("import_locations", DataCache.getAllLocations(context).size());
            nbElementsByPrefKey.put("import_mensurations", DataCache.getAllMensurations(context).size());
            nbElementsByPrefKey.put("import_metiers", DataCache.getAllMetiers(context).size());
            nbElementsByPrefKey.put("import_presentations", DataCache.getAllPresentations(context).size());
            nbElementsByPrefKey.put("import_scientific_species", DataCache.getAllScientificSpecies(context).size());
            nbElementsByPrefKey.put("import_states", DataCache.getAllStates(context).size());
            nbElementsByPrefKey.put("import_vessels", DataCache.getAllVessels(context).size());

            nbElementsByPrefKey.put("export_commercial_species", DataCache.getAllCommercialSpecies(context).size());
            nbElementsByPrefKey.put("export_locations", DataCache.getAllLocations(context).size());
            nbElementsByPrefKey.put("export_mensurations", DataCache.getAllMensurations(context).size());
            nbElementsByPrefKey.put("export_metiers", DataCache.getAllMetiers(context).size());
            nbElementsByPrefKey.put("export_presentations", DataCache.getAllPresentations(context).size());
            nbElementsByPrefKey.put("export_scientific_species", DataCache.getAllScientificSpecies(context).size());
            nbElementsByPrefKey.put("export_states", DataCache.getAllStates(context).size());
            nbElementsByPrefKey.put("export_vessels", DataCache.getAllVessels(context).size());

            for (String key : requestCodeByPrefKey.keySet()) {
                final Integer requestCode = requestCodeByPrefKey.get(key);
                Preference filePicker = findPreference(key);
                Integer nb = nbElementsByPrefKey.get(key);
                if (nb != null) {
                    filePicker.setSummary(getString(R.string.preferences_import_summary, nb));
                }
                filePicker.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        Intent intent = new Intent(getActivity(), FileDialog.class); //Intent to start openIntents File Manager
                        intent.putExtra(FileDialog.START_PATH, "/sdcard");
                        intent.putExtra(FileDialog.CAN_SELECT_DIR, requestCode >= REQUEST_EXPORT_COMMERCIAL_SPECIES);
                        intent.putExtra(FileDialog.SELECTION_MODE, SelectionMode.MODE_OPEN);

                        //alternatively you can set file filter
                        intent.putExtra(FileDialog.FORMAT_FILTER, new String[] { "csv" });

                        startActivityForResult(intent, requestCode);
                        return true;
                    }
                });
            }

            // categories
            Preference categoryPref = findPreference("import_categories");
            int nb = DataCache.getAllCategories(context).size();
            categoryPref.setSummary(getString(R.string.preferences_import_summary, nb));
            categoryPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent intent = new Intent(getActivity(), CategoryCreationAcivity.class);
                    startActivityForResult(intent, REQUEST_CREATE_CATEGORY);
                    return true;
                }
            });

            // update
            Preference updatePref = findPreference("preferences_check_update");
            try {
                String currentVersion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                updatePref.setSummary(getString(R.string.preferences_current_version, currentVersion));

            } catch (PackageManager.NameNotFoundException e) {
                Log.e(TAG, "error while getting the version");
            }
            updatePref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    new UpdateCheckTask(getActivity(), true).execute();
                    return true;
                }
            });

        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

            Preference preference = findPreference(key);
            String summary = null;

            ListItemPreference pref = ListItemPreference.getListItemPreference(key);
            if (pref != null) {
                String entry = sharedPreferences.getString(key, "");
                summary = pref.getEntryForValue(getActivity(), entry);

            } else {
                try {
                    summary = sharedPreferences.getString(key, "");
                } catch (ClassCastException e) {
                    // ok, that's normal, it is a list so we do not want to have a summary
                }
            }
            // Set summary to be the user-description for the selected value
            preference.setSummary(summary);
        }

        @Override
        public void onResume() {
            super.onResume();
            SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();

            for (StringPreference pref : StringPreference.values()) {
                String key = pref.getKey();
                Preference preference = findPreference(key);
                preference.setSummary(sharedPreferences.getString(key, ""));
            }

            for (ListItemPreference pref : ListItemPreference.values()) {
                String key = pref.getKey();
                Preference preference = findPreference(key);
                String entry = sharedPreferences.getString(key, "");
                String summary = pref.getEntryForValue(getActivity(), entry);
                preference.setSummary(summary);
            }

            // favorites
            initMultiSelectListPreference(MultiSelectItemPreference.COMMERCIAL_SPECIES_FAVORITES.getKey(),
                                          CommercialSpecies.class,
                                          REQUEST_FAVORITES_COMMERCIAL_SPECIES);

            initMultiSelectListPreference(MultiSelectItemPreference.SCIENTIFIC_SPECIES_FAVORITES.getKey(),
                                          ScientificSpecies.class,
                                          REQUEST_FAVORITES_SCIENTIFIC_SPECIES);

            sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceScreen().getSharedPreferences()
                    .unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
            Context context = getActivity();
            if (resultCode == RESULT_OK) {
                if (requestCode >= REQUEST_IMPORT_COMMERCIAL_SPECIES
                        && requestCode <= REQUEST_IMPORT_CALCIFIED_PARTS_TAKINGS) {

                    if (!StorageUtils.isExternalStorageWritable()) {
                        AlertDialog dialog = new AlertDialog.Builder(context)
                                .setTitle(R.string.preferences_import_no_external_storage_title)
                                .setMessage(R.string.preferences_import_no_external_storage_message)
                                .setCancelable(false)
                                .setNegativeButton(R.string.no, UIUtils.getCancelClickListener())
                                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        importData(requestCode, data, false);
                                    }
                                }).create();
                        dialog.show();

                    } else {
                        importData(requestCode, data, true);
                    }

                } else if (requestCode >= REQUEST_EXPORT_COMMERCIAL_SPECIES
                        && requestCode <= REQUEST_EXPORT_CALCIFIED_PARTS_TAKINGS) {

                        exportData(requestCode, data);

                } else if (requestCode == REQUEST_CREATE_CATEGORY) {
                    int nb = DataCache.getAllCategories(context).size();
                    Preference categoryPref = findPreference("import_categories");
                    categoryPref.setSummary(getString(R.string.preferences_import_summary, nb));

                } else if (requestCode == REQUEST_FAVORITES_SCIENTIFIC_SPECIES) {
                    saveMultiSelectListPreference(data, MultiSelectItemPreference.SCIENTIFIC_SPECIES_FAVORITES.getKey());

                } else if (requestCode == REQUEST_FAVORITES_COMMERCIAL_SPECIES) {
                    saveMultiSelectListPreference(data, MultiSelectItemPreference.COMMERCIAL_SPECIES_FAVORITES.getKey());
                }

            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }

        protected void importData(final int requestCode, final Intent data, final boolean backupFile) {
            String path = data.getStringExtra(FileDialog.RESULT_PATH);
            new ImportAsyncTask().execute(path, requestCode, backupFile);
        }

        protected void exportData(final int requestCode, final Intent data) {
            String path = data.getStringExtra(FileDialog.RESULT_PATH);
            new ExportAsyncTask().execute(path, requestCode);
        }

        protected <M extends BaseModel> void initMultiSelectListPreference(String prefKey,
                                                                           final Class clazz,
                                                                           final int requestCode) {

            Preference preference = findPreference(prefKey);
            SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();
            final ArrayList<String> data = new ArrayList<>(sharedPreferences.getStringSet(prefKey,
                                                                                          new HashSet<String>()));

            preference.setSummary(getString(R.string.favorite_nb, data.size()));
            preference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent intent = new Intent(getActivity(), MultiSelectionActivity.class);
                    intent.putExtra(MultiSelectionActivity.INTENT_UNIVERSE_CLASS, clazz);
                    intent.putExtra(MultiSelectionActivity.INTENT_FAVORITE_LIST, data);
                    startActivityForResult(intent, requestCode);
                    return true;
                }
            });
        }

        protected void saveMultiSelectListPreference(Intent data, String key) {
            Preference pref = findPreference(key);
            Set<String> favoriteIds = new HashSet<>(
                    data.getStringArrayListExtra(MultiSelectionActivity.INTENT_FAVORITE_LIST));
            SharedPreferences.Editor editor = getPreferenceManager().getSharedPreferences().edit();
            editor.putStringSet(key, favoriteIds);
            try {
                editor.apply();

            } catch (AbstractMethodError unused) {
                // The app injected its own pre-Gingerbread
                // SharedPreferences.Editor implementation without
                // an apply method.
                editor.commit();
            }
            pref.setSummary(getString(R.string.favorite_nb, favoriteIds.size()));
        }

        protected class ImportAsyncTask extends AsyncTask<Object, Integer, Integer[]> {

            protected ProgressDialog dialog;

            protected String errorMessage;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                dialog = new ProgressDialog(getActivity());
                dialog.setCancelable(false);
                dialog.setMessage(getString(R.string.preferences_importing_referential));
                dialog.show();
            }

            @Override
            protected Integer[] doInBackground(Object... params) {
                if (params.length != 3) {
                    return null;
                }
                Context context = getActivity();
                String path = (String) params[0];
                Integer requestCode = (Integer) params[1];
                Boolean backupFile = (Boolean) params[2];

                Integer[] result = new Integer[3];
                result[2] = requestCode;

                try {
                    switch (requestCode) {
                        case REQUEST_IMPORT_COMMERCIAL_SPECIES:
                            result[0] = ImportExportUtil.importCommercialSpecies(context, path);
                            result[1] = DataCache.getAllCommercialSpecies(context).size();
                            break;
                        case REQUEST_IMPORT_LOCATIONS:
                            result[0] = ImportExportUtil.importLocations(context, path);
                            result[1] = DataCache.getAllLocations(context).size();
                            break;
                        case REQUEST_IMPORT_MENSURATIONS:
                            result[0] = ImportExportUtil.importMensurations(context, path);
                            result[1] = DataCache.getAllMensurations(context).size();
                            break;
                        case REQUEST_IMPORT_METIERS:
                            result[0] = ImportExportUtil.importMetiers(context, path);
                            result[1] = DataCache.getAllMetiers(context).size();
                            break;
                        case REQUEST_IMPORT_PRESENTATIONS:
                            result[0] = ImportExportUtil.importPresentations(context, path);
                            result[1] = DataCache.getAllPresentations(context).size();
                            break;
                        case REQUEST_IMPORT_SCIENTIFIC_SPECIES:
                            result[0] = ImportExportUtil.importScientificSpecies(context, path);
                            result[1] = DataCache.getAllScientificSpecies(context).size();
                            break;
                        case REQUEST_IMPORT_STATES:
                            result[0] = ImportExportUtil.importStates(context, path);
                            result[1] = DataCache.getAllStates(context).size();
                            break;
                        case REQUEST_IMPORT_VESSELS:
                            result[0] = ImportExportUtil.importVessels(context, path);
                            result[1] = DataCache.getAllVessels(context).size();
                            break;
                        case REQUEST_IMPORT_CALCIFIED_PARTS_TAKINGS:
                            WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
                            soh.clearCalcifiedPartTakings();
                            soh.close();
                            result[0] = ImportExportUtil.importCalcifiedPartTakings(context, path);
                            result[1] = null;
                            break;
                        default:
                            result[0] = null;
                            result[1] = null;
                    }

                    if (backupFile) {
                        File importedFile = new File(path);
                        String copyFileName = String.format("%1$tY%1$tm%1$td-%1$tH%1$tM%1$tS-", new Date()) + importedFile.getName();
                        File dir = Environment.getExternalStoragePublicDirectory(".wlo");
                        File file = new File(dir, copyFileName);
                        try {
                            FileUtils.copyInputStreamToFile(new FileInputStream(path), file);
                        } catch (IOException e) {
                            Log.e(TAG, "Error while copying the file", e);
                        }
                    }

                } catch (Exception e) {
                    // cf issue #4205
                    // nuiton-i18n provocks an error when trying to get the error message
                    Log.e(TAG, "error during import " + requestCode, e);

                    if (requestCode == REQUEST_IMPORT_CALCIFIED_PARTS_TAKINGS) {
                        errorMessage = e.getMessage();

                    } else {
                        errorMessage = getString(R.string.import_error);
                    }
                    result = null;
                    cancel(true);

                }
                return result;
            }

            @Override
            protected void onPostExecute(Integer[] result) {
                super.onPostExecute(result);

                dialog.dismiss();

                if (result != null) {
                    if (result[1] != null) {
                        String key = requestCodeByPrefKey.inverse().get(result[2]);
                        Preference filePicker = findPreference(key);
                        filePicker.setSummary(getString(R.string.preferences_import_summary, result[1]));
                    }

                    Toast.makeText(getActivity(), getString(R.string.import_new_element_number, result[0]), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            protected void onCancelled(Integer[] integer) {
                super.onCancelled(integer);
                dialog.dismiss();

                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.error)
                        .setMessage(errorMessage)
                        .setNeutralButton(android.R.string.ok, UIUtils.getCancelClickListener())
                        .create().show();
            }

        }

        protected class ExportAsyncTask extends AsyncTask<Object, Integer, Integer> {

            protected ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                dialog = new ProgressDialog(getActivity());
                dialog.setCancelable(false);
                dialog.setMessage(getString(R.string.preferences_exporting_referential));
                dialog.show();
            }

            @Override
            protected Integer doInBackground(Object... params) {
                if (params.length != 2) {
                    return null;
                }
                Context context = getActivity();
                String path = (String) params[0];
                Integer requestCode = (Integer) params[1];

                Integer result;

                try {
                    switch (requestCode) {
                        case REQUEST_EXPORT_COMMERCIAL_SPECIES:
                            result = ImportExportUtil.exportCommercialSpecies(context, path);
                            break;
                        case REQUEST_EXPORT_LOCATIONS:
                            result = ImportExportUtil.exportLocations(context, path);
                            break;
                        case REQUEST_EXPORT_MENSURATIONS:
                            result = ImportExportUtil.exportMensurations(context, path);
                            break;
                        case REQUEST_EXPORT_METIERS:
                            result = ImportExportUtil.exportMetiers(context, path);
                            break;
                        case REQUEST_EXPORT_PRESENTATIONS:
                            result = ImportExportUtil.exportPresentations(context, path);
                            break;
                        case REQUEST_EXPORT_SCIENTIFIC_SPECIES:
                            result = ImportExportUtil.exportScientificSpecies(context, path);
                            break;
                        case REQUEST_EXPORT_STATES:
                            result = ImportExportUtil.exportStates(context, path);
                            break;
                        case REQUEST_EXPORT_VESSELS:
                            result = ImportExportUtil.exportVessels(context, path);
                            break;
                        case REQUEST_EXPORT_CALCIFIED_PARTS_TAKINGS:
                            WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
                            soh.clearCalcifiedPartTakings();
                            soh.close();
                            result = ImportExportUtil.exportCalcifiedPartTakings(context, path);
                            break;
                        default:
                            result = null;
                    }

                } catch (Exception e) {
                    // cf issue #4205
                    // nuiton-i18n provocks an error when trying to get the error message
                    Log.e(TAG, "error during import", e);
                    result = null;
                    cancel(true);

                }
                return result;
            }

            @Override
            protected void onPostExecute(Integer result) {
                super.onPostExecute(result);

                dialog.dismiss();

                if (result != null) {
                    Toast.makeText(getActivity(), getString(R.string.export_element_number, result), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            protected void onCancelled(Integer integer) {
                super.onCancelled(integer);
                dialog.dismiss();

                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.error)
                        .setMessage(R.string.export_error)
                        .setNeutralButton(android.R.string.ok, UIUtils.getCancelClickListener())
                        .create().show();
            }

        }
    }

}

package fr.ifremer.wlo.preferences;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.wlo.R;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.models.referentials.CommercialSpecies;
import fr.ifremer.wlo.models.referentials.ScientificSpecies;
import fr.ifremer.wlo.storage.DataCache;
import fr.ifremer.wlo.utils.WloAutoCompleteTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class MultiSelectionActivity extends ListActivity implements AdapterView.OnItemLongClickListener {

    private static final String TAG = "MultiSelectionActivity";

    public static final String INTENT_FAVORITE_LIST = "favoriteList";
    public static final String INTENT_UNIVERSE_CLASS = "universeClass";

    protected ArrayAdapter favoriteAdapter;
    protected ArrayAdapter universeAdapter;

    protected ArrayList<String> favoriteIds = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.favorite_selection);

        List<BaseModel> universe = new ArrayList<>();
        Class universeClass = (Class) getIntent().getSerializableExtra(INTENT_UNIVERSE_CLASS);
        if (ScientificSpecies.class.equals(universeClass)) {
            universe.addAll(DataCache.getAllScientificSpecies(this));

        } else if (CommercialSpecies.class.equals(universeClass)) {
            universe.addAll(DataCache.getAllCommercialSpecies(this));
        }

        final Map<String, BaseModel> modelsById = Maps.uniqueIndex(universe, BaseModel.GET_ID_FUNCTION);

        favoriteIds = getIntent().getStringArrayListExtra(INTENT_FAVORITE_LIST);
        List<BaseModel> favorites = new ArrayList<>(Lists.transform(favoriteIds, new Function<String, BaseModel>() {
            @Override
            public BaseModel apply(String input) {
                return modelsById.get(input);
            }
        }));
        favoriteAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, favorites);
        setListAdapter(favoriteAdapter);

        final WloAutoCompleteTextView search = (WloAutoCompleteTextView) findViewById(R.id.favorite_search);

        universe.removeAll(favorites);
        universeAdapter = new ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, universe);
        search.setAdapter(universeAdapter);

        search.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                BaseModel selected = (BaseModel) parent.getItemAtPosition(position);
                favoriteAdapter.add(selected);
                favoriteIds.add(selected.getId());
                search.setText(null);
                universeAdapter.remove(selected);
            }
        });

        getListView().setOnItemLongClickListener(this);
    }

    public void cancel(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    public void validate(View view) {
        Intent result = new Intent();
        result.putExtra(INTENT_FAVORITE_LIST, favoriteIds);
        setResult(RESULT_OK, result);
        finish();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        BaseModel selected = (BaseModel) parent.getItemAtPosition(position);
        favoriteAdapter.remove(selected);
        favoriteIds.remove(selected.getId());
        universeAdapter.add(selected);
        return true;
    }
}

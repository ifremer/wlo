package fr.ifremer.wlo.preferences;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import fr.ifremer.wlo.R;
import fr.ifremer.wlo.WloBaseActivity;
import fr.ifremer.wlo.models.categorization.CategoryModel;
import fr.ifremer.wlo.models.categorization.QualitativeValueModel;
import fr.ifremer.wlo.storage.DataCache;
import fr.ifremer.wlo.storage.WloSqlOpenHelper;
import fr.ifremer.wlo.utils.BaseTextWatcher;
import fr.ifremer.wlo.utils.ImportExportUtil;
import fr.ifremer.wlo.utils.UIUtils;
import fr.ifremer.wlo.utils.filechooser.FileDialog;
import fr.ifremer.wlo.utils.filechooser.SelectionMode;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class CategoryCreationAcivity extends WloBaseActivity {

    private static final String TAG = "CategoryCreationAcivity";

    protected static final int REQUEST_IMPORT_QUALITATIVE_VALUES = 0;

    protected CategoryModel model = new CategoryModel();

    @Override
    protected Integer getContentView() {
        return R.layout.category_creation_form;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EditText labelEditor = (EditText) findViewById(R.id.category_label);
        labelEditor.addTextChangedListener(new BaseTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                model.setLabel(String.valueOf(s));
            }
        });
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return null;
    }

    public void validate(View source) {
        WloSqlOpenHelper soh = new WloSqlOpenHelper(this);
        soh.saveData(model);
        if (model.getQualitativeValues() != null) {
            soh.saveData(model.getQualitativeValues());
        }

        DataCache.addCategory(model);

        setResult(RESULT_OK);
        finish();
    }

    public void importQualitativeValues(View source) {
        Intent intent = new Intent(this, FileDialog.class);
        intent.putExtra(FileDialog.START_PATH, "/sdcard");
        intent.putExtra(FileDialog.CAN_SELECT_DIR, false);
        intent.putExtra(FileDialog.SELECTION_MODE, SelectionMode.MODE_OPEN);

        //alternatively you can set file filter
        intent.putExtra(FileDialog.FORMAT_FILTER, new String[] { "csv" });

        startActivityForResult(intent, REQUEST_IMPORT_QUALITATIVE_VALUES);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMPORT_QUALITATIVE_VALUES && resultCode == RESULT_OK) {
            String path = data.getStringExtra(FileDialog.RESULT_PATH);
            try {
                Collection<QualitativeValueModel> qualitativeValueModels =
                        ImportExportUtil.importQualitativeValues(model, path);
                model.setQualitativeValues(new ArrayList<>(qualitativeValueModels));

                TextView textView = (TextView) findViewById(R.id.qualitative_values_nb_label);
                textView.setText(getString(R.string.qualitative_values_nb, qualitativeValueModels.size()));
                textView.setVisibility(View.VISIBLE);

            } catch (Exception e) {
                // cf issue #4205
                // nuiton-i18n provocks an error when trying to get the error message
                Log.e(TAG, "error during import", e);
                new AlertDialog.Builder(CategoryCreationAcivity.this)
                        .setMessage(R.string.import_error)
                        .setNeutralButton(android.R.string.ok, UIUtils.getCancelClickListener())
                        .create().show();
            }

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}

package fr.ifremer.wlo.models;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.database.Cursor;

import java.util.Set;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class ContextModel extends BaseModel {

    private static final String TAG = "Context";

    public static final String TABLE_NAME = "contexts";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_COMMENT = "comment";
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            COLUMN_NAME,
            COLUMN_COMMENT
    };

    protected String name;
    protected String comment;

    public ContextModel() {
    }

    public ContextModel(Cursor cursor) {
        super(cursor);
        name = cursor.getString(1);
        comment = cursor.getString(2);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldValue = this.name;
        this.name = name;
        firePropertyChange(COLUMN_NAME, oldValue, name);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        Object oldValue = this.comment;
        this.comment = comment;
        firePropertyChange(COLUMN_COMMENT, oldValue, comment);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String toString(android.content.Context context) {
        return name;
    }

    @Override
    public ContentValues convertIntoContentValues() {
        ContentValues value = super.convertIntoContentValues();
        putValue(value, COLUMN_NAME, name);
        putValue(value, COLUMN_COMMENT, comment);
        return value;
    }

    @Override
    public Set<String> getRequiredFields() {
        Set<String> result = super.getRequiredFields();
        result.add(COLUMN_NAME);
        return result;
    }
}

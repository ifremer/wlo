package fr.ifremer.wlo.models;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import fr.ifremer.wlo.R;
import fr.ifremer.wlo.models.referentials.Mensuration;
import fr.ifremer.wlo.utils.UIUtils;

import java.util.Calendar;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class MeasurementModel extends HierarchicalModel<ScientificSpeciesModel> {

    private static final String TAG = "MeasurementModel";

    public static final String TABLE_NAME = "measurements";
    public static final String COLUMN_SIZE = "size";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_CATEGORY_1 = "category1";
    public static final String COLUMN_CATEGORY_2 = "category2";
    public static final String COLUMN_CATEGORY_3 = "category3";
    public static final String COLUMN_SCIENTIFIC_SPECIES_ID = "scientificSpeciesId";
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            COLUMN_SIZE,
            COLUMN_DATE,
            COLUMN_CATEGORY_1,
            COLUMN_CATEGORY_2,
            COLUMN_CATEGORY_3,
            COLUMN_SCIENTIFIC_SPECIES_ID
    };

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    protected Integer size;
    protected Calendar date;
    protected String category1;
    protected String category2;
    protected String category3;

    public MeasurementModel() {
    }

    public MeasurementModel(Cursor cursor) {
        super(cursor);
        size = cursor.getInt(1);
        date = UIUtils.getCalendarFromCursor(cursor, 2);
        category1 = cursor.getString(3);
        category2 = cursor.getString(4);
        category3 = cursor.getString(5);
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        Integer oldValue = this.size;
        this.size = size;
        firePropertyChange(COLUMN_SIZE, oldValue, size);
    }

    public void incSize(int inc) {
        if (size != null) {
            setSize(size + inc);
        }
    }

    public void decSize(int dec) {
        if (size != null) {
            setSize(size - dec);
        }
    }

    public void roundSize(Mensuration.Precision precision) {
        if (size == null) {
            return;
        }
        int precisionValue = precision.getValue();
        int roundedSize = (size / precisionValue) * precisionValue;
        setSize(roundedSize);
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        Object oldValue = this.date;
        this.date = date;
        firePropertyChange(COLUMN_DATE, oldValue, date);
    }

    public String getCategory1() {
        return category1;
    }

    public void setCategory1(String category1) {
        Object oldValue = this.category1;
        this.category1 = category1;
        firePropertyChange(COLUMN_CATEGORY_1, oldValue, category1);
    }

    public String getCategory2() {
        return category2;
    }

    public void setCategory2(String category2) {
        Object oldValue = this.category2;
        this.category2 = category2;
        firePropertyChange(COLUMN_CATEGORY_2, oldValue, category2);
    }

    public String getCategory3() {
        return category3;
    }

    public void setCategory3(String category3) {
        Object oldValue = this.category3;
        this.category3 = category3;
        firePropertyChange(COLUMN_CATEGORY_3, oldValue, category3);
    }

    public String toString(Context context, Mensuration.Precision precision) {
        String dateFormat = UIUtils.getDateFormat(context) +
                " " + context.getString(R.string.fulltime_format);

        String result = String.format(dateFormat, date);
        result += " - " + UIUtils.getFormattedSize(size, precision);
        return result;
    }

    @Override
    public ContentValues convertIntoContentValues() {
        ContentValues value = super.convertIntoContentValues();
        putValue(value, COLUMN_SIZE, size);
        putValue(value, COLUMN_DATE, date != null ? date.getTimeInMillis() : null);
        putValue(value, COLUMN_CATEGORY_1, category1);
        putValue(value, COLUMN_CATEGORY_2, category2);
        putValue(value, COLUMN_CATEGORY_3, category3);
        putValue(value, COLUMN_SCIENTIFIC_SPECIES_ID, getParentId());
        return value;
    }
}

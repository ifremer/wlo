package fr.ifremer.wlo.models;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.database.Cursor;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 0.2
 */
public class CategoryWeightModel extends HierarchicalModel<ScientificSpeciesModel> {

    private static final String TAG = "CategoryWeightModel";

    public static final String TABLE_NAME = "category_weights";
    public static final String COLUMN_CATEGORY_1 = "category1";
    public static final String COLUMN_CATEGORY_2 = "category2";
    public static final String COLUMN_CATEGORY_3 = "category3";
    public static final String COLUMN_WEIGHT = "weight";
    public static final String COLUMN_SCIENTIFIC_SPECIES_ID = "scientificSpeciesId";
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            COLUMN_CATEGORY_1,
            COLUMN_CATEGORY_2,
            COLUMN_CATEGORY_3,
            COLUMN_WEIGHT,
            COLUMN_SCIENTIFIC_SPECIES_ID
    };

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    protected String category1;
    protected String category2;
    protected String category3;
    protected Integer weight;

    public CategoryWeightModel() {
    }

    public CategoryWeightModel(Cursor cursor) {
        super(cursor);
        category1 = cursor.getString(1);
        category2 = cursor.getString(2);
        category3 = cursor.getString(3);
        if (!cursor.isNull(4)) {
            weight = cursor.getInt(4);
        }
    }
    public String getCategory1() {
        return category1;
    }

    public void setCategory1(String category1) {
        Object oldValue = this.category1;
        this.category1 = category1;
        firePropertyChange(COLUMN_CATEGORY_1, oldValue, category1);
    }

    public String getCategory2() {
        return category2;
    }

    public void setCategory2(String category2) {
        Object oldValue = this.category2;
        this.category2 = category2;
        firePropertyChange(COLUMN_CATEGORY_2, oldValue, category2);
    }

    public String getCategory3() {
        return category3;
    }

    public void setCategory3(String category3) {
        Object oldValue = this.category3;
        this.category3 = category3;
        firePropertyChange(COLUMN_CATEGORY_3, oldValue, category3);
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        Object oldValue = this.weight;
        this.weight = weight;
        firePropertyChange(COLUMN_WEIGHT, oldValue, weight);
    }

    @Override
    public ContentValues convertIntoContentValues() {
        ContentValues value = super.convertIntoContentValues();
        putValue(value, COLUMN_CATEGORY_1, category1);
        putValue(value, COLUMN_CATEGORY_2, category2);
        putValue(value, COLUMN_CATEGORY_3, category3);
        putValue(value, COLUMN_WEIGHT, weight);
        putValue(value, COLUMN_SCIENTIFIC_SPECIES_ID, getParentId());
        return value;
    }

}

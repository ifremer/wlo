package fr.ifremer.wlo.models;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import fr.ifremer.wlo.models.referentials.Location;
import fr.ifremer.wlo.storage.DataCache;
import fr.ifremer.wlo.utils.UIUtils;

import java.util.Calendar;
import java.util.Set;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class LocationModel extends HierarchicalModel<ContextModel> {

    private static final String TAG = "Location";

    public static final String TABLE_NAME = "locations";
    public static final String COLUMN_OPERATOR = "operator";
    public static final String COLUMN_START_DATE = "startDate";
    public static final String COLUMN_END_DATE = "endDate";
    public static final String COLUMN_LOCATION = "location";
    public static final String COLUMN_COMMENT = "comment";
    public static final String COLUMN_CONTEXT_ID = "contextId";
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            COLUMN_OPERATOR,
            COLUMN_START_DATE,
            COLUMN_END_DATE,
            COLUMN_LOCATION,
            COLUMN_COMMENT,
            COLUMN_CONTEXT_ID
    };

    protected String operator;
    protected Calendar startDate;
    protected Calendar endDate;
    protected Location location;
    protected String comment;

    public LocationModel() {
    }

    public LocationModel(Context context, Cursor cursor) {
        super(cursor);
        operator = cursor.getString(1);
        startDate = UIUtils.getCalendarFromCursor(cursor, 2);
        endDate = UIUtils.getCalendarFromCursor(cursor, 3);
        String locationId = cursor.getString(4);
        location = DataCache.getLocationById(context, locationId);
        comment = cursor.getString(5);
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        Object oldValue = this.operator;
        this.operator = operator;
        firePropertyChange(COLUMN_OPERATOR, oldValue, operator);
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        Object oldValue = this.startDate;
        this.startDate = startDate;
        firePropertyChange(COLUMN_START_DATE, oldValue, startDate);
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(Calendar endDate) {
        Object oldValue = this.endDate;
        this.endDate = endDate;
        firePropertyChange(COLUMN_END_DATE, oldValue, endDate);
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        Object oldValue = this.location;
        this.location = location;
        firePropertyChange(COLUMN_LOCATION, oldValue, location);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        Object oldValue = this.comment;
        this.comment = comment;
        firePropertyChange(COLUMN_COMMENT, oldValue, comment);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String toString(android.content.Context context) {
        return UIUtils.getStringOrUndefined(location, context);
    }

    @Override
    public ContentValues convertIntoContentValues() {
        ContentValues value = super.convertIntoContentValues();
        putValue(value, COLUMN_OPERATOR, operator);
        putValue(value, COLUMN_START_DATE, startDate != null ? startDate.getTimeInMillis() : null);
        putValue(value, COLUMN_END_DATE, endDate != null ? endDate.getTimeInMillis() : null);
        putValue(value, COLUMN_LOCATION, location != null ? location.getId() : null);
        putValue(value, COLUMN_COMMENT, comment);
        putValue(value, COLUMN_CONTEXT_ID, getParentId());
        return value;
    }

    @Override
    public Set<String> getRequiredFields() {
        Set<String> result = super.getRequiredFields();
        result.add(COLUMN_LOCATION);
        return result;
    }
}

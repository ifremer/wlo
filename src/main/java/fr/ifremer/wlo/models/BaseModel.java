package fr.ifremer.wlo.models;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.util.Log;
import com.google.common.base.Function;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Set;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public abstract class BaseModel implements Serializable, BaseColumns {

    private static final String TAG = "BaseModel";

    public static final Function<BaseModel, String> GET_ID_FUNCTION = new Function<BaseModel, String>() {
        @Override
        public String apply(BaseModel input) {
            return input.getId();
        }
    };

    public enum ErrorType {
        REQUIRED,
        ONE_REQUIRED
    }

    protected PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    protected String id;

    protected boolean modified;

    public BaseModel() {
    }

    public BaseModel(Cursor cursor) {
        id = cursor.getString(0);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        String oldValue = this.id;
        this.id = id;
        firePropertyChange(_ID, oldValue, id);
    }

    public boolean isNew() {
        return StringUtils.isBlank(id);
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public abstract String getTableName();

    public String toString(android.content.Context context) {
        return toString();
    }

    public ContentValues convertIntoContentValues() {
        ContentValues value = new ContentValues();
        value.put(_ID, id);
        return value;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String property, PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(property, listener);
    }

    public void firePropertyChange(String property, Object oldValue, Object newValue) {
        if (ObjectUtils.notEqual(oldValue, newValue)) {
            changeSupport.firePropertyChange(property, oldValue, newValue);
        }
    }

    public Set<String> getRequiredFields() {
        return Sets.newHashSet();
    }

    /**
     * @return A set of sets containing fields. At least one of these fields must have a value.
     */
    public Set<Set<String>> getOneRequiredFields() {
        return Sets.newHashSet();
    }

    /**
     * Check if the model is valid
     * @return a map of the fields in error by error type
     */
    public Multimap<ErrorType, String> checkValidity() {
        Multimap<ErrorType, String> result = HashMultimap.create();
        // check required
        for (String requiredField : getRequiredFields()) {
            try {
                if (getClass().getDeclaredField(requiredField).get(this) == null) {
                    result.put(ErrorType.REQUIRED, requiredField);
                }
            } catch (Exception e) {
                Log.e(TAG, "Error while accessing the field " + requiredField, e);
            }
        }

        // check one required
        for (Set<String> oneRequiredFields : getOneRequiredFields()) {
            boolean notNull = false;
            for (String oneRequiredField : oneRequiredFields) {
                try {
                    if (getClass().getDeclaredField(oneRequiredField).get(this) != null) {
                        notNull = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Error while accessing the field " + oneRequiredField, e);
                }
            }
            if (!notNull) {
                result.putAll(ErrorType.ONE_REQUIRED, oneRequiredFields);
            }
        }

        return result;
    }

    protected void putValue(ContentValues values, String column, String value) {
        if (value == null) {
            values.putNull(column);
        } else {
            values.put(column, value);
        }
    }

    protected void putValue(ContentValues values, String column, Long value) {
        if (value == null) {
            values.putNull(column);
        } else {
            values.put(column, value);
        }
    }

    protected void putValue(ContentValues values, String column, Integer value) {
        if (value == null) {
            values.putNull(column);
        } else {
            values.put(column, value);
        }
    }

    protected void putValue(ContentValues values, String column, Float value) {
        if (value == null) {
            values.putNull(column);
        } else {
            values.put(column, value);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BaseModel baseModel = (BaseModel) o;
        if (id != null) {
            return id.equals(baseModel.id);
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : super.hashCode();
    }
}

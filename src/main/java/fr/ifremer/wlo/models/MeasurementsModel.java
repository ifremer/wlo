package fr.ifremer.wlo.models;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import fr.ifremer.wlo.models.categorization.QualitativeValueModel;
import fr.ifremer.wlo.models.referentials.CalcifiedPartTaking;
import fr.ifremer.wlo.models.referentials.Mensuration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class MeasurementsModel implements Serializable {

    private static final String TAG = "Measurements";

    protected ScientificSpeciesModel scientificSpecies;

    protected Mensuration.Precision precision;

    protected Multimap<Integer, MeasurementModel> measurements = HashMultimap.create();

    protected List<CalcifiedPartTaking> calcifiedPartTakings;

    protected Map<String, QualitativeValueModel> categoryValuesById = new HashMap<>();

    transient protected List<MeasurementsListener> listeners = new ArrayList<MeasurementsListener>();

    public ScientificSpeciesModel getScientificSpecies() {
        return scientificSpecies;
    }

    public void setScientificSpecies(ScientificSpeciesModel scientificSpecies) {
        this.scientificSpecies = scientificSpecies;
    }

    public Mensuration.Precision getPrecision() {
        return precision;
    }

    public void setPrecision(Mensuration.Precision precision) {
        this.precision = precision;
    }

    public Multimap<Integer, MeasurementModel> getMeasurements() {
        return measurements;
    }

    public List<MeasurementModel> getMeasurements(int size) {
        return Lists.newArrayList(measurements.get(size));
    }

    public int getMeasurementNb(int size) {
        return measurements.get(size).size();
    }

    public boolean addMeasurement(MeasurementModel fishMeasurement) {
        int size = fishMeasurement.getSize();
        measurements.put(size, fishMeasurement);
        fireMeasurementAdded(fishMeasurement);

        boolean result = false;
        if (calcifiedPartTakings != null) {
            CalcifiedPartTaking calcifiedPartTaking = null;
            for (CalcifiedPartTaking cpt : calcifiedPartTakings) {
                Integer startSize = cpt.getStartSize();
                Integer endSize = cpt.getEndSize();
                if ((startSize == null || startSize <= size)
                        && (endSize == null || endSize >= size)) {
                    calcifiedPartTaking = cpt;
                    break;
                }
            }
            if (calcifiedPartTaking != null) {
                int nb = measurements.get(size).size();
                int step = calcifiedPartTaking.getStep();
                Integer stop = calcifiedPartTaking.getStop();
                boolean stepOk = (nb - 1) % step == 0;
                boolean stopOk = stop == null || (nb - 1) / step < stop;
                result = nb == 1 || stepOk && stopOk;
            }

        }
        return result;
    }

    public void removeMeasurement(MeasurementModel fishMeasurement) {
        int size = fishMeasurement.getSize();
        measurements.remove(size, fishMeasurement);
        fireMeasurementRemoved(fishMeasurement);
    }

    public List<CalcifiedPartTaking> getCalcifiedPartTakings() {
        return calcifiedPartTakings;
    }

    public void setCalcifiedPartTakings(List<CalcifiedPartTaking> calcifiedPartTakings) {
        this.calcifiedPartTakings = calcifiedPartTakings;
    }

    public QualitativeValueModel getCategoryValuesById(String id) {
        return categoryValuesById.get(id);
    }

    public void addCategoryValues(Collection<QualitativeValueModel> categoryValues) {
        if (categoryValues != null) {
            categoryValuesById.putAll(Maps.uniqueIndex(categoryValues, BaseModel.GET_ID_FUNCTION));
        }
    }

    public void addMeasurementsListener(MeasurementsListener listener) {
        ensureListeners();
        listeners.add(listener);
    }

    public void removeMeasurementsListener(MeasurementsListener listener) {
        ensureListeners();
        listeners.remove(listener);
    }

    protected void fireMeasurementAdded(MeasurementModel measurement) {
        for (MeasurementsListener listener : listeners) {
            listener.onMeasurementAdded(this, measurement);
        }
    }

    protected void ensureListeners() {
        if (listeners == null) {
            listeners = Lists.newArrayList();
        }
    }

    protected void fireMeasurementRemoved(MeasurementModel measurement) {
        for (MeasurementsListener listener : listeners) {
            listener.onMeasurementRemoved(this, measurement);
        }
    }

    public static interface MeasurementsListener {

        void onMeasurementAdded(MeasurementsModel source, MeasurementModel measurement);

        void onMeasurementRemoved(MeasurementsModel source, MeasurementModel measurement);
    }

}

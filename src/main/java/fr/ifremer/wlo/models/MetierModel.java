package fr.ifremer.wlo.models;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import fr.ifremer.wlo.models.referentials.Metier;
import fr.ifremer.wlo.storage.DataCache;
import fr.ifremer.wlo.utils.UIUtils;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class MetierModel extends HierarchicalModel<VesselModel> {

    private static final String TAG = "Metier";

    public static final String TABLE_NAME = "metiers";
    public static final String COLUMN_GEAR_SPECIES = "gearSpecies";
    public static final String COLUMN_ZONE = "zone";
    public static final String COLUMN_SAMPLE_ROW_CODE = "sampleRowCode";
    public static final String COLUMN_COMMENT = "comment";
    public static final String COLUMN_VESSEL_ID = "vesselId";
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            COLUMN_GEAR_SPECIES,
            COLUMN_ZONE,
            COLUMN_SAMPLE_ROW_CODE,
            COLUMN_COMMENT,
            COLUMN_VESSEL_ID
    };

    protected Metier gearSpecies;
    protected String zone;
    protected String sampleRowCode;
    protected String comment;

    public MetierModel() {
    }

    public MetierModel(Context context, Cursor cursor) {
        super(cursor);
        String gearSpeciesId = cursor.getString(1);
        gearSpecies = DataCache.getMetierById(context, gearSpeciesId);
        zone = cursor.getString(2);
        sampleRowCode = cursor.getString(3);
        comment = cursor.getString(4);
    }

    public Metier getGearSpecies() {
        return gearSpecies;
    }

    public void setGearSpecies(Metier gearSpecies) {
        Object oldValue = this.gearSpecies;
        this.gearSpecies = gearSpecies;
        firePropertyChange(COLUMN_GEAR_SPECIES, oldValue, gearSpecies);
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        Object oldValue = this.zone;
        this.zone = zone;
        firePropertyChange(COLUMN_ZONE, oldValue, zone);
    }

    public String getSampleRowCode() {
        return sampleRowCode;
    }

    public void setSampleRowCode(String sampleRowCode) {
        Object oldValue = this.sampleRowCode;
        this.sampleRowCode = sampleRowCode;
        firePropertyChange(COLUMN_SAMPLE_ROW_CODE, oldValue, sampleRowCode);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        Object oldValue = this.comment;
        this.comment = comment;
        firePropertyChange(COLUMN_COMMENT, oldValue, comment);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String toString(Context context) {
        return UIUtils.getStringOrUndefined(gearSpecies, context);
    }

    @Override
    public ContentValues convertIntoContentValues() {
        ContentValues value = super.convertIntoContentValues();
        putValue(value, COLUMN_GEAR_SPECIES, gearSpecies != null ? gearSpecies.getId() : null);
        putValue(value, COLUMN_ZONE, zone);
        putValue(value, COLUMN_SAMPLE_ROW_CODE, sampleRowCode);
        putValue(value, COLUMN_COMMENT, comment);
        putValue(value, COLUMN_VESSEL_ID, getParentId());
        return value;
    }

}

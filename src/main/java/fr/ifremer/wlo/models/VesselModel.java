package fr.ifremer.wlo.models;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.google.common.collect.Sets;
import fr.ifremer.wlo.models.referentials.Location;
import fr.ifremer.wlo.models.referentials.Vessel;
import fr.ifremer.wlo.storage.DataCache;
import fr.ifremer.wlo.utils.UIUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Calendar;
import java.util.Set;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class VesselModel extends HierarchicalModel<LocationModel> {

    private static final String TAG = "Vessel";

    public static final String TABLE_NAME = "vessels";
    public static final String COLUMN_REGISTRATION_NUMBER = "registrationNumber";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_LANDING_DATE = "landingDate";
    public static final String COLUMN_LANDING_LOCATION = "landingLocation";
    public static final String COLUMN_COMMENT = "comment";
    public static final String COLUMN_LOCATION_ID = "location_id";
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            COLUMN_REGISTRATION_NUMBER,
            COLUMN_NAME,
            COLUMN_LANDING_DATE,
            COLUMN_LANDING_LOCATION,
            COLUMN_COMMENT,
            COLUMN_LOCATION_ID
    };

    protected String registrationNumber;
    protected String name;
    protected Calendar landingDate;
    protected Location landingLocation;
    protected String comment;

    public VesselModel() {
    }

    public VesselModel(Context context, Cursor cursor) {
        super(cursor);
        registrationNumber = cursor.getString(1);
        name = cursor.getString(2);
        landingDate = UIUtils.getCalendarFromCursor(cursor, 3);
        String landingLocationId = cursor.getString(4);
        landingLocation = DataCache.getLocationById(context, landingLocationId);
        comment = cursor.getString(5);
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        Object oldValue = this.registrationNumber;
        this.registrationNumber = registrationNumber;
        firePropertyChange(COLUMN_REGISTRATION_NUMBER, oldValue, registrationNumber);
    }

    public void setRegistrationNumber(Vessel vessel) {
        if (vessel != null) {
            setRegistrationNumber(vessel.getCode());
            if (StringUtils.isEmpty(name)) {
                setName(vessel.getName());
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        Object oldValue = this.name;
        this.name = name;
        firePropertyChange(COLUMN_NAME, oldValue, name);
    }

    public Calendar getLandingDate() {
        return landingDate;
    }

    public void setLandingDate(Calendar landingDate) {
        Object oldValue = this.landingDate;
        this.landingDate = landingDate;
        firePropertyChange(COLUMN_LANDING_DATE, oldValue, landingDate);
    }

    public Location getLandingLocation() {
        return landingLocation;
    }

    public void setLandingLocation(Location landingLocation) {
        Object oldValue = this.landingLocation;
        this.landingLocation = landingLocation;
        firePropertyChange(COLUMN_LANDING_LOCATION, oldValue, landingLocation);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        Object oldValue = this.comment;
        this.comment = comment;
        firePropertyChange(COLUMN_COMMENT, oldValue, comment);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String toString(android.content.Context context) {
        return UIUtils.getStringOrUndefined(registrationNumber, context) + " - "
                + UIUtils.getStringOrUndefined(name, context);
    }

    @Override
    public ContentValues convertIntoContentValues() {
        ContentValues value = super.convertIntoContentValues();
        putValue(value, COLUMN_REGISTRATION_NUMBER, registrationNumber);
        putValue(value, COLUMN_NAME, name);
        putValue(value, COLUMN_LANDING_DATE, landingDate != null ? landingDate.getTimeInMillis() : null);
        putValue(value, COLUMN_LANDING_LOCATION, landingLocation != null ? landingLocation.getId() : null);
        putValue(value, COLUMN_COMMENT, comment);
        putValue(value, COLUMN_LOCATION_ID, getParentId());
        return value;
    }

    @Override
    public Set<Set<String>> getOneRequiredFields() {
        Set<Set<String>> result = super.getOneRequiredFields();
        Set<String> oneRequired = Sets.newHashSet(COLUMN_REGISTRATION_NUMBER, COLUMN_NAME);
        result.add(oneRequired);
        return result;
    }
}

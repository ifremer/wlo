package fr.ifremer.wlo.models;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import fr.ifremer.wlo.models.referentials.ScientificSpecies;
import fr.ifremer.wlo.storage.DataCache;
import fr.ifremer.wlo.utils.UIUtils;
import org.apache.commons.lang3.tuple.Triple;

import java.util.Map;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class ScientificSpeciesModel extends HierarchicalModel<CommercialSpeciesModel> {

    private static final String TAG = "ScientificSpecies";

    public static final String TABLE_NAME = "scientific_species";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_TAKING_ACTIVATION = "takingActivation";
    public static final String COLUMN_COMMENT = "comment";
    public static final String COLUMN_SORTED_WEIGHT = "sortedWeight";
    public static final String COLUMN_SAMPLE_WEIGHT = "sampleWeight";
    public static final String COLUMN_COMMERCIAL_SPECIES_ID = "commercialSpeciesId";
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            COLUMN_NAME,
            COLUMN_TAKING_ACTIVATION,
            COLUMN_COMMENT,
            COLUMN_SORTED_WEIGHT,
            COLUMN_SAMPLE_WEIGHT,
            COLUMN_COMMERCIAL_SPECIES_ID
    };

    protected ScientificSpecies name;
    protected boolean takingActivation;
    protected String comment;
    protected Integer sortedWeight;
    protected Integer sampleWeight;

    // used for the export
    protected Map<Triple<String, String, String>, Integer> categoryWeights;

    public ScientificSpeciesModel() {
    }

    public ScientificSpeciesModel(Context context, Cursor cursor) {
        super(cursor);
        String nameId = cursor.getString(1);
        name = DataCache.getScientificSpeciesById(context, nameId);
        takingActivation = cursor.getShort(2) > 0;
        comment = cursor.getString(3);
        if (!cursor.isNull(4)) {
            sortedWeight = cursor.getInt(4);
        }
        if (!cursor.isNull(5)) {
            sampleWeight = cursor.getInt(5);
        }
    }

    public ScientificSpecies getName() {
        return name;
    }

    public void setName(ScientificSpecies name) {
        Object oldValue = this.name;
        this.name = name;
        firePropertyChange(COLUMN_NAME, oldValue, name);
    }

    public boolean isTakingActivation() {
        return takingActivation;
    }

    public void setTakingActivation(boolean takingActivation) {
        Object oldValue = this.takingActivation;
        this.takingActivation = takingActivation;
        firePropertyChange(COLUMN_TAKING_ACTIVATION, oldValue, takingActivation);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        Object oldValue = this.comment;
        this.comment = comment;
        firePropertyChange(COLUMN_COMMENT, oldValue, comment);
    }

    public Integer getSortedWeight() {
        return sortedWeight;
    }

    public void setSortedWeight(Integer sortedWeight) {
        Object oldValue = this.sortedWeight;
        this.sortedWeight = sortedWeight;
        firePropertyChange(COLUMN_SORTED_WEIGHT, oldValue, sortedWeight);
    }

    public Integer getSampleWeight() {
        return sampleWeight;
    }

    public void setSampleWeight(Integer sampleWeight) {
        Object oldValue = this.sampleWeight;
        this.sampleWeight = sampleWeight;
        firePropertyChange(COLUMN_SAMPLE_WEIGHT, oldValue, sampleWeight);
    }

    public Map<Triple<String, String, String>, Integer> getCategoryWeights() {
        return categoryWeights;
    }

    public void setCategoryWeights(Map<Triple<String, String, String>, Integer> categoryWeights) {
        this.categoryWeights = categoryWeights;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String toString(android.content.Context context) {
        return UIUtils.getStringOrUndefined(name, context);
    }

    @Override
    public ContentValues convertIntoContentValues() {
        ContentValues value = super.convertIntoContentValues();
        putValue(value, COLUMN_NAME, name != null ? name.getId() : null);
        putValue(value, COLUMN_TAKING_ACTIVATION, takingActivation ? 1 : 0);
        putValue(value, COLUMN_COMMENT, comment);
        putValue(value, COLUMN_SORTED_WEIGHT, sortedWeight);
        putValue(value, COLUMN_SAMPLE_WEIGHT, sampleWeight);
        putValue(value, COLUMN_COMMERCIAL_SPECIES_ID, getParentId());
        return value;
    }
}

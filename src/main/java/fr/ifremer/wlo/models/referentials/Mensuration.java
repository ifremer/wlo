package fr.ifremer.wlo.models.referentials;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.database.Cursor;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.utils.UIUtils;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class Mensuration extends BaseModel implements HasCode {

    private static final String TAG = "Mensuration";

    public enum Precision {
        CM1(10, 10, "1 cm"),
        MM5(5, 10, "0.5 cm"),
        MM1(1, 1, "1 mm");

        private int value;
        private int unitDivider;
        private String label;

        private Precision(int value, int unitDivider, String label) {
            this.value = value;
            this.unitDivider = unitDivider;
            this.label = label;
        }

        public int getValue() {
            return value;
        }

        public int getUnitDivider() {
            return unitDivider;
        }

        public String getLabel() {
            return label;
        }

        @Override
        public String toString() {
            return label;
        }

        public boolean isDecimal() {
            return value % unitDivider > 0;
        }
    }

    public static final String TABLE_NAME = "ref_mensurations";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_LABEL = "label";
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            COLUMN_CODE,
            COLUMN_LABEL
    };

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    protected String code;
    protected String label;

    public Mensuration() {
    }

    public Mensuration(Cursor cursor) {
        super(cursor);
        code = cursor.getString(1);
        label = cursor.getString(2);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return label;
    }

    @Override
    public String toString(android.content.Context context) {
        return UIUtils.getStringOrUndefined(label, context);
    }

    @Override
    public ContentValues convertIntoContentValues() {
        ContentValues value = super.convertIntoContentValues();
        putValue(value, COLUMN_CODE, code);
        putValue(value, COLUMN_LABEL, label);
        return value;
    }
}

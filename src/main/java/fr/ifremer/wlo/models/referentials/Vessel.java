package fr.ifremer.wlo.models.referentials;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.utils.UIUtils;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class Vessel extends BaseModel implements HasCode {

    private static final String TAG = "Vessel";

    public static final String TABLE_NAME = "ref_vessel";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_QUARTER_CODE = "quarterCode";
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            COLUMN_CODE,
            COLUMN_NAME,
            COLUMN_QUARTER_CODE
    };

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    protected String code;
    protected String name;
    protected String quarterCode;

    public Vessel() {
    }

    public Vessel(Cursor cursor) {
        super(cursor);
        code = cursor.getString(1);
        name = cursor.getString(2);
        quarterCode = cursor.getString(3);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuarterCode() {
        return quarterCode;
    }

    public void setQuarterCode(String quarterCode) {
        this.quarterCode = quarterCode;
    }

    @Override
    public String toString() {
        return code + " - " + name;
    }

    @Override
    public String toString(Context context) {
        return UIUtils.getStringOrUndefined(code, context) + " - " +
                UIUtils.getStringOrUndefined(name, context);
    }

    @Override
    public ContentValues convertIntoContentValues() {
        ContentValues value = super.convertIntoContentValues();
        putValue(value, COLUMN_CODE, code);
        putValue(value, COLUMN_NAME, name);
        putValue(value, COLUMN_QUARTER_CODE, quarterCode);
        return value;
    }
}

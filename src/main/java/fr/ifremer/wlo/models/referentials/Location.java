package fr.ifremer.wlo.models.referentials;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.database.Cursor;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.utils.UIUtils;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class Location extends BaseModel implements HasCode {

    private static final String TAG = "Locations";

    public static final String TABLE_NAME = "ref_location";
    public static final String COLUMN_TYPE_LABEL = "typeLabel";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_LABEL = "label";
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            COLUMN_TYPE_LABEL,
            COLUMN_CODE,
            COLUMN_LABEL
    };

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    protected String typeLabel;
    protected String code;
    protected String label;

    public Location() {
    }

    public Location(Cursor cursor) {
        super(cursor);
        typeLabel = cursor.getString(1);
        code = cursor.getString(2);
        label = cursor.getString(3);
    }

    public String getTypeLabel() {
        return typeLabel;
    }

    public void setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return code + " - " + label + " (" + typeLabel  + ")";
    }

    @Override
    public String toString(android.content.Context context) {
        return UIUtils.getStringOrUndefined(code, context) + " - " +
                UIUtils.getStringOrUndefined(label, context) + " ("
                + UIUtils.getStringOrUndefined(typeLabel, context) + ")";
    }

    @Override
    public ContentValues convertIntoContentValues() {
        ContentValues value = super.convertIntoContentValues();
        putValue(value, COLUMN_TYPE_LABEL, typeLabel);
        putValue(value, COLUMN_CODE, code);
        putValue(value, COLUMN_LABEL, label);
        return value;
    }
}

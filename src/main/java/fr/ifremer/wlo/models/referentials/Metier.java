package fr.ifremer.wlo.models.referentials;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.database.Cursor;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.utils.UIUtils;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class Metier extends BaseModel implements HasCode {

    private static final String TAG = "Metier";

    public static final String TABLE_NAME = "ref_metiers";
    public static final String COLUMN_ID = "metierId";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_LABEL = "label";
    public static final String COLUMN_GEAR_CODE = "gearCode";
    public static final String COLUMN_GEAR_LABEL = "gearLabel";
    public static final String COLUMN_SPECIES_CODE = "speciesCode";
    public static final String COLUMN_SPECIES_LABEL = "speciesLabel";
    public static final String COLUMN_FISHING = "fishing";
    public static final String COLUMN_ACTIVE = "active";
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            COLUMN_ID,
            COLUMN_CODE,
            COLUMN_LABEL,
            COLUMN_GEAR_CODE,
            COLUMN_GEAR_LABEL,
            COLUMN_SPECIES_CODE,
            COLUMN_SPECIES_LABEL,
            COLUMN_FISHING,
            COLUMN_ACTIVE
    };

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    protected String metierId;
    protected String code;
    protected String label;
    protected String gearCode;
    protected String gearLabel;
    protected String speciesCode;
    protected String speciesLabel;
    protected int fishing;
    protected int active;

    public Metier() {
    }

    public Metier(Cursor cursor) {
        super(cursor);
        metierId = cursor.getString(1);
        code = cursor.getString(2);
        label = cursor.getString(3);
        gearCode = cursor.getString(4);
        gearLabel = cursor.getString(5);
        speciesCode = cursor.getString(6);
        speciesLabel = cursor.getString(7);
        fishing = cursor.getInt(8);
        active = cursor.getInt(9);
    }

    public String getMetierId() {
        return metierId;
    }

    public void setMetierId(String metierId) {
        this.metierId = metierId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getGearCode() {
        return gearCode;
    }

    public void setGearCode(String gearCode) {
        this.gearCode = gearCode;
    }

    public String getGearLabel() {
        return gearLabel;
    }

    public void setGearLabel(String gearLabel) {
        this.gearLabel = gearLabel;
    }

    public String getSpeciesCode() {
        return speciesCode;
    }

    public void setSpeciesCode(String speciesCode) {
        this.speciesCode = speciesCode;
    }

    public String getSpeciesLabel() {
        return speciesLabel;
    }

    public void setSpeciesLabel(String speciesLabel) {
        this.speciesLabel = speciesLabel;
    }

    public int getFishing() {
        return fishing;
    }

    public void setFishing(int fishing) {
        this.fishing = fishing;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return label;
    }

    @Override
    public String toString(android.content.Context context) {
        return UIUtils.getStringOrUndefined(label, context);
    }

    @Override
    public ContentValues convertIntoContentValues() {
        ContentValues value = super.convertIntoContentValues();
        putValue(value, COLUMN_ID, metierId);
        putValue(value, COLUMN_CODE, code);
        putValue(value, COLUMN_LABEL, label);
        putValue(value, COLUMN_GEAR_CODE, gearCode);
        putValue(value, COLUMN_GEAR_LABEL, gearLabel);
        putValue(value, COLUMN_SPECIES_CODE, speciesCode);
        putValue(value, COLUMN_SPECIES_LABEL, speciesLabel);
        putValue(value, COLUMN_FISHING, fishing);
        putValue(value, COLUMN_ACTIVE, active);
        return value;
    }
}

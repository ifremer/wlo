package fr.ifremer.wlo.models.referentials;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.database.Cursor;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.utils.UIUtils;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class CommercialSpecies extends BaseModel implements HasCode {

    private static final String TAG = "CommercialSpecies";

    public static final String TABLE_NAME = "ref_commercial_species";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_ISSCAP = "isscap";
    public static final String COLUMN_TAXON_CODE = "taxonCode";
    public static final String COLUMN_SCIENTIFIC_LABEL = "scientificLabel";
    public static final String COLUMN_FRENCH_LABEL = "frenchLabel";
    public static final String COLUMN_FAMILY = "family";
    public static final String COLUMN_SPECIES_ORDER = "speciesOrder";
    public static final String COLUMN_ACTIVE = "active";
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            COLUMN_CODE,
            COLUMN_ISSCAP,
            COLUMN_TAXON_CODE,
            COLUMN_SCIENTIFIC_LABEL,
            COLUMN_FRENCH_LABEL,
            COLUMN_FAMILY,
            COLUMN_SPECIES_ORDER,
            COLUMN_ACTIVE
    };

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    protected String code;
    protected String isscap;
    protected String taxonCode;
    protected String scientificLabel;
    protected String frenchLabel;
    protected String family;
    protected String speciesOrder;
    protected int active;

    public CommercialSpecies() {
    }

    public CommercialSpecies(Cursor cursor) {
        super(cursor);
        code = cursor.getString(1);
        isscap = cursor.getString(2);
        taxonCode = cursor.getString(3);
        scientificLabel = cursor.getString(4);
        frenchLabel = cursor.getString(5);
        family = cursor.getString(6);
        speciesOrder = cursor.getString(7);
        active = cursor.getInt(8);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIsscap() {
        return isscap;
    }

    public void setIsscap(String isscap) {
        this.isscap = isscap;
    }

    public String getTaxonCode() {
        return taxonCode;
    }

    public void setTaxonCode(String taxonCode) {
        this.taxonCode = taxonCode;
    }

    public String getScientificLabel() {
        return scientificLabel;
    }

    public void setScientificLabel(String scientificLabel) {
        this.scientificLabel = scientificLabel;
    }

    public String getFrenchLabel() {
        return frenchLabel;
    }

    public void setFrenchLabel(String frenchLabel) {
        this.frenchLabel = frenchLabel;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getSpeciesOrder() {
        return speciesOrder;
    }

    public void setSpeciesOrder(String speciesOrder) {
        this.speciesOrder = speciesOrder;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return code + " - " + frenchLabel;
    }

    @Override
    public String toString(android.content.Context context) {
        return UIUtils.getStringOrUndefined(code, context) + " - " +
                UIUtils.getStringOrUndefined(frenchLabel, context);
    }

    @Override
    public ContentValues convertIntoContentValues() {
        ContentValues value = super.convertIntoContentValues();
        putValue(value, COLUMN_CODE, code);
        putValue(value, COLUMN_ISSCAP, isscap);
        putValue(value, COLUMN_TAXON_CODE, taxonCode);
        putValue(value, COLUMN_SCIENTIFIC_LABEL, scientificLabel);
        putValue(value, COLUMN_FRENCH_LABEL, frenchLabel);
        putValue(value, COLUMN_FAMILY, family);
        putValue(value, COLUMN_SPECIES_ORDER, speciesOrder);
        putValue(value, COLUMN_ACTIVE, active);
        return value;
    }
}

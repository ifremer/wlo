package fr.ifremer.wlo.models.referentials;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import org.apache.commons.lang3.ObjectUtils;

import java.util.Comparator;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public interface HasCode {

    String getCode();

    public static final Comparator<HasCode> GET_CODE_COMPARATOR = new Comparator<HasCode>() {
        @Override
        public int compare(HasCode lhs, HasCode rhs) {
            return ObjectUtils.compare(lhs.getCode(), rhs.getCode(), true);
        }
    };

    public static final Function<HasCode, String> GET_CODE_FUNCTION = new Function<HasCode, String>() {
        @Override
        public String apply(HasCode input) {
            return input.getCode();
        }
    };
}

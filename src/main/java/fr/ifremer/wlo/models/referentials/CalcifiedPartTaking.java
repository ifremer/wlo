package fr.ifremer.wlo.models.referentials;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.database.Cursor;
import fr.ifremer.wlo.models.HierarchicalModel;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class CalcifiedPartTaking extends HierarchicalModel<ScientificSpecies> {

    private static final String TAG = "CalcifiedPartTaking";

    public static final String TABLE_NAME = "calcifiedPartTaking";
    public static final String COLUMN_START_SIZE = "startSize";
    public static final String COLUMN_END_SIZE = "endSize";
    public static final String COLUMN_SIZE_STEP = "sizeStep";
    public static final String COLUMN_STEP = "step";
    public static final String COLUMN_STOP = "stop";
    public static final String COLUMN_SCIENTIFIC_SPECIES_ID = "scientificSpeciesId";
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            COLUMN_START_SIZE,
            COLUMN_END_SIZE,
            COLUMN_SIZE_STEP,
            COLUMN_STEP,
            COLUMN_STOP,
            COLUMN_SCIENTIFIC_SPECIES_ID
    };

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    protected Integer startSize;
    protected Integer endSize;
    protected Integer sizeStep;
    protected int step;
    protected Integer stop;

    public CalcifiedPartTaking() {
    }

    public CalcifiedPartTaking(Cursor cursor) {
        super(cursor);
        if (!cursor.isNull(1)) {
            startSize = cursor.getInt(1);
        }
        if (!cursor.isNull(2)) {
            endSize = cursor.getInt(2);
        }
        if (!cursor.isNull(3)) {
            sizeStep = cursor.getInt(3);
        }
        step = cursor.getInt(4);
        if (!cursor.isNull(5)) {
            stop = cursor.getInt(5);
        }
    }

    public Integer getStartSize() {
        return startSize;
    }

    public void setStartSize(Integer startSize) {
        this.startSize = startSize;
    }

    public Integer getEndSize() {
        return endSize;
    }

    public void setEndSize(Integer endSize) {
        this.endSize = endSize;
    }

    public Integer getSizeStep() {
        return sizeStep;
    }

    public void setSizeStep(Integer sizeStep) {
        this.sizeStep = sizeStep;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public Integer getStop() {
        return stop;
    }

    public void setStop(Integer stop) {
        this.stop = stop;
    }

    @Override
    public ContentValues convertIntoContentValues() {
        ContentValues value = super.convertIntoContentValues();
        putValue(value, COLUMN_START_SIZE, startSize);
        putValue(value, COLUMN_END_SIZE, endSize);
        putValue(value, COLUMN_SIZE_STEP, sizeStep);
        putValue(value, COLUMN_STEP, step);
        putValue(value, COLUMN_STOP, stop);
        putValue(value, COLUMN_SCIENTIFIC_SPECIES_ID, getParentId());
        return value;
    }
}

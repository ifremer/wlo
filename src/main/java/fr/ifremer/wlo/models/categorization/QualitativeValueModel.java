package fr.ifremer.wlo.models.categorization;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.database.Cursor;
import fr.ifremer.wlo.models.BaseModel;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class QualitativeValueModel extends BaseModel {

    private static final String TAG = "QualitativeValueModel";

    public static final String TABLE_NAME = "qualitativeValues";
    public static final String COLUMN_VALUE = "value";
    public static final String COLUMN_LABEL = "label";
    public static final String COLUMN_CATEGORY_ID = "categoryId";
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            COLUMN_VALUE,
            COLUMN_LABEL,
            COLUMN_CATEGORY_ID
    };

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    protected String value;
    protected String label;
    protected CategoryModel category;

    public QualitativeValueModel() {
    }

    public QualitativeValueModel(Cursor cursor) {
        super(cursor);
        value = cursor.getString(1);
        label = cursor.getString(2);
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public CategoryModel getCategory() {
        return category;
    }

    public void setCategory(CategoryModel category) {
        this.category = category;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return StringUtils.isNotEmpty(label) ? label : value;
    }

    @Override
    public ContentValues convertIntoContentValues() {
        ContentValues value = super.convertIntoContentValues();
        putValue(value, COLUMN_VALUE, this.value);
        putValue(value, COLUMN_LABEL, label);
        putValue(value, COLUMN_CATEGORY_ID, category != null ? category.getId() : null);
        return value;
    }
}

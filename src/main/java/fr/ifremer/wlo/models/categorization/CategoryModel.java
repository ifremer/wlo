package fr.ifremer.wlo.models.categorization;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.database.Cursor;
import fr.ifremer.wlo.models.BaseModel;

import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class CategoryModel extends BaseModel {

    private static final String TAG = "CategoryModel";

    public static final String TABLE_NAME = "categories";
    public static final String COLUMN_LABEL = "label";
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            COLUMN_LABEL
    };

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    protected String label;
    protected List<QualitativeValueModel> qualitativeValues;

    public CategoryModel() {
    }

    public CategoryModel(Cursor cursor) {
        super(cursor);
        label = cursor.getString(1);
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<QualitativeValueModel> getQualitativeValues() {
        return qualitativeValues;
    }

    public void setQualitativeValues(List<QualitativeValueModel> qualitativeValues) {
        this.qualitativeValues = qualitativeValues;
    }

    @Override
    public String toString() {
        return label;
    }

    @Override
    public ContentValues convertIntoContentValues() {
        ContentValues value = super.convertIntoContentValues();
        putValue(value, COLUMN_LABEL, label);
        return value;
    }
}

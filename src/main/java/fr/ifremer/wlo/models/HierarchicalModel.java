package fr.ifremer.wlo.models;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.database.Cursor;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public abstract class HierarchicalModel<P extends BaseModel> extends BaseModel {

    protected P parent;

    public HierarchicalModel() {}

    public HierarchicalModel(Cursor cursor) {
        id = cursor.getString(0);
    }

    public P getParent() {
        return parent;
    }

    public <T extends BaseModel> T getParent(Class<T> parentClass) {
        BaseModel result = this;
        while(result != null && !result.getClass().isAssignableFrom(parentClass)) {
            result = ((HierarchicalModel) result).getParent();
        }
        return (T) result;
    }

    public void setParent(P parent) {
        this.parent = parent;
    }

    public String getParentId() {
        return parent != null ? parent.getId() : null;
    }

}

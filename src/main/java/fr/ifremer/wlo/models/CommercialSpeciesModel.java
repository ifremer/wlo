package fr.ifremer.wlo.models;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import fr.ifremer.wlo.models.categorization.CategoryModel;
import fr.ifremer.wlo.models.referentials.CommercialSpecies;
import fr.ifremer.wlo.models.referentials.Mensuration;
import fr.ifremer.wlo.models.referentials.Presentation;
import fr.ifremer.wlo.models.referentials.State;
import fr.ifremer.wlo.storage.DataCache;
import fr.ifremer.wlo.utils.UIUtils;

import java.util.Set;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class CommercialSpeciesModel extends HierarchicalModel<MetierModel> {

    private static final String TAG = "CommercialSpecies";

    public static final String TABLE_NAME = "commercial_species";
    public static final String COLUMN_FAO_CODE = "faoCode";
    public static final String COLUMN_MEASUREMENT_METHOD = "measurementMethod";
    public static final String COLUMN_PRECISION = "precision";
    public static final String COLUMN_SPECIES_MIX = "speciesMix";
    public static final String COLUMN_SORT_CATEGORY = "sortCategory";
    public static final String COLUMN_STATE = "state";
    public static final String COLUMN_PRESENTATION = "presentation";
    public static final String COLUMN_CATEGORY1 = "category1";
    public static final String COLUMN_CATEGORY2 = "category2";
    public static final String COLUMN_CATEGORY3 = "category3";
    public static final String COLUMN_COMMENT = "comment";
    public static final String COLUMN_TOTAL_UNLOADED_WEIGHT = "totalUnloadedWeight";
    public static final String COLUMN_METIER_ID = "metierId";
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            COLUMN_FAO_CODE,
            COLUMN_MEASUREMENT_METHOD,
            COLUMN_PRECISION,
            COLUMN_SPECIES_MIX,
            COLUMN_SORT_CATEGORY,
            COLUMN_STATE,
            COLUMN_PRESENTATION,
            COLUMN_CATEGORY1,
            COLUMN_CATEGORY2,
            COLUMN_CATEGORY3,
            COLUMN_COMMENT,
            COLUMN_TOTAL_UNLOADED_WEIGHT,
            COLUMN_METIER_ID
    };

    protected CommercialSpecies faoCode;
    protected Mensuration measurementMethod;
    protected Mensuration.Precision precision = Mensuration.Precision.CM1;
    protected boolean speciesMix;
    protected String sortCategory;
    protected State state;
    protected Presentation presentation;
    protected CategoryModel category1;
    protected CategoryModel category2;
    protected CategoryModel category3;
    protected String comment;
    protected Integer totalUnloadedWeight;

    public CommercialSpeciesModel() {
    }

    public CommercialSpeciesModel(Context context, Cursor cursor) {
        super(cursor);
        String faoCodeId = cursor.getString(1);
        faoCode = DataCache.getCommercialSpeciesById(context, faoCodeId);
        String measurementMethodId = cursor.getString(2);
        measurementMethod = DataCache.getMensurationById(context, measurementMethodId);
        precision = Mensuration.Precision.valueOf(cursor.getString(3));
        speciesMix = cursor.getShort(4) > 0;
        sortCategory = cursor.getString(5);
        String stateId = cursor.getString(6);
        state = DataCache.getStateById(context, stateId);
        String presentationId = cursor.getString(7);
        presentation = DataCache.getPresentationById(context, presentationId);
        String category1Id = cursor.getString(8);
        category1 = DataCache.getCategoryById(context, category1Id);
        String category2Id = cursor.getString(9);
        category2 = DataCache.getCategoryById(context, category2Id);
        String category3Id = cursor.getString(10);
        category3 = DataCache.getCategoryById(context, category3Id);
        comment = cursor.getString(11);
        if (!cursor.isNull(12)) {
            totalUnloadedWeight = cursor.getInt(12);
        }
    }

    public CommercialSpecies getFaoCode() {
        return faoCode;
    }

    public void setFaoCode(CommercialSpecies faoCode) {
        CommercialSpecies oldValue = this.faoCode;
        this.faoCode = faoCode;
        firePropertyChange(COLUMN_FAO_CODE, oldValue, faoCode);
    }

    public Mensuration getMeasurementMethod() {
        return measurementMethod;
    }

    public void setMeasurementMethod(Mensuration measurementMethod) {
        Mensuration oldValue = this.measurementMethod;
        this.measurementMethod = measurementMethod;
        firePropertyChange(COLUMN_MEASUREMENT_METHOD, oldValue, measurementMethod);
    }

    public Mensuration.Precision getPrecision() {
        return precision;
    }

    public void setPrecision(Mensuration.Precision precision) {
        Mensuration.Precision oldValue = this.precision;
        this.precision = precision;
        firePropertyChange(COLUMN_PRECISION, oldValue, precision);
    }

    public boolean isSpeciesMix() {
        return speciesMix;
    }

    public void setSpeciesMix(boolean speciesMix) {
        boolean oldValue = this.speciesMix;
        this.speciesMix = speciesMix;
        firePropertyChange(COLUMN_SPECIES_MIX, oldValue, speciesMix);
    }

    public String getSortCategory() {
        return sortCategory;
    }

    public void setSortCategory(String sortCategory) {
        String oldValue = this.sortCategory;
        this.sortCategory = sortCategory;
        firePropertyChange(COLUMN_SORT_CATEGORY, oldValue, sortCategory);
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        State oldValue = this.state;
        this.state = state;
        firePropertyChange(COLUMN_STATE, oldValue, state);
    }

    public Presentation getPresentation() {
        return presentation;
    }

    public void setPresentation(Presentation presentation) {
        String oldValue = this.id;
        this.presentation = presentation;
        firePropertyChange(_ID, oldValue, id);
    }

    public CategoryModel getCategory1() {
        return category1;
    }

    public void setCategory1(CategoryModel category1) {
        this.category1 = category1;
    }

    public CategoryModel getCategory2() {
        return category2;
    }

    public void setCategory2(CategoryModel category2) {
        this.category2 = category2;
    }

    public CategoryModel getCategory3() {
        return category3;
    }

    public void setCategory3(CategoryModel category3) {
        this.category3 = category3;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        Object oldValue = this.comment;
        this.comment = comment;
        firePropertyChange(COLUMN_COMMENT, oldValue, comment);
    }

    public Integer getTotalUnloadedWeight() {
        return totalUnloadedWeight;
    }

    public void setTotalUnloadedWeight(Integer totalUnloadedWeight) {
        Object oldValue = this.totalUnloadedWeight;
        this.totalUnloadedWeight = totalUnloadedWeight;
        firePropertyChange(COLUMN_TOTAL_UNLOADED_WEIGHT, oldValue, totalUnloadedWeight);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String toString(android.content.Context context) {
        return UIUtils.getStringOrUndefined(faoCode, context);
    }

    @Override
    public ContentValues convertIntoContentValues() {
        ContentValues value = super.convertIntoContentValues();
        putValue(value, COLUMN_FAO_CODE, faoCode != null ? faoCode.getId() : null);
        putValue(value, COLUMN_MEASUREMENT_METHOD, measurementMethod != null ? measurementMethod.getId() : null);
        putValue(value, COLUMN_PRECISION, precision.name());
        putValue(value, COLUMN_SPECIES_MIX, speciesMix ? 1 : 0);
        putValue(value, COLUMN_SORT_CATEGORY, sortCategory);
        putValue(value, COLUMN_STATE, state != null ? state.getId() : null);
        putValue(value, COLUMN_PRESENTATION, presentation != null ? presentation.getId() : null);
        putValue(value, COLUMN_CATEGORY1, category1 != null ? category1.getId() : null);
        putValue(value, COLUMN_CATEGORY2, category2 != null ? category2.getId() : null);
        putValue(value, COLUMN_CATEGORY3, category3 != null ? category3.getId() : null);
        putValue(value, COLUMN_COMMENT, comment);
        putValue(value, COLUMN_TOTAL_UNLOADED_WEIGHT, totalUnloadedWeight);
        putValue(value, COLUMN_METIER_ID, getParentId());
        return value;
    }

    @Override
    public Set<String> getRequiredFields() {
        Set<String> result = super.getRequiredFields();
        result.add(COLUMN_FAO_CODE);
        result.add(COLUMN_MEASUREMENT_METHOD);
        result.add(COLUMN_PRECISION);
        return result;
    }
}

package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public abstract class WloBaseActivity extends ActionBarActivity implements ServiceConnection {

    private static final String TAG = "WloBaseActivity";

    protected Messenger mServiceMessenger = null;
    protected Messenger mMessenger = null;

    protected DownloadManager downloadManager;
    protected static Long downloadingApkId = null;

    protected final BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(downloadingApkId);
            Cursor cursor = downloadManager.query(query);

            if(cursor.moveToFirst()){
                int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                int status = cursor.getInt(columnIndex);
                int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
                int reason = cursor.getInt(columnReason);

                if(status == DownloadManager.STATUS_SUCCESSFUL){
                    //Retrieve the saved download id
                    Uri uri = downloadManager.getUriForDownloadedFile(downloadingApkId);

                    Intent promptInstall = new Intent(Intent.ACTION_VIEW)
                            .setDataAndType(uri, "application/vnd.android.package-archive");
                    startActivity(promptInstall);

                } else if (status == DownloadManager.STATUS_FAILED){
                    Toast.makeText(WloBaseActivity.this,
                                   R.string.update_dowload_error,
                                   Toast.LENGTH_LONG).show();
                }
                downloadingApkId = null;
            }
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        downloadManager = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);

        Integer viewId = getContentView();
        if (viewId != null) {
            setContentView(viewId);
        }

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(getUpActivity() != null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindService(new Intent(this, BigFinCommunicationService.class), this, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(downloadReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(downloadReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        doUnbindService();
    }

    protected void doUnbindService() {
        // If we have received the service, and hence registered with it, then now is the time to unregister.
        if (mServiceMessenger != null && mMessenger != null) {
            try {
                Message msg = Message.obtain(null, BigFinCommunicationService.MESSAGE_UNREGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mServiceMessenger.send(msg);

            } catch (RemoteException e) {
                Log.e(TAG, "Error while sending data to the service");
            }
        }
        // Detach our existing connection.
        unbindService(this);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mServiceMessenger = new Messenger(service);
        if (mMessenger != null) {
            try {
                Message msg = Message.obtain(null, BigFinCommunicationService.MESSAGE_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mServiceMessenger.send(msg);
            }
            catch (RemoteException e) {
                Log.e(TAG, "Error while sending data to the service");
            }
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mServiceMessenger = null;
    }

    public void cancel(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    protected abstract Integer getContentView();

    protected abstract Class<? extends WloBaseActivity> getUpActivity();

    @Override
    public Intent getSupportParentActivityIntent() {
        Class upClass = getUpActivity();
        if (upClass == null) {
            return super.getSupportParentActivityIntent();
        }
        Intent intent = new Intent(this, upClass);
        return intent;
    }

    public static Long getDownloadingApkId() {
        return downloadingApkId;
    }

    public static void setDownloadingApkId(Long downloadingApkId) {
        WloBaseActivity.downloadingApkId = downloadingApkId;
    }
}

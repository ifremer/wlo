package fr.ifremer.wlo.measurement;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import fr.ifremer.wlo.R;
import fr.ifremer.wlo.models.referentials.Mensuration;
import fr.ifremer.wlo.utils.UIUtils;

import java.util.TreeMap;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class TableAdapter extends BaseAdapter {

    private static final String TAG = "TableAdapter";

    protected TreeMap<Integer, Integer> data = new TreeMap<>();
    protected Context context;
    protected Mensuration.Precision precision;

    public TableAdapter(Context context, Mensuration.Precision precision) {
        this.context = context;
        this.precision = precision;
    }

    /* BaseAdapter methods */

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        if (position < 0 || position >= data.size()) {
            return null;
        }
        return data.keySet().toArray()[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                                        .inflate(R.layout.measurement_table_row, null);

            viewHolder = new ViewHolder();
            viewHolder.sizeText = (TextView) convertView.findViewById(R.id.table_size);
            viewHolder.nbText = (TextView) convertView.findViewById(R.id.table_nb);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Integer size = (Integer) getItem(position);
        viewHolder.sizeText.setText(UIUtils.getFormattedSize(size, precision));
        viewHolder.nbText.setText(String.valueOf(data.get(size)));

        int color;
        if (position % 2 == 0) {
            color = R.color.gray;
        } else {
            color = android.R.color.black;
        }
        convertView.setBackgroundResource(color);

        return convertView;
    }

    public void set(int size, int nb) {
        if (nb == 0) {
            data.remove(size);
        } else {
            data.put(size, nb);
        }
        notifyDataSetChanged();
    }

    /* Cache for the views of the table items */
    static class ViewHolder {
        TextView sizeText;
        TextView nbText;
    }

}

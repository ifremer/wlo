package fr.ifremer.wlo.measurement;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import fr.ifremer.tutti.ichtyometer.feed.record.FeedReaderMeasureRecord;
import fr.ifremer.wlo.BigFinCommunicationService;
import fr.ifremer.wlo.CommercialSpeciesActivity;
import fr.ifremer.wlo.CommercialSpeciesFormActivity;
import fr.ifremer.wlo.MainActivity;
import fr.ifremer.wlo.MetierFormActivity;
import fr.ifremer.wlo.MetiersActivity;
import fr.ifremer.wlo.R;
import fr.ifremer.wlo.ScientificSpeciesActivity;
import fr.ifremer.wlo.ScientificSpeciesFormActivity;
import fr.ifremer.wlo.VesselFormActivity;
import fr.ifremer.wlo.VesselsActivity;
import fr.ifremer.wlo.WloBaseActivity;
import fr.ifremer.wlo.WloBaseListActivity;
import fr.ifremer.wlo.WloModelEditionActivity;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.models.CategoryWeightModel;
import fr.ifremer.wlo.models.CommercialSpeciesModel;
import fr.ifremer.wlo.models.LocationModel;
import fr.ifremer.wlo.models.MeasurementModel;
import fr.ifremer.wlo.models.MeasurementsModel;
import fr.ifremer.wlo.models.MetierModel;
import fr.ifremer.wlo.models.ScientificSpeciesModel;
import fr.ifremer.wlo.models.VesselModel;
import fr.ifremer.wlo.models.categorization.CategoryModel;
import fr.ifremer.wlo.models.categorization.QualitativeValueModel;
import fr.ifremer.wlo.models.referentials.CalcifiedPartTaking;
import fr.ifremer.wlo.models.referentials.Mensuration;
import fr.ifremer.wlo.storage.WloSqlOpenHelper;
import fr.ifremer.wlo.utils.BaseTextWatcher;
import fr.ifremer.wlo.utils.UIUtils;
import org.apache.commons.collections4.CollectionUtils;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class MeasurementActivity extends WloBaseActivity implements AdapterView.OnItemSelectedListener, TextToSpeech.OnInitListener {

    private static final String TAG = "MeasurementActivity";

    public static final String INTENT_EXTRA_MEASUREMENTS = "measurements";
    public static final String INTENT_EXTRA_SCIENTIFIC_SPECIES = "scientificSpecies";

    protected static final String GRAPH_TAB = "graph";
    protected static final String TABLE_TAB = "table";
    protected static final String LOGS_TAB = "logs";

    protected static final int CHECK_TTS_REQUEST_CODE = 42;

    protected WloSqlOpenHelper soh = new WloSqlOpenHelper(this);

    protected TabHost tabs;
    protected EditText sizeText;
    protected ActionBarDrawerToggle mDrawerToggle;
    protected TextView observedNumberText;
    protected Button validateButton;

    protected MeasurementModel measurement;
    protected MeasurementsModel measurements;

    protected ScientificSpeciesModel scientificSpecies;
    protected CommercialSpeciesModel commercialSpecies;
    protected MetierModel metier;
    protected VesselModel vessel;
    protected LocationModel location;

    protected ListView mDrawerList;

    protected TextToSpeech textToSpeech;
    protected boolean useTextToSpeech = false;

    @Override
    protected Integer getContentView() {
        return R.layout.measurement;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return ScientificSpeciesActivity.class;
    }

    /* Activity methods */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        measurements = (MeasurementsModel) getIntent().getSerializableExtra(INTENT_EXTRA_MEASUREMENTS);
        if (measurements != null) {
            scientificSpecies = measurements.getScientificSpecies();

        } else {
            scientificSpecies = (ScientificSpeciesModel) getIntent().getSerializableExtra(INTENT_EXTRA_SCIENTIFIC_SPECIES);
            measurements = new MeasurementsModel();
            measurements.setScientificSpecies(scientificSpecies);

            Cursor cursor = soh.getAllMeasurements(scientificSpecies.getId());
            List<MeasurementModel> measurementList = WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                    new Function<Cursor, MeasurementModel>() {
                        @Override
                        public MeasurementModel apply(Cursor cursor) {
                            return new MeasurementModel(cursor);
                        }
                    });
            for (MeasurementModel measurement : measurementList) {
                measurements.addMeasurement(measurement);
            }
        }
        fetchCalcifiedPartTakings();

        measurements.addMeasurementsListener(new MeasurementsModel.MeasurementsListener() {
            @Override
            public void onMeasurementAdded(MeasurementsModel source, MeasurementModel measurement) {
                int size = source.getMeasurements().size();
                observedNumberText.setText(String.valueOf(size));
            }

            @Override
            public void onMeasurementRemoved(MeasurementsModel source, MeasurementModel measurement) {
                soh.deleteMeasurement(measurement);
                int size = source.getMeasurements().size();
                observedNumberText.setText(String.valueOf(size));
            }
        });

        commercialSpecies = scientificSpecies.getParent();
        measurements.setPrecision(commercialSpecies.getPrecision());
        metier = commercialSpecies.getParent();
        vessel = metier.getParent();
        location = vessel.getParent();

        tabs = (TabHost)findViewById(android.R.id.tabhost);

        tabs.setup();

        setupTab(R.id.graph, GRAPH_TAB, R.string.graph_tab);
        setupTab(R.id.table, TABLE_TAB, R.string.table_tab);
        setupTab(R.id.logs, LOGS_TAB, R.string.logs_tab);

        validateButton = (Button) findViewById(R.id.measurement_add_button);

        initSizeText();

        observedNumberText = (TextView) findViewById(R.id.observed_number);
        observedNumberText.setText(String.valueOf(measurements.getMeasurements().size()));

        initCategories();

        initFishMeasurement(10 * measurements.getPrecision().getUnitDivider(), null, null, null);

        initDrawer();

        initActionBar();

        mMessenger = new Messenger(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (!MeasurementActivity.this.isFinishing()) {
                    switch (msg.what) {
                        case BigFinCommunicationService.MESSAGE_READ:
                            FeedReaderMeasureRecord record = (FeedReaderMeasureRecord) msg.obj;

                            if (record != null && record.isValid()) {

                                int size = record.getMeasure();
                                measurement.setSize(size);
                                MeasurementModel savedMeasurement = addMeasurement();

                                if (useTextToSpeech) {
                                    String formattedSize = UIUtils.getFormattedSize(savedMeasurement.getSize(), measurements.getPrecision());
                                    textToSpeech.speak(formattedSize, TextToSpeech.QUEUE_ADD, null);

                                } else {
                                    ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, ToneGenerator.MAX_VOLUME);
                                    tg.startTone(ToneGenerator.TONE_PROP_BEEP);
                                }

                                // Get instance of Vibrator from current Context
                                Vibrator v = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                                // Vibrate for 300 milliseconds
                                v.vibrate(300);
                            }
                            break;
                    }
                }
            }
        });

        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkIntent, CHECK_TTS_REQUEST_CODE);
    }

    protected void initActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(commercialSpecies.toString(this) + " / " +
                                   scientificSpecies.toString(this) + " / " +
                                   commercialSpecies.getMeasurementMethod().toString(this) + " / " +
                                   commercialSpecies.getPrecision());
        actionBar.setSubtitle(vessel.toString(this) + " / " +
                                      metier.toString(this));
    }

    protected void initDrawer() {
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // Set the adapter for the list view
        setDrawerListAdapter();

        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                selectItem(position);
            }
        });
        mDrawerList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                editItem(position);
                return true;
            }
        });

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        );

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    protected void initSizeText() {
        sizeText = (EditText) findViewById(R.id.size);
        int inputType = InputType.TYPE_CLASS_NUMBER;
        if (measurements.getPrecision().isDecimal()) {
            inputType |= InputType.TYPE_NUMBER_FLAG_DECIMAL;
        }
        sizeText.setRawInputType(inputType);
        sizeText.addTextChangedListener(new BaseTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                sizeText.setSelection(start + count);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    measurement.setSize(null);

                } else {
                    Mensuration.Precision precision = measurements.getPrecision();
                    int multiplier = precision.getUnitDivider();
                    try {
                        NumberFormat format = NumberFormat.getInstance(Locale.US);
                        Number number = format.parse(s.toString());
                        double d = number.doubleValue();
                        int size = (int)(d * multiplier);
                        measurement.setSize(size);

                    } catch (ParseException e) {
                        Log.e(TAG, "ParseException " + e.getMessage());
                        measurement.setSize(null);
                    }
                }
            }
        });
    }

    protected void initCategories() {
        CategoryModel category1 = commercialSpecies.getCategory1();
        TextView category1Label = (TextView) findViewById(R.id.form_category1_label);
        LinearLayout category1Editors = (LinearLayout) findViewById(R.id.category1_panel);
        Spinner category1Spinner = (Spinner) findViewById(R.id.category1_spinner);
        EditText category1Text = (EditText) findViewById(R.id.category1_text);
        initCategoryInput(category1, category1Label, category1Editors, category1Spinner, category1Text);

        CategoryModel category2 = commercialSpecies.getCategory2();
        TextView category2Label = (TextView) findViewById(R.id.form_category2_label);
        LinearLayout category2Editors = (LinearLayout) findViewById(R.id.category2_panel);
        Spinner category2Spinner = (Spinner) findViewById(R.id.category2_spinner);
        EditText category2Text = (EditText) findViewById(R.id.category2_text);
        initCategoryInput(category2, category2Label, category2Editors, category2Spinner, category2Text);

        CategoryModel category3 = commercialSpecies.getCategory3();
        TextView category3Label = (TextView) findViewById(R.id.form_category3_label);
        LinearLayout category3Editors = (LinearLayout) findViewById(R.id.category3_panel);
        Spinner category3Spinner = (Spinner) findViewById(R.id.category3_spinner);
        EditText category3Text = (EditText) findViewById(R.id.category3_text);
        initCategoryInput(category3, category3Label, category3Editors, category3Spinner, category3Text);
    }

    protected void initCategoryInput(CategoryModel category, TextView categoryLabel, LinearLayout categoryEditorPanel,
                                     Spinner categorySpinner, final EditText categoryText) {
        if (category == null) {
            categoryLabel.setVisibility(View.INVISIBLE);
            categoryEditorPanel.setVisibility(View.INVISIBLE);

        } else {
            categoryLabel.setText(category.getLabel());
            List values = Lists.newArrayList(category.getQualitativeValues());
            if (values.isEmpty()) {
                categorySpinner.setVisibility(View.GONE);
                categoryText.addTextChangedListener(new BaseTextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        categoryText.setSelection(start + count);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        switch(categoryText.getId()) {
                            case R.id.category1_text:
                                measurement.setCategory1(String.valueOf(s));
                                break;
                            case R.id.category2_text:
                                measurement.setCategory2(String.valueOf(s));
                                break;
                            case R.id.category3_text:
                                measurement.setCategory3(String.valueOf(s));
                                break;
                        }
                    }
                });

            } else {
                categoryText.setVisibility(View.GONE);
                categorySpinner.setOnItemSelectedListener(this);
                measurements.addCategoryValues(values);
                ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, values);
                categorySpinner.setAdapter(adapter);
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CHECK_TTS_REQUEST_CODE && resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
            // Succès, au moins un moteur de TTS à été trouvé, on l'instancie
            textToSpeech = new TextToSpeech(this, this);
            if (textToSpeech.isLanguageAvailable(Locale.FRANCE) == TextToSpeech.LANG_COUNTRY_AVAILABLE) {
                textToSpeech.setLanguage(Locale.FRANCE);
            }

        } else if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 1:
                    vessel = (VesselModel) data.getSerializableExtra(WloModelEditionActivity.INTENT_EXTRA_MODEL);
                    break;
                case 2:
                    metier = (MetierModel) data.getSerializableExtra(WloModelEditionActivity.INTENT_EXTRA_MODEL);
                    break;
                case 3:
                    commercialSpecies = (CommercialSpeciesModel) data.getSerializableExtra(WloModelEditionActivity.INTENT_EXTRA_MODEL);
                    measurements.setPrecision(commercialSpecies.getPrecision());
                    initCategories();
                    break;
                case 4:
                    scientificSpecies = (ScientificSpeciesModel) data.getSerializableExtra(WloModelEditionActivity.INTENT_EXTRA_MODEL);
                    fetchCalcifiedPartTakings();
                    break;
            }
            setDrawerListAdapter();

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        soh.close();
        textToSpeech.shutdown();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        BaseModel selected = (BaseModel) adapterView.getItemAtPosition(i);
        String id = selected != null ? selected.getId() : null;
        switch(adapterView.getId()) {
            case R.id.category1_spinner:
                measurement.setCategory1(id);
                break;
            case R.id.category2_spinner:
                measurement.setCategory2(id);
                break;
            case R.id.category3_spinner:
                measurement.setCategory3(id);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        switch(adapterView.getId()) {
            case R.id.category1_spinner:
                measurement.setCategory1(null);
                break;
            case R.id.category2_spinner:
                measurement.setCategory2(null);
                break;
            case R.id.category3_spinner:
                measurement.setCategory3(null);
                break;
        }
    }

    @Override
    public void onInit(int status) {
        useTextToSpeech = status == TextToSpeech.SUCCESS;
    }

    /* Public methods */

    public void incSize(View source) {
        measurement.incSize(measurements.getPrecision().getValue());
    }

    public void decSize(View source) {
        measurement.decSize(measurements.getPrecision().getValue());
    }

    public void addMeasurement(View source) {
        addMeasurement();
    }

    /* Protected methods */

    protected MeasurementModel addMeasurement() {
        measurement.setDate(Calendar.getInstance());
        measurement.roundSize(measurements.getPrecision());

        Integer size = measurement.getSize();
        String cat1 = measurement.getCategory1();
        String cat2 = measurement.getCategory2();
        String cat3 = measurement.getCategory3();

        Cursor categoryWeightModelCursor = soh.getCategoryWeight(scientificSpecies.getId(), cat1, cat2, cat3);
        if (categoryWeightModelCursor.getCount() == 0) {
            CategoryWeightModel categoryWeightModel = new CategoryWeightModel();
            categoryWeightModel.setCategory1(cat1);
            categoryWeightModel.setCategory2(cat2);
            categoryWeightModel.setCategory3(cat3);
            categoryWeightModel.setParent(scientificSpecies);
            soh.saveData(categoryWeightModel);
        }

        soh.saveData(measurement);
        boolean take = measurements.addMeasurement(measurement);
        if (take) {
            if (useTextToSpeech) {
                textToSpeech.speak(getString(R.string.measurement_taking_audio_message), TextToSpeech.QUEUE_FLUSH, null);

            } else {
                ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, ToneGenerator.MAX_VOLUME);
                tg.startTone(ToneGenerator.TONE_PROP_PROMPT);
            }

            // Get instance of Vibrator from current Context
            Vibrator v = (Vibrator) getSystemService(VIBRATOR_SERVICE);
            // Vibrate for 300 milliseconds
            v.vibrate(300);

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.measurement_taking_message)
                    .setPositiveButton(android.R.string.ok, UIUtils.getCancelClickListener())
                    .create()
                    .show();
        }

        MeasurementModel result = measurement;

        initFishMeasurement(size, cat1, cat2, cat3);

        return result;
    }

    protected void initFishMeasurement(Integer size, String cat1, String cat2, String cat3) {
        measurement = new MeasurementModel();
        measurement.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent event) {
                String propertyName = event.getPropertyName();

                Object newValue = event.getNewValue();
                switch(propertyName) {
                    case MeasurementModel.COLUMN_SIZE:
                        String text = null;
                        if (newValue != null) {
                            text = UIUtils.getFormattedSize((Integer) newValue, measurements.getPrecision());
                            validateButton.setEnabled(true);
                        } else {
                            validateButton.setEnabled(false);
                        }
                        sizeText.setText(text);
                        break;

                    case MeasurementModel.COLUMN_CATEGORY_1:
                        CategoryModel category = commercialSpecies.getCategory1();
                        Spinner spinner = (Spinner) findViewById(R.id.category1_spinner);
                        setCategorySpinnerSelection(newValue, category, spinner);

                        EditText editText = (EditText) findViewById(R.id.category1_text);
                        editText.setText(String.valueOf(newValue));
                        break;

                    case MeasurementModel.COLUMN_CATEGORY_2:
                        category = commercialSpecies.getCategory2();
                        spinner = (Spinner) findViewById(R.id.category2_spinner);
                        setCategorySpinnerSelection(newValue, category, spinner);

                        editText = (EditText) findViewById(R.id.category2_text);
                        editText.setText(String.valueOf(newValue));
                        break;

                    case MeasurementModel.COLUMN_CATEGORY_3:
                        category = commercialSpecies.getCategory3();
                        spinner = (Spinner) findViewById(R.id.category3_spinner);
                        setCategorySpinnerSelection(newValue, category, spinner);

                        editText = (EditText) findViewById(R.id.category3_text);
                        editText.setText(String.valueOf(newValue));
                        break;
                }
            }
        });
        measurement.setSize(size);
        measurement.setCategory1(cat1);
        measurement.setCategory2(cat2);
        measurement.setCategory3(cat3);
        measurement.setParent(scientificSpecies);
    }

    protected void setCategorySpinnerSelection(Object newValue, CategoryModel category, Spinner spinner) {
        List<QualitativeValueModel> qualitativeValues = category.getQualitativeValues();
        if (category != null && CollectionUtils.isNotEmpty(qualitativeValues)) {
            ArrayAdapter adapter = (ArrayAdapter) spinner.getAdapter();
            QualitativeValueModel value = null;
            if (newValue != null) {
                value = measurements.getCategoryValuesById(newValue.toString());
            }
            int position = adapter.getPosition(value);
            spinner.setSelection(position);
        }
    }

    protected void setupTab(int contentId, String tag, int label) {
        View tabview = LayoutInflater.from(this).inflate(R.layout.tabs_bg, null);
        TextView tv = (TextView) tabview.findViewById(R.id.tabsText);
        tv.setText(label);

        TabHost.TabSpec content = tabs.newTabSpec(tag).setIndicator(tabview).setContent(contentId);
        tabs.addTab(content);

        MeasurementsDisplayerFragment fragment =
                (MeasurementsDisplayerFragment) getFragmentManager().findFragmentById(contentId);
        fragment.setMeasurements(measurements);

    }

    protected void setDrawerListAdapter() {
        mDrawerList.setAdapter(new ArrayAdapter<>(this,
                                                  android.R.layout.simple_list_item_1,
                                                  new String[]{
                                                          getString(R.string.home_title),
                                                          vessel.toString(this),
                                                          metier.toString(this),
                                                          commercialSpecies.toString(this),
                                                          scientificSpecies.toString(this)
                                                  }));
    }

    protected void fetchCalcifiedPartTakings() {
        if (scientificSpecies.isTakingActivation()) {
            WloSqlOpenHelper soh = new WloSqlOpenHelper(this);
            Cursor cursor = soh.getAllCalcifiedPartTakings(scientificSpecies.getName().getId());
            List<CalcifiedPartTaking> calcifiedPartTakings = WloSqlOpenHelper.transformCursorIntoCollection(cursor, new Function<Cursor, CalcifiedPartTaking>() {
                @Override
                public CalcifiedPartTaking apply(Cursor input) {
                    return new CalcifiedPartTaking(input);
                }
            });
            soh.close();
            measurements.setCalcifiedPartTakings(calcifiedPartTakings);
        }
    }

    protected void editItem(int position) {
        Class clazz;
        BaseModel modelToEdit;
        switch (position) {
            case 0:
                clazz = MainActivity.class;
                modelToEdit = null;
                break;
            case 1:
                clazz = VesselFormActivity.class;
                modelToEdit = vessel;
                break;
            case 2:
                clazz = MetierFormActivity.class;
                modelToEdit = metier;
                break;
            case 3:
                clazz = CommercialSpeciesFormActivity.class;
                modelToEdit = commercialSpecies;
                break;
            case 4:
                clazz = ScientificSpeciesFormActivity.class;
                modelToEdit = scientificSpecies;
                break;
            default:
                clazz = null;
                modelToEdit = null;
        }
        if (clazz != null) {
            Intent intent = new Intent(this, clazz);
            intent.putExtra(WloModelEditionActivity.INTENT_EXTRA_MODEL, modelToEdit);
            startActivityForResult(intent, position);
        }
    }

    protected void selectItem(int position) {
        Class activityClass;
        BaseModel parentModel = null;
        switch (position) {
            case 0:
                activityClass = MainActivity.class;
                break;
            case 1:
                activityClass = VesselsActivity.class;
                parentModel = location;
                break;
            case 2:
                activityClass = MetiersActivity.class;
                parentModel = vessel;
                break;
            case 3:
                activityClass = CommercialSpeciesActivity.class;
                parentModel = metier;
                break;
            case 4:
                activityClass = ScientificSpeciesActivity.class;
                parentModel = commercialSpecies;
                break;
            default:
                activityClass = null;
        }
        if (activityClass != null) {
            Intent intent = new Intent(this, activityClass);
            intent.putExtra(WloBaseListActivity.INTENT_EXTRA_PARENT_MODEL, parentModel);
            startActivity(intent);
        }
    }

}

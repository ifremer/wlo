package fr.ifremer.wlo.measurement;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.app.Fragment;
import fr.ifremer.wlo.models.MeasurementsModel;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public abstract class MeasurementsDisplayerFragment extends Fragment
                                                    implements MeasurementsModel.MeasurementsListener {

    private static final String TAG = "MeasurementsDisplayerFragment";

    protected MeasurementsModel measurements;

    public void setMeasurements(MeasurementsModel measurements) {
        this.measurements = measurements;
        this.measurements.addMeasurementsListener(this);
    }
}

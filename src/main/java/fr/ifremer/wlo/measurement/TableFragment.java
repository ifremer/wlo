package fr.ifremer.wlo.measurement;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import fr.ifremer.wlo.R;
import fr.ifremer.wlo.models.MeasurementModel;
import fr.ifremer.wlo.models.MeasurementsModel;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class TableFragment extends MeasurementsDisplayerFragment {

    private static final String TAG = "TableFragment";

    protected ListView table;
    protected TableAdapter adapter;

    /* Fragment methods */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.measurement_table_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        table = (ListView) getActivity().findViewById(R.id.table_content);
        adapter = new TableAdapter(getActivity(), measurements.getPrecision());
        table.setAdapter(adapter);

        for (MeasurementModel measurement : measurements.getMeasurements().values()) {
            onMeasurementAdded(measurements, measurement);
        }
    }

    /* MeasurementsListener methods */

    @Override
    public void onMeasurementAdded(MeasurementsModel source, MeasurementModel measurement) {
        int size = measurement.getSize();
        adapter.set(size, source.getMeasurementNb(size));
    }

    @Override
    public void onMeasurementRemoved(MeasurementsModel source, MeasurementModel measurement) {
        int size = measurement.getSize();
        adapter.set(size, source.getMeasurementNb(size));
    }
}

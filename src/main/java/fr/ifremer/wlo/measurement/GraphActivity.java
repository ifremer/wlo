package fr.ifremer.wlo.measurement;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;
import fr.ifremer.wlo.R;
import fr.ifremer.wlo.WloBaseActivity;
import fr.ifremer.wlo.models.MeasurementsModel;
import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.text.NumberFormat;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class GraphActivity extends WloBaseActivity {

    private static final String TAG = "GraphActivity";

    public static final String INTENT_EXTRA_DATA = "data";
    public static final String INTENT_EXTRA_MEASUREMENTS = "measurements";

    protected MeasurementsModel measurements;
    protected GraphicalView mChartView;
    protected XYMultipleSeriesDataset mDataset;
    protected XYMultipleSeriesRenderer mRenderer;

    @Override
    protected Integer getContentView() {
        return R.layout.measurement_graph;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return MeasurementActivity.class;
    }

    /* Activity methods */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayout root = (LinearLayout) findViewById(R.id.graphContainer);

        mDataset = new XYMultipleSeriesDataset();
        mRenderer = new XYMultipleSeriesRenderer();

        mChartView = ChartFactory.getBarChartView(this, mDataset, mRenderer, BarChart.Type.DEFAULT);
        root.addView(mChartView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        measurements = (MeasurementsModel) getIntent().getSerializableExtra(INTENT_EXTRA_MEASUREMENTS);
        XYSeries mCurrentSeries = (XYSeries) getIntent().getSerializableExtra(INTENT_EXTRA_DATA);

        XYSeriesRenderer mCurrentRenderer = new XYSeriesRenderer();
        mCurrentRenderer.setChartValuesFormat(NumberFormat.getIntegerInstance());
        mCurrentRenderer.setColor(getResources().getColor(android.R.color.holo_blue_light));
        mCurrentRenderer.setDisplayBoundingPoints(true);
        mCurrentRenderer.setChartValuesFormat(NumberFormat.getIntegerInstance());
        mCurrentRenderer.setDisplayChartValues(true);

        mRenderer.removeAllRenderers();
        mDataset.clear();

        mRenderer.addSeriesRenderer(mCurrentRenderer);
        mDataset.addSeries(mCurrentSeries);

        mChartView.repaint();
    }

    @Override
    public Intent getSupportParentActivityIntent() {
        Intent intent = super.getSupportParentActivityIntent();
        if (intent != null) {
            intent.putExtra(MeasurementActivity.INTENT_EXTRA_MEASUREMENTS, measurements);
        }
        return intent;
    }
}

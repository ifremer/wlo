package fr.ifremer.wlo.measurement;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import fr.ifremer.wlo.R;
import fr.ifremer.wlo.models.MeasurementModel;
import fr.ifremer.wlo.models.MeasurementsModel;
import fr.ifremer.wlo.models.referentials.Mensuration;
import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.text.NumberFormat;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since x.x
 */
public class GraphFragment extends MeasurementsDisplayerFragment {

    private static final String TAG = "GraphFragment";

    protected GraphicalView mChartView;
    protected XYSeries mCurrentSeries;
    protected XYMultipleSeriesRenderer mRenderer;

    /* Fragment methods */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.measurement_graph_fragment, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayout root = (LinearLayout) getView();

        mCurrentSeries = new XYSeries(getString(R.string.measurement_graph_title));
        final XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();
        mDataset.addSeries(mCurrentSeries);

        XYSeriesRenderer mCurrentRenderer = new XYSeriesRenderer();
        mCurrentRenderer.setChartValuesFormat(NumberFormat.getIntegerInstance());
        mCurrentRenderer.setColor(getResources().getColor(android.R.color.holo_blue_light));
        mCurrentRenderer.setDisplayBoundingPoints(true);
        mCurrentRenderer.setDisplayChartValues(true);

        mRenderer = new XYMultipleSeriesRenderer();
        mRenderer.addSeriesRenderer(mCurrentRenderer);
        mRenderer.setZoomEnabled(false, false);
        mRenderer.setPanEnabled(false, false);
        mRenderer.setYAxisMin(0);

        mChartView = ChartFactory.getBarChartView(getActivity(), mDataset, mRenderer, BarChart.Type.DEFAULT);
        mChartView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent i = new Intent(getActivity(), GraphActivity.class);
                i.putExtra(GraphActivity.INTENT_EXTRA_DATA, mCurrentSeries);
                i.putExtra(GraphActivity.INTENT_EXTRA_MEASUREMENTS, measurements);
                startActivity(i);
                return true;
            }
        });

        for (MeasurementModel measurement : measurements.getMeasurements().values()) {
            addMeasurement(measurement);
        }

        root.addView(mChartView);

    }

    /* MeasurementsListener methods */

    @Override
    public void onMeasurementAdded(MeasurementsModel source, MeasurementModel measurement) {
        addMeasurement(measurement);
        mChartView.repaint();
    }

    @Override
    public void onMeasurementRemoved(MeasurementsModel source, MeasurementModel measurement) {
        int size = measurement.getSize();

        int nb = measurements.getMeasurementNb(size);

        Mensuration.Precision precision = measurements.getPrecision();
        double dSize = (double) size / precision.getUnitDivider();
        int index = mCurrentSeries.getIndexForKey(dSize);

        mCurrentSeries.remove(index);
        mCurrentSeries.add(index, dSize, nb);

        mChartView.repaint();
    }

    /*  Protected methods  */

    protected void addMeasurement(MeasurementModel measurement) {
        int size = measurement.getSize();
        int nb = measurements.getMeasurementNb(size);

        Mensuration.Precision precision = measurements.getPrecision();
        double dSize = (double) size / precision.getUnitDivider();
        double step = (double) precision.getValue() / precision.getUnitDivider();
        double count = mCurrentSeries.getItemCount();

        if (precision.isDecimal()) {
            count = count * precision.getValue() / precision.getUnitDivider();
        }

        if (dSize >= count) {
            for (double i = count ; i < dSize ; i = i + step) {
                mCurrentSeries.add(i, 0);
            }
            mCurrentSeries.add(dSize, nb);

        } else {
            int index = mCurrentSeries.getIndexForKey(dSize);
            mCurrentSeries.remove(index);
            mCurrentSeries.add(index, dSize, nb);
        }

        updateAxis(dSize, nb);
    }

    protected void updateAxis(double size, int nb) {
        mRenderer.setXAxisMin(Math.min(mRenderer.getXAxisMin(), size - 1));
        mRenderer.setXAxisMax(Math.max(mRenderer.getXAxisMax(), size + 1));
        mRenderer.setYAxisMax(Math.max(mRenderer.getYAxisMax(), nb + 1));
    }
}

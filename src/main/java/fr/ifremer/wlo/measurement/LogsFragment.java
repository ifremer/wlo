package fr.ifremer.wlo.measurement;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import fr.ifremer.wlo.R;
import fr.ifremer.wlo.models.MeasurementModel;
import fr.ifremer.wlo.models.MeasurementsModel;
import fr.ifremer.wlo.utils.BaseModelArrayAdapter;
import fr.ifremer.wlo.utils.UIUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Comparator;
import java.util.List;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class LogsFragment extends MeasurementsDisplayerFragment {

    private static final String TAG = "LogsFragment";

    protected ArrayAdapter<MeasurementModel> adapter;
    protected AlertDialog.Builder dialogBuilder;

    /* Fragment methods */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.measurement_logs_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Activity activity = getActivity();
        adapter = new BaseModelArrayAdapter<MeasurementModel>(activity, android.R.layout.simple_list_item_1,
                        new Function<MeasurementModel, String>() {
                                @Override
                                public String apply(MeasurementModel measurement) {
                                    Context context = getActivity();
                                    String result = measurement.toString(context,measurements.getPrecision()) + "\n";
                                    List<Object> cats = Lists.newArrayList();

                                    String category1 = measurement.getCategory1();
                                    if (category1 != null) {
                                        Object category1Value = measurements.getCategoryValuesById(category1);
                                        if (category1Value == null) {
                                            category1Value = category1;
                                        }
                                        cats.add(category1Value);
                                    }

                                    String category2 = measurement.getCategory2();
                                    if (category2 != null) {
                                        Object category2Value = measurements.getCategoryValuesById(category2);
                                        if (category2Value == null) {
                                            category2Value = category2;
                                        }
                                        cats.add(category2Value);
                                    }

                                    String category3 = measurement.getCategory3();
                                    if (category3 != null) {
                                        Object category3Value = measurements.getCategoryValuesById(category3);
                                        if (category3Value == null) {
                                            category3Value = category3;
                                        }
                                        cats.add(category3Value);
                                    }
                                    result += StringUtils.join(cats, " - ");
                                    return result;
                                }
                        }) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View result = super.getView(position, convertView, parent);
                int color;
                if (position % 2 == 0) {
                    color = R.color.dark_gray;
                } else {
                    color = android.R.color.black;
                }
                result.setBackgroundResource(color);
                return result;
            }
        };

        dialogBuilder = new AlertDialog.Builder(activity)
                .setTitle(R.string.input_deletion_confirmation_title)
                .setNegativeButton(android.R.string.cancel, UIUtils.getCancelClickListener());

        // Find and set up the ListView for paired devices
        ListView logsList = (ListView) activity.findViewById(R.id.logs_list);
        logsList.setAdapter(adapter);
        logsList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final MeasurementModel measurement = (MeasurementModel) parent.getItemAtPosition(position);

                dialogBuilder.setMessage(getString(R.string.input_deletion_confirmation_message,
                                                   measurement.toString(getActivity(), measurements.getPrecision())))
                        .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    measurements.removeMeasurement(measurement);
                                }
                        })
                        .create()
                        .show();
                return true;
            }
        });

        for (MeasurementModel measurement : measurements.getMeasurements().values()) {
            onMeasurementAdded(measurements, measurement);
        }
        adapter.sort(new Comparator<MeasurementModel>() {
            @Override
            public int compare(MeasurementModel lhs, MeasurementModel rhs) {
                return ObjectUtils.compare(rhs.getDate(), lhs.getDate());
            }
        });
    }

    /* MeasurementsListener methods */

    @Override
    public void onMeasurementAdded(MeasurementsModel source, MeasurementModel measurement) {
        adapter.insert(measurement, 0);
    }

    @Override
    public void onMeasurementRemoved(MeasurementsModel source, MeasurementModel measurement) {
        adapter.remove(measurement);
    }

}

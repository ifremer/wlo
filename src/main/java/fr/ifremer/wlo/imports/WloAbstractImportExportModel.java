package fr.ifremer.wlo.imports;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.wlo.models.BaseModel;
import org.nuiton.csv.ValueGetter;
import org.nuiton.csv.ValueSetter;
import org.nuiton.csv.ext.AbstractImportExportModel;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public abstract class WloAbstractImportExportModel<M extends BaseModel> extends AbstractImportExportModel<M> {

    public WloAbstractImportExportModel(char separator) {
        super(separator);

        newOptionalColumn("id", new ValueSetter<M, String>() {
            @Override
            public void set(M object, String value) throws Exception {
                object.setId(value);
            }
        });

        newColumnForExport("id", new ValueGetter<M, String>() {
            @Override
            public String get(M object) throws Exception {
                return object.getId();
            }
        });
    }
}

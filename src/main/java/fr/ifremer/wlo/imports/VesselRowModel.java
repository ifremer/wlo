package fr.ifremer.wlo.imports;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.wlo.models.referentials.Vessel;
import org.nuiton.csv.ValueGetterSetter;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class VesselRowModel extends WloAbstractImportExportModel<Vessel> {


    public VesselRowModel(char separator) {
        super(separator);

        newColumnForImportExport("NAVS_COD", new ValueGetterSetter<Vessel, String>() {
            @Override
            public void set(Vessel object, String value) throws Exception {
                object.setCode(value);
            }

            @Override
            public String get(Vessel object) throws Exception {
                return object.getCode();
            }
        });
        newColumnForImportExport("CARN_NOM", new ValueGetterSetter<Vessel, String>() {
            @Override
            public void set(Vessel object, String value) throws Exception {
                object.setName(value);
            }

            @Override
            public String get(Vessel object) throws Exception {
                return object.getName();
            }
        });
        newColumnForImportExport("QUARTIER_COD", new ValueGetterSetter<Vessel, String>() {
            @Override
            public void set(Vessel object, String value) throws Exception {
                object.setQuarterCode(value);
            }

            @Override
            public String get(Vessel object) throws Exception {
                return object.getQuarterCode();
            }
        });
    }

    @Override
    public Vessel newEmptyInstance() {
        return new Vessel();
    }
}

package fr.ifremer.wlo.imports;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.wlo.models.referentials.Metier;
import org.nuiton.csv.ValueGetterSetter;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class MetierRowModel extends WloAbstractImportExportModel<Metier> {


    public MetierRowModel(char separator) {
        super(separator);

        newColumnForImportExport("MET_ID", new ValueGetterSetter<Metier, String>() {
            @Override
            public void set(Metier object, String value) throws Exception {
                object.setMetierId(value);
            }

            @Override
            public String get(Metier object) throws Exception {
                return object.getMetierId();
            }
        });
        newColumnForImportExport("MET_COD", new ValueGetterSetter<Metier, String>() {
            @Override
            public void set(Metier object, String value) throws Exception {
                object.setCode(value);
            }

            @Override
            public String get(Metier object) throws Exception {
                return object.getCode();
            }
        });
        newColumnForImportExport("MET_LIB", new ValueGetterSetter<Metier, String>() {
            @Override
            public void set(Metier object, String value) throws Exception {
                object.setLabel(value);
            }

            @Override
            public String get(Metier object) throws Exception {
                return object.getLabel();
            }
        });
        newColumnForImportExport("MET_ENGIN_COD", new ValueGetterSetter<Metier, String>() {
            @Override
            public void set(Metier object, String value) throws Exception {
                object.setGearCode(value);
            }

            @Override
            public String get(Metier object) throws Exception {
                return object.getGearCode();
            }
        });
        newColumnForImportExport("MET_ENGIN_LIB", new ValueGetterSetter<Metier, String>() {
            @Override
            public void set(Metier object, String value) throws Exception {
                object.setGearLabel(value);
            }

            @Override
            public String get(Metier object) throws Exception {
                return object.getGearLabel();
            }
        });
        newColumnForImportExport("MET_ESPECE_COD", new ValueGetterSetter<Metier, String>() {
            @Override
            public void set(Metier object, String value) throws Exception {
                object.setSpeciesCode(value);
            }

            @Override
            public String get(Metier object) throws Exception {
                return object.getSpeciesCode();
            }
        });
        newColumnForImportExport("MET_ESPECE_LIB", new ValueGetterSetter<Metier, String>() {
            @Override
            public void set(Metier object, String value) throws Exception {
                object.setSpeciesLabel(value);
            }

            @Override
            public String get(Metier object) throws Exception {
                return object.getSpeciesLabel();
            }
        });
        newColumnForImportExport("MET_PECHE", new ValueGetterSetter<Metier, String>() {
            @Override
            public void set(Metier object, String value) throws Exception {
                Integer iValue;
                try {
                    iValue = Integer.valueOf(value);
                } catch (NumberFormatException e) {
                    iValue = 0;
                }
                object.setFishing(iValue);
            }

            @Override
            public String get(Metier object) throws Exception {
                return String.valueOf(object.getFishing());
            }
        });
        newColumnForImportExport("MET_ACT", new ValueGetterSetter<Metier, String>() {
            @Override
            public void set(Metier object, String value) throws Exception {
                Integer iValue;
                try {
                    iValue = Integer.valueOf(value);
                } catch (NumberFormatException e) {
                    iValue = 0;
                }
                object.setActive(iValue);
            }

            @Override
            public String get(Metier object) throws Exception {
                return String.valueOf(object.getActive());
            }
        });
    }

    @Override
    public Metier newEmptyInstance() {
        return new Metier();
    }
}

package fr.ifremer.wlo.imports;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.wlo.models.referentials.CommercialSpecies;
import org.nuiton.csv.ValueGetterSetter;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class CommercialSpeciesRowModel extends WloAbstractImportExportModel<CommercialSpecies> {


    public CommercialSpeciesRowModel(char separator) {
        super(separator);

        newColumnForImportExport("ESPF_COD", new ValueGetterSetter<CommercialSpecies, String>() {
            @Override
            public void set(CommercialSpecies object, String value) throws Exception {
                object.setCode(value);
            }

            @Override
            public String get(CommercialSpecies object) throws Exception {
                return object.getCode();
            }
        });
        newColumnForImportExport("ESPF_ISSCAP", new ValueGetterSetter<CommercialSpecies, String>() {
            @Override
            public void set(CommercialSpecies object, String value) throws Exception {
                object.setIsscap(value);
            }

            @Override
            public String get(CommercialSpecies object) throws Exception {
                return object.getIsscap();
            }
        });
        newColumnForImportExport("ESPF_TAXON_COD", new ValueGetterSetter<CommercialSpecies, String>() {
            @Override
            public void set(CommercialSpecies object, String value) throws Exception {
                object.setTaxonCode(value);
            }

            @Override
            public String get(CommercialSpecies object) throws Exception {
                return object.getTaxonCode();
            }
        });
        newColumnForImportExport("ESPF_SCI_LIB", new ValueGetterSetter<CommercialSpecies, String>() {
            @Override
            public void set(CommercialSpecies object, String value) throws Exception {
                object.setScientificLabel(value);
            }

            @Override
            public String get(CommercialSpecies object) throws Exception {
                return object.getScientificLabel();
            }
        });
        newColumnForImportExport("ESPF_FRA_LIB", new ValueGetterSetter<CommercialSpecies, String>() {
            @Override
            public void set(CommercialSpecies object, String value) throws Exception {
                object.setFrenchLabel(value);
            }

            @Override
            public String get(CommercialSpecies object) throws Exception {
                return object.getFrenchLabel();
            }
        });
        newColumnForImportExport("ESPF_FAMILLE", new ValueGetterSetter<CommercialSpecies, String>() {
            @Override
            public void set(CommercialSpecies object, String value) throws Exception {
                object.setFamily(value);
            }

            @Override
            public String get(CommercialSpecies object) throws Exception {
                return object.getFamily();
            }
        });
        newColumnForImportExport("ESPF_ORDRE", new ValueGetterSetter<CommercialSpecies, String>() {
            @Override
            public void set(CommercialSpecies object, String value) throws Exception {
                object.setSpeciesOrder(value);
            }

            @Override
            public String get(CommercialSpecies object) throws Exception {
                return object.getSpeciesOrder();
            }
        });
        newColumnForImportExport("ESPF_ACT", new ValueGetterSetter<CommercialSpecies, String>() {
            @Override
            public void set(CommercialSpecies object, String value) throws Exception {
                Integer iValue;
                try {
                    iValue = Integer.valueOf(value);
                } catch (NumberFormatException e) {
                    iValue = 0;
                }
                object.setActive(iValue);
            }

            @Override
            public String get(CommercialSpecies object) throws Exception {
                return String.valueOf(object.getActive());
            }
        });
    }

    @Override
    public CommercialSpecies newEmptyInstance() {
        return new CommercialSpecies();
    }
}

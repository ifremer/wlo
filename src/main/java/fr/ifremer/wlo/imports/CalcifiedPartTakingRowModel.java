package fr.ifremer.wlo.imports;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.Maps;
import fr.ifremer.wlo.models.referentials.CalcifiedPartTaking;
import fr.ifremer.wlo.models.referentials.ScientificSpecies;
import org.nuiton.csv.ValueGetterSetter;

import java.util.Collection;
import java.util.Map;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class CalcifiedPartTakingRowModel extends WloAbstractImportExportModel<CalcifiedPartTaking> {

    private static final String TAG = "CalcifiedPartTakingRowModel";

    public CalcifiedPartTakingRowModel(char separator, Collection<ScientificSpecies> scientificSpecies) {

        super(separator);

        final Map<String, ScientificSpecies> scientificSpeciesByCode = Maps.uniqueIndex(scientificSpecies, new Function<ScientificSpecies, String>() {
            @Override
            public String apply(ScientificSpecies input) {
                return input.getCode();
            }
        });

        newColumnForImportExport("Espece_scientifique", new ValueGetterSetter<CalcifiedPartTaking, String>() {
            @Override
            public void set(CalcifiedPartTaking object, String value) throws Exception {
                ScientificSpecies species = scientificSpeciesByCode.get(value);
                object.setParent(species);
            }

            @Override
            public String get(CalcifiedPartTaking object) throws Exception {
                return object.getParentId();
            }
        });

        newColumnForImportExport("Classe_debut", new ValueGetterSetter<CalcifiedPartTaking, String>() {
            @Override
            public void set(CalcifiedPartTaking object, String value) throws Exception {
                Integer size;
                try {
                    size = Integer.parseInt(value);
                } catch (NumberFormatException e) {
                    size = null;
                }
                object.setStartSize(size);
            }

            @Override
            public String get(CalcifiedPartTaking object) throws Exception {
                return String.valueOf(object.getStartSize());
            }
        });

        newColumnForImportExport("Classe_fin", new ValueGetterSetter<CalcifiedPartTaking, String>() {
            @Override
            public void set(CalcifiedPartTaking object, String value) throws Exception {
                Integer size;
                try {
                    size = Integer.parseInt(value);
                } catch (NumberFormatException e) {
                    size = null;
                }
                object.setEndSize(size);
            }

            @Override
            public String get(CalcifiedPartTaking object) throws Exception {
                return String.valueOf(object.getEndSize());
            }
        });

        newColumnForImportExport("Pas_de_classe", new ValueGetterSetter<CalcifiedPartTaking, String>() {
            @Override
            public void set(CalcifiedPartTaking object, String value) throws Exception {
                Integer size;
                try {
                    size = Integer.parseInt(value);
                } catch (NumberFormatException e) {
                    size = null;
                }
                object.setSizeStep(size);
            }

            @Override
            public String get(CalcifiedPartTaking object) throws Exception {
                return String.valueOf(object.getSizeStep());
            }
        });

        newColumnForImportExport("Pas", new ValueGetterSetter<CalcifiedPartTaking, String>() {
            @Override
            public void set(CalcifiedPartTaking object, String value) throws Exception {
                Integer size = Integer.parseInt(value);
                object.setStep(size);
            }

            @Override
            public String get(CalcifiedPartTaking object) throws Exception {
                return String.valueOf(object.getStep());
            }
        });

        newColumnForImportExport("Arret", new ValueGetterSetter<CalcifiedPartTaking, String>() {
            @Override
            public void set(CalcifiedPartTaking object, String value) throws Exception {
                Integer size;
                try {
                    size = Integer.parseInt(value);
                } catch (NumberFormatException e) {
                    size = null;
                }
                object.setStop(size);
            }

            @Override
            public String get(CalcifiedPartTaking object) throws Exception {
                return String.valueOf(object.getStop());
            }
        });
    }

    @Override
    public CalcifiedPartTaking newEmptyInstance() {
        return new CalcifiedPartTaking();
    }
}

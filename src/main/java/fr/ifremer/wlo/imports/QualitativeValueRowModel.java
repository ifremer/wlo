package fr.ifremer.wlo.imports;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.wlo.models.categorization.CategoryModel;
import fr.ifremer.wlo.models.categorization.QualitativeValueModel;
import org.nuiton.csv.ValueSetter;
import org.nuiton.csv.ext.AbstractImportExportModel;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class QualitativeValueRowModel extends AbstractImportExportModel<QualitativeValueModel> {

    protected CategoryModel categoryModel;

    public QualitativeValueRowModel(char separator, CategoryModel category) {
        super(separator);

        categoryModel = category;

        newMandatoryColumn("Code", new ValueSetter<QualitativeValueModel, String>() {
            @Override
            public void set(QualitativeValueModel object, String value) throws Exception {
                object.setValue(value);
            }
        });
        newOptionalColumn("Libelle", new ValueSetter<QualitativeValueModel, String>() {
            @Override
            public void set(QualitativeValueModel object, String value) throws Exception {
                object.setLabel(value);
            }
        });
    }

    @Override
    public QualitativeValueModel newEmptyInstance() {
        QualitativeValueModel qualitativeValueModel = new QualitativeValueModel();
        qualitativeValueModel.setCategory(categoryModel);
        return qualitativeValueModel;
    }
}

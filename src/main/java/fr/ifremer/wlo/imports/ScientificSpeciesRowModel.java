package fr.ifremer.wlo.imports;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.wlo.models.referentials.ScientificSpecies;
import org.nuiton.csv.ValueGetterSetter;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class ScientificSpeciesRowModel extends WloAbstractImportExportModel<ScientificSpecies> {


    public ScientificSpeciesRowModel(char separator) {
        super(separator);

        newColumnForImportExport("C_Perm", new ValueGetterSetter<ScientificSpecies, String>() {
            @Override
            public void set(ScientificSpecies object, String value) throws Exception {
                object.setPermCode(value);
            }

            @Override
            public String get(ScientificSpecies object) throws Exception {
                return object.getPermCode();
            }
        });
        newColumnForImportExport("C_VALIDE", new ValueGetterSetter<ScientificSpecies, String>() {
            @Override
            public void set(ScientificSpecies object, String value) throws Exception {
                object.setCode(value);
            }

            @Override
            public String get(ScientificSpecies object) throws Exception {
                return object.getCode();
            }
        });
        newColumnForImportExport("L_VALIDE", new ValueGetterSetter<ScientificSpecies, String>() {
            @Override
            public void set(ScientificSpecies object, String value) throws Exception {
                object.setLabel(value);
            }

            @Override
            public String get(ScientificSpecies object) throws Exception {
                return object.getLabel();
            }
        });
    }

    @Override
    public ScientificSpecies newEmptyInstance() {
        return new ScientificSpecies();
    }
}

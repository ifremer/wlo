package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.wlo.measurement.MeasurementActivity;
import fr.ifremer.wlo.models.ScientificSpeciesModel;
import fr.ifremer.wlo.models.referentials.ScientificSpecies;
import fr.ifremer.wlo.preferences.MultiSelectItemPreference;
import fr.ifremer.wlo.storage.DataCache;
import fr.ifremer.wlo.utils.WloAutoCompleteTextViewWithFavorites;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class ScientificSpeciesFormActivity extends WloModelEditionActivity<ScientificSpeciesModel> {

    private static final String TAG = "ScientificSpeciesFormActivity";

    @Override
    protected Integer getContentView() {
        return R.layout.scientific_species_form;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return ScientificSpeciesActivity.class;
    }

    @Override
    protected Class<? extends WloModelEditionActivity> getNextEditionActivity() {
        return null;
    }

    @Override
    protected Class<? extends WloBaseListActivity> getNextListActivity() {
        return null;
    }

    @Override
    protected ScientificSpeciesModel createNewModel() {
        return new ScientificSpeciesModel();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // editors

        // init editors
        List<ScientificSpecies> scientificSpecies = Lists.newArrayList(DataCache.getAllScientificSpecies(this));

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        final Set<String> favoriteIds = sharedPref.getStringSet(MultiSelectItemPreference.SCIENTIFIC_SPECIES_FAVORITES.getKey(),
                                                                Sets.<String>newHashSet());
        Collection<ScientificSpecies> favorites = Collections2.filter(scientificSpecies, new Predicate<ScientificSpecies>() {
            @Override
            public boolean apply(ScientificSpecies input) {
                return favoriteIds.contains(input.getId());
            }
        });

        WloAutoCompleteTextViewWithFavorites actvwf = (WloAutoCompleteTextViewWithFavorites) findViewById(R.id.scientific_species_form_name);
        initAutoCompleteTextView(actvwf.getAutoCompleteTextView(), ScientificSpeciesModel.COLUMN_NAME, scientificSpecies, favorites);
        actvwf.useFavorites(!favorites.isEmpty());

        CheckBox takingActivationEditor = (CheckBox) findViewById(R.id.scientific_species_form_takingActivation);
        takingActivationEditor.setChecked(model.isTakingActivation());
        takingActivationEditor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                model.setTakingActivation(b);
            }
        });

        initEditText(R.id.form_comment, ScientificSpeciesModel.COLUMN_COMMENT);
    }

    @Override
    public void gotoNextActivity() {
        Intent intent = new Intent(this, MeasurementActivity.class);
        intent.putExtra(MeasurementActivity.INTENT_EXTRA_SCIENTIFIC_SPECIES, model);
        startActivity(intent);
    }

}

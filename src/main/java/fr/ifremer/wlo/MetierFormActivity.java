package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.os.Bundle;
import com.google.common.collect.Lists;
import fr.ifremer.wlo.models.MetierModel;
import fr.ifremer.wlo.models.referentials.Metier;
import fr.ifremer.wlo.storage.DataCache;

import java.util.List;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class MetierFormActivity extends WloModelEditionActivity<MetierModel> {

    private static final String TAG = "MetierFormActivity";

    @Override
    protected Integer getContentView() {
        return R.layout.metier_form;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return MetiersActivity.class;
    }

    @Override
    protected Class<? extends WloModelEditionActivity> getNextEditionActivity() {
        return CommercialSpeciesFormActivity.class;
    }

    @Override
    protected Class<? extends WloBaseListActivity> getNextListActivity() {
        return CommercialSpeciesActivity.class;
    }

    @Override
    protected MetierModel createNewModel() {
        return new MetierModel();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // init editors
        List<Metier> metiers = Lists.newArrayList(DataCache.getAllMetiers(this));
        initAutoCompleteTextView(R.id.metier_form_gear_species, MetierModel.COLUMN_GEAR_SPECIES, metiers);

        initEditText(R.id.metier_form_zone, MetierModel.COLUMN_ZONE);
        initEditText(R.id.metier_form_sample_row_code, MetierModel.COLUMN_SAMPLE_ROW_CODE);

        initEditText(R.id.form_comment, MetierModel.COLUMN_COMMENT);
    }

}

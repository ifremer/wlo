package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import com.google.common.collect.Maps;
import fr.ifremer.wlo.models.LocationModel;
import fr.ifremer.wlo.utils.WloItemListViewBinder;

import java.util.Map;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class LocationsActivity extends WloBaseListActivity<LocationModel> {

    private static final String TAG = "LocationsActivity";

    /*  Activity methods  */

    @Override
    protected SimpleCursorAdapter createAdapter() {
        return new SimpleCursorAdapter(this, R.layout.location_list_item, null,
                                       new String[] { LocationModel.COLUMN_LOCATION, LocationModel.COLUMN_START_DATE, LocationModel.COLUMN_END_DATE },
                                       new int[] { R.id.location_location, R.id.location_start_date, R.id.location_end_date }, 0);
    }

    @Override
    protected Cursor getAllData() {
        Cursor cursor = woh.getAllLocations(parentModel.getId());
        return cursor;
    }

    @Override
    protected LocationModel createNewModel(Cursor cursor) {
        return new LocationModel(this, cursor);
    }

    @Override
    protected Class<? extends WloBaseActivity> getNextActivity() {
        return VesselsActivity.class;
    }

    @Override
    protected Class<? extends WloModelEditionActivity> getEditionActivity() {
        return LocationFormActivity.class;
    }

    @Override
    protected Integer getSubtitle() {
        return R.string.locations_subtitle;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return ContextsActivity.class;
    }

    @Override
    protected SimpleCursorAdapter.ViewBinder getAdapterBinder() {
        Map<Integer, WloItemListViewBinder.DataType> types = Maps.newHashMap();
        types.put(2, WloItemListViewBinder.DataType.DATETIME);
        types.put(3, WloItemListViewBinder.DataType.DATETIME);
        types.put(4, WloItemListViewBinder.DataType.LOCATION);
        return new WloItemListViewBinder(this, types);
    }
}

package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import fr.ifremer.wlo.models.VesselModel;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class VesselsActivity extends WloBaseListActivity<VesselModel> {

    private static final String TAG = "VesselsActivity";

    /*  Activity methods  */

    @Override
    protected SimpleCursorAdapter createAdapter() {
        return new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, null,
                                       new String[] { VesselModel.COLUMN_REGISTRATION_NUMBER, VesselModel.COLUMN_NAME },
                                       new int[] { android.R.id.text1, android.R.id.text2 }, 0);
    }

    @Override
    protected Cursor getAllData() {
        Cursor cursor = woh.getAllVessels(parentModel.getId());
        return cursor;
    }

    @Override
    protected VesselModel createNewModel(Cursor cursor) {
        return new VesselModel(this, cursor);
    }

    @Override
    protected Class<? extends WloBaseListActivity> getNextActivity() {
        return MetiersActivity.class;
    }

    @Override
    protected Class<? extends WloModelEditionActivity> getEditionActivity() {
        return VesselFormActivity.class;
    }

    @Override
    protected Integer getSubtitle() {
        return R.string.vessels_subtitle;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return LocationsActivity.class;
    }
}

package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.common.base.Function;
import com.google.common.collect.Maps;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.models.CategoryWeightModel;
import fr.ifremer.wlo.models.CommercialSpeciesModel;
import fr.ifremer.wlo.models.ScientificSpeciesModel;
import fr.ifremer.wlo.models.categorization.CategoryModel;
import fr.ifremer.wlo.models.categorization.QualitativeValueModel;
import fr.ifremer.wlo.preferences.ListItemPreference;
import fr.ifremer.wlo.storage.WloSqlOpenHelper;
import fr.ifremer.wlo.utils.BaseTextWatcher;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class WeightsActivity extends WloBaseActivity {

    private static final String TAG = "WeightsActivity";

    public static final String INTENT_COMMERCIAL_SPECIES = "commercialSpecies";

    protected LinearLayout container;

    protected CommercialSpeciesModel commercialSpeciesModel;

    protected List<ScientificSpeciesModel> scientficSpeciesModels;

    protected List<CategoryWeightModel> categoryWeightModels = new ArrayList<>();

    protected Map<String, QualitativeValueModel> valuesById = new HashMap<>();

    protected int weightUnit;

    protected String weightUnitLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        container = (LinearLayout) findViewById(R.id.container);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String weightUnitString = sharedPref.getString(ListItemPreference.WEIGHT_UNIT.getKey(), "1");
        weightUnit = Integer.parseInt(weightUnitString);
        switch (weightUnit) {
            case 1000:
                weightUnitLabel = "kg";
                break;
            default:
                weightUnitLabel = "g";
                break;
        }

        commercialSpeciesModel = (CommercialSpeciesModel) getIntent().getSerializableExtra(INTENT_COMMERCIAL_SPECIES);

        List<String> categoryLabels = new ArrayList<>();
        CategoryModel category1 = commercialSpeciesModel.getCategory1();
        if (category1 != null) {
            valuesById.putAll(Maps.uniqueIndex(category1.getQualitativeValues(), BaseModel.GET_ID_FUNCTION));
            categoryLabels.add(category1.getLabel());
        }
        CategoryModel category2 = commercialSpeciesModel.getCategory2();
        if (category2 != null) {
            valuesById.putAll(Maps.uniqueIndex(category2.getQualitativeValues(), BaseModel.GET_ID_FUNCTION));
            categoryLabels.add(category2.getLabel());
        }
        CategoryModel category3 = commercialSpeciesModel.getCategory3();
        if (category3 != null) {
            valuesById.putAll(Maps.uniqueIndex(category3.getQualitativeValues(), BaseModel.GET_ID_FUNCTION));
            categoryLabels.add(category3.getLabel());
        }

        getSupportActionBar().setSubtitle(commercialSpeciesModel.toString(this));

        // create total unloaded weight label
        createWeightLabel(getString(R.string.commercial_species_total_unloaded_weight));

        // create total unloaded weight field
        createEditText(commercialSpeciesModel.getTotalUnloadedWeight(), new Function<Integer, Void>() {
            @Override
            public Void apply(Integer input) {
                commercialSpeciesModel.setTotalUnloadedWeight(input);
                return null;
            }
        });

        // get all scientific species
        WloSqlOpenHelper woh = new WloSqlOpenHelper(this);
        Cursor allScientificSpeciesCursor = woh.getAllScientificSpecies(commercialSpeciesModel.getId());
        scientficSpeciesModels = WloSqlOpenHelper.transformCursorIntoCollection(allScientificSpeciesCursor,
                                                                                new Function<Cursor, ScientificSpeciesModel>() {
            @Override
            public ScientificSpeciesModel apply(Cursor input) {
                ScientificSpeciesModel scientificSpeciesModel =
                        new ScientificSpeciesModel(WeightsActivity.this, input);
                scientificSpeciesModel.setParent(commercialSpeciesModel);
                return scientificSpeciesModel;
            }
        });

        for (final ScientificSpeciesModel scientificSpecies : scientficSpeciesModels) {

            // add separator
            getLayoutInflater().inflate(R.layout.vertical_separator, container);

            // add label
            TextView labelView = new TextView(this);
            labelView.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            labelView.setTextAppearance(this, android.R.style.TextAppearance_Large);
            labelView.setText(scientificSpecies.toString(this));
            container.addView(labelView);

            // add label
            createWeightLabel(getString(R.string.scientific_species_sorted_weight));

            // create the sorted weight field
            createEditText(scientificSpecies.getSortedWeight(), new Function<Integer, Void>() {
                @Override
                public Void apply(Integer input) {
                    scientificSpecies.setSortedWeight(input);
                    return null;
                }
            });

            // add label
            createWeightLabel(getString(R.string.scientific_species_sample_weight));

            // create the sample weight field
            createEditText(scientificSpecies.getSampleWeight(), new Function<Integer, Void>() {
                @Override
                public Void apply(Integer input) {
                    scientificSpecies.setSampleWeight(input);
                    return null;
                }
            });

            if (!categoryLabels.isEmpty()) {
                // weight per category
                TextView weightByCategoryView = new TextView(this);
                weightByCategoryView.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                weightByCategoryView.setTextAppearance(this, android.R.style.TextAppearance_Medium);
                String weightByCategoryViewText = getString(R.string.scientific_species_categories)
                                                    + " (" + StringUtils.join(categoryLabels, " / ") + ")";
                weightByCategoryView.setText(weightByCategoryViewText);
                container.addView(weightByCategoryView);

                // get all the category weights
                Cursor categoryWeighs = woh.getAllCategoryWeigths(scientificSpecies.getId());
                List<CategoryWeightModel> categoryWeightModels = WloSqlOpenHelper.transformCursorIntoCollection(categoryWeighs,
                                                                                      new Function<Cursor, CategoryWeightModel>() {
                                                                                          @Override
                                                                                          public CategoryWeightModel apply(Cursor input) {
                                                                                              CategoryWeightModel result = new CategoryWeightModel(input);
                                                                                              result.setParent(scientificSpecies);
                                                                                              return result;
                                                                                          }
                                                                                      });
                this.categoryWeightModels.addAll(categoryWeightModels);


                for (final CategoryWeightModel categoryWeightModel : categoryWeightModels) {
                    // create the weight field

                    List<String> labels = new ArrayList<>();

                    String categoryWeightModelCategory1 = categoryWeightModel.getCategory1();
                    if (categoryWeightModelCategory1 != null) {
                        QualitativeValueModel category1Value = valuesById.get(categoryWeightModelCategory1);
                        String category1ValueLabel = category1Value != null ? category1Value.toString() : categoryWeightModelCategory1;
                        labels.add(category1ValueLabel);
                    }

                    String categoryWeightModelCategory2 = categoryWeightModel.getCategory2();
                    if (categoryWeightModelCategory2 != null) {
                        QualitativeValueModel category2Value = valuesById.get(categoryWeightModelCategory2);
                        String category2ValueLabel = category2Value != null ? category2Value.toString() : categoryWeightModelCategory2;
                        labels.add(category2ValueLabel);
                    }

                    String categoryWeightModelCategory3 = categoryWeightModel.getCategory3();
                    if (categoryWeightModelCategory3 != null) {
                        QualitativeValueModel category3Value = valuesById.get(categoryWeightModelCategory3);
                        String category3ValueLabel = category3Value != null ? category3Value.toString() : categoryWeightModelCategory3;
                        labels.add(category3ValueLabel);
                    }

                    String label = StringUtils.join(labels, " / ");

                    // add label
                    createWeightLabel(label);

                    createEditText(categoryWeightModel.getWeight(), new Function<Integer, Void>() {
                        @Override
                        public Void apply(Integer input) {
                            categoryWeightModel.setWeight(input);
                            return null;
                        }
                    });
                }
            }
        }
        woh.close();
    }

    @Override
    protected Integer getContentView() {
        return R.layout.weights;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return ScientificSpeciesActivity.class;
    }

    @Override
    public Intent getSupportParentActivityIntent() {
        Intent intent = super.getSupportParentActivityIntent();
        intent.putExtra(ScientificSpeciesActivity.INTENT_EXTRA_PARENT_MODEL, commercialSpeciesModel);
        return intent;
    }

    public void validate(View view) {
        saveModel();
        Intent intent = new Intent();
        intent.putExtra(INTENT_COMMERCIAL_SPECIES, commercialSpeciesModel);
        setResult(RESULT_OK, intent);
        finish();
    }

    /* Protected methods */

    protected TextView createWeightLabel(String label) {
        TextView textView = new TextView(this);
        textView.setText(label + " (" + weightUnitLabel + ")");
        container.addView(textView);
        return textView;
    }

    protected TextWatcher getTextWatcher(final EditText editText, final Function<Integer, Void> setWeightFunction) {
        return new BaseTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Integer newValue = null;
                if (StringUtils.isNotEmpty(s)) {
                    if (weightUnit > 1) {
                        newValue = (int) (Float.parseFloat(s.toString()) * weightUnit);

                    } else {
                        newValue = Integer.parseInt(s.toString());
                    }
                }
                setWeightFunction.apply(newValue);
                editText.setSelection(start + count);
            }
        };
    }

    protected EditText createEditText(Integer value, Function<Integer, Void> setWeightFunction) {
        final EditText editText = new EditText(this);

        int inputType = InputType.TYPE_CLASS_NUMBER;
        String textValue = null;

        if (weightUnit > 1) {
            inputType |= InputType.TYPE_NUMBER_FLAG_DECIMAL;
        }
        if (value != null) {
            if (weightUnit > 1 && value != null) {
                float floatValue = (float) value / weightUnit;
                textValue = String.format(Locale.US, "%.3f", floatValue);

            } else {
                textValue = String.valueOf(value);
            }
        }
        editText.setInputType(inputType);
        editText.setText(textValue);
        editText.addTextChangedListener(getTextWatcher(editText, setWeightFunction));
        container.addView(editText);

        return editText;
    }

    protected void saveModel() {
        WloSqlOpenHelper woh = new WloSqlOpenHelper(this);
        woh.saveData(categoryWeightModels);
        woh.saveData(scientficSpeciesModels);
        woh.saveData(commercialSpeciesModel);
        woh.close();
    }

}

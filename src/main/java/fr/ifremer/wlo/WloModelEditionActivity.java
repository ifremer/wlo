package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import com.google.common.collect.Multimap;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.models.HierarchicalModel;
import fr.ifremer.wlo.storage.WloSqlOpenHelper;
import fr.ifremer.wlo.utils.BaseTextWatcher;
import fr.ifremer.wlo.utils.UIUtils;
import fr.ifremer.wlo.utils.WloAutoCompleteTextViewWithFavorites;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public abstract class WloModelEditionActivity<M extends BaseModel> extends WloBaseActivity {

    private static final String TAG = "WloModelEditionActivity";

    public static final String INTENT_EXTRA_PARENT_MODEL = "parentModel";
    public static final String INTENT_EXTRA_MODEL = "model";
    public static final String INTENT_EXTRA_SUBTITLE = "subtitle";

    protected M model;

    protected abstract Class<? extends WloModelEditionActivity> getNextEditionActivity();

    protected abstract Class<? extends WloBaseListActivity> getNextListActivity();

    protected abstract M createNewModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // model
        model = (M) getIntent().getSerializableExtra(INTENT_EXTRA_MODEL);
        if (model == null) {
            model = createNewModel();
            if (HierarchicalModel.class.isAssignableFrom(model.getClass())) {
                BaseModel parent = (BaseModel) getIntent().getSerializableExtra(INTENT_EXTRA_PARENT_MODEL);
                HierarchicalModel hModel = (HierarchicalModel) model;
                hModel.setParent(parent);
            }

        } else {
            setTitle(model.toString(this));
        }

        String subtitle = getIntent().getStringExtra(INTENT_EXTRA_SUBTITLE);
        getSupportActionBar().setSubtitle(subtitle);

        model.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent event) {
                model.setModified(true);
            }
        });
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public Intent getSupportParentActivityIntent() {
        Intent intent = super.getSupportParentActivityIntent();
        if (intent != null && HierarchicalModel.class.isAssignableFrom(model.getClass())) {
            HierarchicalModel hModel = (HierarchicalModel) model;
            intent.putExtra(WloBaseListActivity.INTENT_EXTRA_PARENT_MODEL, hModel.getParent());
        }
        return intent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.form_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_item:
                if (model.isModified()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == AlertDialog.BUTTON_POSITIVE) {
                                saveModel();
                            }
                            openNewModelEdition();

                        }
                    };
                    builder.setMessage(R.string.exit_form_confirmation)
                            .setNegativeButton(android.R.string.cancel, UIUtils.getCancelClickListener())
                            .setNeutralButton(R.string.no, listener)
                            .setPositiveButton(R.string.yes, listener)
                            .create()
                            .show();

                } else {
                    openNewModelEdition();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /* Method called by the view */

    public void validate(View view) {
        boolean newModel = model.isNew();

        Multimap<BaseModel.ErrorType, String> errors = model.checkValidity();

        if (errors.isEmpty()) {
            saveModel();

            if (newModel) {
                gotoNextActivity();

            } else {
                Intent intent = new Intent();
                intent.putExtra(WloModelEditionActivity.INTENT_EXTRA_MODEL, model);
                setResult(RESULT_OK, intent);
                finish();
            }

        } else {
            View root = findViewById(android.R.id.content);

            Collection<String> requiredFields = errors.get(BaseModel.ErrorType.REQUIRED);
            String errorMessage = getString(R.string.required_field_error_message);
            for (String field : requiredFields) {
                View v = root.findViewWithTag(field);
                if (v != null && EditText.class.isAssignableFrom(v.getClass())) {
                    ((EditText) v).setError(errorMessage);
                }
            }

            Collection<String> oneRequiredFields = errors.get(BaseModel.ErrorType.ONE_REQUIRED);
            errorMessage = getString(R.string.one_required_field_error_message);
            for (String field : oneRequiredFields) {
                View v = root.findViewWithTag(field);
                if (v != null && EditText.class.isAssignableFrom(v.getClass())) {
                    ((EditText) v).setError(errorMessage);
                }
            }
        }
    }

    protected void gotoNextActivity() {
        Intent intent = new Intent(this, getNextListActivity());
        intent.putExtra(WloModelEditionActivity.INTENT_EXTRA_PARENT_MODEL, model);
        startActivity(intent);
    }

    /* Protected methods */

    protected void saveModel() {
        WloSqlOpenHelper woh = new WloSqlOpenHelper(this);
        woh.saveData(model);
        woh.close();
    }

    protected void initEditText(int editorId, final String attribute) {
        initEditText(editorId, attribute, null);
    }

    protected void initEditText(int editorId, final String attribute, String defaultValue) {
        EditText editText = (EditText) findViewById(editorId);
        initEditText(editText, attribute, defaultValue);
    }

    protected void initEditText(final EditText editText, final String attribute, String defaultValue) {
        final Class clazz = model.getClass();
        final String firtsLetterUpperCaseAttribute =
                attribute.substring(0, 1).toUpperCase() + attribute.substring(1);

        editText.setTag(attribute);

        try {
            Object value = clazz.getMethod("get" + firtsLetterUpperCaseAttribute).invoke(model);
            editText.setText(value != null ? value.toString() : defaultValue);

        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            Log.e(TAG, "Error on get" + firtsLetterUpperCaseAttribute + " for class " + clazz, e);
        }

        editText.addTextChangedListener(new BaseTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    clazz.getMethod("set" + firtsLetterUpperCaseAttribute, String.class).invoke(model, s.toString());

                } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                    Log.e(TAG, "Error on set" + firtsLetterUpperCaseAttribute + " for class " + clazz, e);
                }
                editText.setError(null);
                editText.setSelection(start + count);
            }

        });

        addModelPropertyChangeListener(editText, attribute, true);
    }

    protected <R> AutoCompleteTextView initAutoCompleteTextView(int autoCompleteTextViewId, final String attribute, Collection<R> data) {
        return initAutoCompleteTextView(autoCompleteTextViewId, attribute, data, null);
    }

    protected <R> AutoCompleteTextView initAutoCompleteTextView(int autoCompleteTextViewId, final String attribute,
                                                                Collection<R> data, Collection<R> favorites) {
        AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) findViewById(autoCompleteTextViewId);
        initAutoCompleteTextView(autoCompleteTextView, attribute, data, favorites);
        return autoCompleteTextView;
    }

    protected <R> void initAutoCompleteTextView(final AutoCompleteTextView autoCompleteTextView, final String attribute, Collection<R> data) {
        initAutoCompleteTextView(autoCompleteTextView, attribute, data, null);
    }

    protected <R> void initAutoCompleteTextView(final AutoCompleteTextView autoCompleteTextView, final String attribute,
                                                Collection<R> data, Collection<R> favorites) {
        final Class clazz = model.getClass();
        final String firtsLetterUpperCaseAttribute =
                attribute.substring(0, 1).toUpperCase() + attribute.substring(1);

        if (!data.isEmpty()) {
            ArrayAdapter<R> adapter = new WloAutoCompleteTextViewWithFavorites.FavoriteAdapter<R>(this, data, favorites);
            autoCompleteTextView.setAdapter(adapter);
        }

        autoCompleteTextView.setThreshold(0);
        autoCompleteTextView.setTag(attribute);
        autoCompleteTextView.addTextChangedListener(new BaseTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                autoCompleteTextView.setError(null);
                autoCompleteTextView.setSelection(start + count);
            }
        });

        try {
            R value = (R) clazz.getMethod("get" + firtsLetterUpperCaseAttribute).invoke(model);
            if (value != null) {
                String text = value.toString();
                autoCompleteTextView.setText(text);
                autoCompleteTextView.dismissDropDown();
            }

        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            Log.e(TAG, "Error on get" + firtsLetterUpperCaseAttribute + " for class " + clazz, e);
        }

        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                R selectedData = (R) parent.getItemAtPosition(position);
                try {
                    clazz.getMethod("set" + firtsLetterUpperCaseAttribute, selectedData.getClass()).invoke(model, selectedData);

                } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                    Log.e(TAG, "Error on set" + firtsLetterUpperCaseAttribute + " for class " + clazz, e);
                }
            }
        });

        addModelPropertyChangeListener(autoCompleteTextView, attribute, false);
    }

    protected void addModelPropertyChangeListener(final EditText editText, String property, final boolean checkFocus) {
        model.addPropertyChangeListener(property, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent event) {
                if (!checkFocus || !editText.isFocused()) {
                    Object newValue = event.getNewValue();
                    String text = newValue != null ? newValue.toString() : getString(fr.ifremer.wlo.R.string.undefined);
                    editText.setText(text);
                }
            }
        });
    }

    protected void openNewModelEdition() {
        Intent intent = new Intent(WloModelEditionActivity.this, WloModelEditionActivity.this.getClass());
        if (HierarchicalModel.class.isAssignableFrom(model.getClass())) {
            HierarchicalModel hModel = (HierarchicalModel) model;
            BaseModel parentModel = hModel.getParent();
            intent.putExtra(WloModelEditionActivity.INTENT_EXTRA_PARENT_MODEL, parentModel);
        }
        startActivity(intent);
    }
}

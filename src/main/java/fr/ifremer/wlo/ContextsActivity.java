package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import fr.ifremer.wlo.models.ContextModel;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class ContextsActivity extends WloBaseListActivity<ContextModel> {

    private static final String TAG = "ContextsActivity";

    /*  Activity methods  */

    @Override
    protected SimpleCursorAdapter createAdapter() {
        return new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, null,
                                       new String[] { ContextModel.COLUMN_NAME },
                                       new int[] { android.R.id.text1 }, 0
        );
    }

    @Override
    protected Cursor getAllData() {
        Cursor cursor = woh.getAllContexts();
        return cursor;
    }

    @Override
    protected ContextModel createNewModel(Cursor cursor) {
        return new ContextModel(cursor);
    }

    @Override
    protected Class<? extends WloModelEditionActivity> getEditionActivity() {
        return ContextFormActivity.class;
    }

    @Override
    protected Integer getSubtitle() {
        return null;
    }

    @Override
    protected Class<? extends WloBaseActivity> getNextActivity() {
        return LocationsActivity.class;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return MainActivity.class;
    }

    @Override
    protected void onResume() {
        super.onResume();

        Cursor cursor = getAllData();
        adapter.swapCursor(cursor);
    }
}

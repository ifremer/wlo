package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.google.common.base.Function;
import fr.ifremer.wlo.models.CommercialSpeciesModel;
import fr.ifremer.wlo.models.ContextModel;
import fr.ifremer.wlo.models.LocationModel;
import fr.ifremer.wlo.models.MeasurementModel;
import fr.ifremer.wlo.models.MetierModel;
import fr.ifremer.wlo.models.ScientificSpeciesModel;
import fr.ifremer.wlo.models.VesselModel;
import fr.ifremer.wlo.models.categorization.CategoryModel;
import fr.ifremer.wlo.preferences.SettingsActivity;
import fr.ifremer.wlo.storage.CsvExporter;
import fr.ifremer.wlo.storage.DataCache;
import fr.ifremer.wlo.storage.JsonExporter;
import fr.ifremer.wlo.storage.WloSqlOpenHelper;
import fr.ifremer.wlo.utils.ImportExportUtil;
import fr.ifremer.wlo.utils.UpdateCheckTask;
import fr.ifremer.wlo.utils.filechooser.FileDialog;
import fr.ifremer.wlo.utils.filechooser.SelectionMode;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.nuiton.csv.Export;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class MainActivity extends WloBaseActivity {

    private static final String TAG = "MainActivity";

    protected static final int REQUEST_ENABLE_BT = 1;
    protected static final int REQUEST_SELECT_EXPORT_FOLDER = 2;

    // Local Bluetooth adapter
    protected BluetoothAdapter mBluetoothAdapter = null;

    protected LinearLayout mainPanel;
    protected LinearLayout logoPanel;
    protected LinearLayout buttonPanel;

    protected Button connectButton;
    protected Button disconnectButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new InitializationTask().execute();
        new UpdateCheckTask(this).execute();

        mainPanel = (LinearLayout) findViewById(R.id.main_panel);
        logoPanel = (LinearLayout) findViewById(R.id.logo_panel);
        buttonPanel = (LinearLayout) findViewById(R.id.button_panel);

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            findViewById(R.id.main_connect_ichtyometer_button).setEnabled(false);
        }

        connectButton = (Button) findViewById(R.id.main_connect_ichtyometer_button);
        disconnectButton = (Button) findViewById(R.id.main_disconnect_ichtyometer_button);

        Configuration config = getResources().getConfiguration();
        setOrientation(config.orientation);

        mMessenger = new Messenger(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case BigFinCommunicationService.MESSAGE_STATE_CHANGE:
                        switch (msg.arg1) {
                            case BigFinCommunicationService.STATE_CONNECTED:
                                bigfinConnected();
                                break;

                            case BigFinCommunicationService.STATE_LISTEN:
                            case BigFinCommunicationService.STATE_NONE:
                                bigfinDisconnected();
                                break;
                        }
                        break;

                    case BigFinCommunicationService.MESSAGE_CONNECTION_FAILED:
                    case BigFinCommunicationService.MESSAGE_CONNECTION_LOST:
                        bigfinDisconnected();
                        break;
                    case BigFinCommunicationService.MESSAGE_CONNECTION_STATE:
                        if (msg.arg1 == BigFinCommunicationService.STATE_CONNECTED) {
                            bigfinConnected();
                        } else {
                            bigfinDisconnected();
                        }
                }
            }
        });
        bigfinDisconnected();
    }

    @Override
    protected void onResume() {
        super.onResume();

        getBigFinConnectionState();
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        super.onServiceConnected(name, service);
        getBigFinConnectionState();
    }

    protected void getBigFinConnectionState() {
        if (mServiceMessenger != null) {
            try {
                Message msg = Message.obtain(null, BigFinCommunicationService.MESSAGE_CONNECTION_STATE);
                msg.replyTo = mMessenger;
                mServiceMessenger.send(msg);

            } catch (RemoteException e) {
                Log.e(TAG, "Error while sending data to the service");
            }
        }
    }

    protected void bigfinConnected() {
        disconnectButton.setVisibility(View.VISIBLE);
        connectButton.setVisibility(View.GONE);
    }

    protected void bigfinDisconnected() {
        connectButton.setVisibility(View.VISIBLE);
        disconnectButton.setVisibility(View.GONE);
    }

    @Override
    protected Integer getContentView() {
        return R.layout.main;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return null;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setOrientation(newConfig.orientation);
    }

    public void openContexts(View source) {
        startActivity(new Intent(this, ContextsActivity.class));
    }

    public void connectIchtyometer(View source) {
        // If BT is not on, request that it be enabled.
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);

        } else {
            selectDevice();
        }
    }

    public void disconnectIchtyometer(View source) {
        Message message = Message.obtain(null, BigFinCommunicationService.MESSAGE_DISCONNECT_DEVICE);
        try {
            mServiceMessenger.send(message);

        } catch (RemoteException e) {
            Log.e(TAG, "Error while sending data to the service");
        }
    }

    public void openSettings(View source) {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    public void export(View source) {
        Intent intent = new Intent(this, FileDialog.class); //Intent to start openIntents File Manager
        intent.putExtra(FileDialog.START_PATH, "/sdcard");
        intent.putExtra(FileDialog.CAN_SELECT_DIR, true);
        intent.putExtra(FileDialog.SELECTION_MODE, SelectionMode.MODE_OPEN);

        //alternatively you can set file filter
        intent.putExtra(FileDialog.FORMAT_FILTER, new String[] { "csv" });

        startActivityForResult(intent, REQUEST_SELECT_EXPORT_FOLDER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
//            case REQUEST_CONNECT_ICHTYOMETER:
//                // When DeviceListActivity returns with a device to connect
//                if (resultCode == Activity.RESULT_OK) {
//                    bigfinConnected();
//                } else {
//                    bigfinDisconnected();
//                }
//                break;

            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so open the device list
                    selectDevice();

                } else {
                    // User did not enable Bluetooth or an error occurred
                    Toast.makeText(this, R.string.bt_not_enabled, Toast.LENGTH_SHORT).show();
                }
                break;

            case REQUEST_SELECT_EXPORT_FOLDER:
                if (resultCode == Activity.RESULT_OK) {
                    try {

                        File selectedFile = new File(data.getStringExtra(FileDialog.RESULT_PATH));
                        if (selectedFile.isFile())  {
                            selectedFile = selectedFile.getParentFile();
                        }
                        String date = String.format("-%1$tY%1$tm%1$td-%1$tH%1$tM%1$tS", new Date());
                        File csvFile = new File(selectedFile, "export" + date + ".csv");

                        CsvExporter exporter = new CsvExporter(';', this);
                        List<MeasurementModel> measurementModelList = getMeasurementModels();
                        Export.exportToFile(exporter, measurementModelList, csvFile);

                        String jsonData = JsonExporter.exportData(this);
                        File jsonFile = new File(selectedFile, "export" + date + ".json");
                        FileUtils.write(jsonFile, jsonData);

                        Toast.makeText(this, "Export effectué dans les fichiers " + csvFile.getAbsolutePath() +
                                " et " + jsonFile.getAbsolutePath(), Toast.LENGTH_LONG).show();

                    } catch (Exception e) {
                        Log.e(TAG, "error while exporting to CSV", e);
                        Toast.makeText(this, "Erreur lors de l'export", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }

    }

    protected List<MeasurementModel> getMeasurementModels() {
        List<MeasurementModel> measurementModelList = new ArrayList<>();

        WloSqlOpenHelper soh = new WloSqlOpenHelper(this);
        Cursor cursor = soh.getAllContexts();
        List<ContextModel> contexts =
                WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                new Function<Cursor, ContextModel>() {
                                                                    @Override
                                                                    public ContextModel apply(Cursor input) {
                                                                        return new ContextModel(input);
                                                                    }
                                                                });
        for (final ContextModel contextModel : contexts) {
            cursor = soh.getAllLocations(contextModel.getId());
            List<LocationModel> locationModels =
                    WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                   new Function<Cursor, LocationModel>() {
                                                                       @Override
                                                                       public LocationModel apply(Cursor input) {
                                                                           LocationModel locationModel = new LocationModel(MainActivity.this, input);
                                                                           locationModel.setParent(contextModel);
                                                                           return locationModel;
                                                                       }
                                                                   });
            for (final LocationModel locationModel : locationModels) {
                cursor = soh.getAllVessels(locationModel.getId());
                List<VesselModel> vesselModels =
                        WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                       new Function<Cursor, VesselModel>() {
                                                                           @Override
                                                                           public VesselModel apply(Cursor input) {
                                                                               VesselModel vesselModel = new VesselModel(MainActivity.this, input);
                                                                               vesselModel.setParent(locationModel);
                                                                               return vesselModel;
                                                                           }
                                                                       });
                for (final VesselModel vesselModel : vesselModels) {
                    cursor = soh.getAllMetiers(vesselModel.getId());
                    List<MetierModel> metierModels =
                            WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                           new Function<Cursor, MetierModel>() {
                                                                               @Override
                                                                               public MetierModel apply(Cursor input) {
                                                                                   MetierModel metierModel = new MetierModel(MainActivity.this, input);
                                                                                   metierModel.setParent(vesselModel);
                                                                                   return metierModel;
                                                                               }
                                                                           });
                    for (final MetierModel metierModel : metierModels) {
                        cursor = soh.getAllCommercialSpecies(metierModel.getId());
                        List<CommercialSpeciesModel> commercialSpeciesModels =
                                WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                               new Function<Cursor, CommercialSpeciesModel>() {
                                                                                   @Override
                                                                                   public CommercialSpeciesModel apply(Cursor input) {
                                                                                       CommercialSpeciesModel commercialSpeciesModel = new CommercialSpeciesModel(MainActivity.this, input);
                                                                                       commercialSpeciesModel.setParent(metierModel);
                                                                                       return commercialSpeciesModel;
                                                                                   }
                                                                               });
                        for (final CommercialSpeciesModel commercialSpeciesModel : commercialSpeciesModels) {
                            cursor = soh.getAllScientificSpecies(commercialSpeciesModel.getId());
                            List<ScientificSpeciesModel> scientificSpeciesModels =
                                    WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                                   new Function<Cursor, ScientificSpeciesModel>() {
                                                                                       @Override
                                                                                       public ScientificSpeciesModel apply(Cursor input) {
                                                                                           ScientificSpeciesModel scientificSpeciesModel = new ScientificSpeciesModel(MainActivity.this, input);
                                                                                           scientificSpeciesModel.setParent(commercialSpeciesModel);
                                                                                           return scientificSpeciesModel;
                                                                                       }
                                                                                   });

                            for (final ScientificSpeciesModel scientificSpeciesModel : scientificSpeciesModels) {
                                Map<Triple<String, String, String>, Integer> categoryWeights = new HashMap<>();
                                cursor = soh.getAllCategoryWeigths(scientificSpeciesModel.getId());
                                boolean cont = cursor.moveToFirst();
                                while (cont) {
                                    String category1 = cursor.getString(1);
                                    String category2 = cursor.getString(2);
                                    String category3 = cursor.getString(3);
                                    Integer weight = null;
                                    if (!cursor.isNull(4)) {
                                        weight = cursor.getInt(4);
                                    }
                                    Triple<String, String, String> key = Triple.of(category1, category2, category3);
                                    categoryWeights.put(key, weight);

                                    cont = cursor.moveToNext();
                                }
                                scientificSpeciesModel.setCategoryWeights(categoryWeights);

                                cursor = soh.getAllMeasurements(scientificSpeciesModel.getId());
                                List<MeasurementModel> measurementModels =
                                        WloSqlOpenHelper.transformCursorIntoCollection(cursor,
                                                                                       new Function<Cursor, MeasurementModel>() {
                                                                                           @Override
                                                                                           public MeasurementModel apply(Cursor input) {
                                                                                               MeasurementModel measurementModel = new MeasurementModel(input);
                                                                                               measurementModel.setParent(scientificSpeciesModel);
                                                                                               return measurementModel;
                                                                                           }
                                                                                       });
                                measurementModelList.addAll(measurementModels);
                            }
                        }
                    }
                }
            }
        }
        return measurementModelList;
    }

    protected void setOrientation(int orientation) {
        LinearLayout.LayoutParams logoParams, buttonParams;

        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            logoParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                                       LinearLayout.LayoutParams.MATCH_PARENT);
            logoParams.weight = 1.0f;

            buttonParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                                         LinearLayout.LayoutParams.MATCH_PARENT);
            buttonParams.weight = 1.0f;

        } else {
            logoParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                                       LinearLayout.LayoutParams.WRAP_CONTENT);
            logoParams.weight = 1.0f;

            buttonParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                                         LinearLayout.LayoutParams.WRAP_CONTENT);
            buttonParams.weight = 0.0f;
        }
        logoPanel.setLayoutParams(logoParams);
        buttonPanel.setLayoutParams(buttonParams);
        mainPanel.setOrientation(orientation);
    }

    protected void selectDevice() {
        // Launch the DeviceListActivity to see devices and do scan
        Intent serverIntent = new Intent(this, DeviceListActivity.class);
        startActivity(serverIntent);
    }

    protected class InitializationTask extends AsyncTask<Void, Integer, Void> {

        protected ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(MainActivity.this);
            dialog.setIndeterminate(false);
            dialog.setCancelable(false);
            dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dialog.setMax(9);
            dialog.setMessage(getString(R.string.main_loading_referential));
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Context context = MainActivity.this;
            try {
                int i = 1;
                if (DataCache.getAllCommercialSpecies(context).isEmpty()) {
                    ImportExportUtil.importCommercialSpecies(context, getAssets().open("ref_import_commercial_species.csv"));
                }
                publishProgress(i++);

                if (DataCache.getAllLocations(context).isEmpty()) {
                    ImportExportUtil.importLocations(context, getAssets().open("ref_import_locations.csv"));
                }
                publishProgress(i++);

                if (DataCache.getAllMensurations(context).isEmpty()) {
                    ImportExportUtil.importMensurations(context, getAssets().open("ref_import_mensurations.csv"));
                }
                publishProgress(i++);

                if (DataCache.getAllMetiers(context).isEmpty()) {
                    ImportExportUtil.importMetiers(context, getAssets().open("ref_import_metiers.csv"));
                }
                publishProgress(i++);

                if (DataCache.getAllPresentations(context).isEmpty()) {
                    ImportExportUtil.importPresentations(context, getAssets().open("ref_import_presentations.csv"));
                }
                publishProgress(i++);

                if (DataCache.getAllScientificSpecies(context).isEmpty()) {
                    ImportExportUtil.importScientificSpecies(context, getAssets().open("ref_import_scientific_species.csv"));
                }
                publishProgress(i++);

                if (DataCache.getAllStates(context).isEmpty()) {
                    ImportExportUtil.importStates(context, getAssets().open("ref_import_states.csv"));
                }
                publishProgress(i++);

                if (DataCache.getAllVessels(context).isEmpty()) {
                    ImportExportUtil.importVessels(context, getAssets().open("ref_import_vessels.csv"));
                }
                publishProgress(i++);

                if (DataCache.getAllCategories(context).isEmpty()) {
                    WloSqlOpenHelper soh = new WloSqlOpenHelper(context);

                    CategoryModel ageCategory = new CategoryModel();
                    ageCategory.setLabel(getString(R.string.age));
                    soh.saveData(ageCategory);

                    CategoryModel genderCategory = new CategoryModel();
                    genderCategory.setLabel(getString(R.string.gender));
                    soh.saveData(genderCategory);

                    CategoryModel maturityCategory = new CategoryModel();
                    maturityCategory.setLabel(getString(R.string.maturity));
                    soh.saveData(maturityCategory);

                    soh.saveData(ImportExportUtil.importQualitativeValues(ageCategory, getAssets().open("ref_import_ages.csv")));
                    soh.saveData(ImportExportUtil.importQualitativeValues(genderCategory, getAssets().open("ref_import_genders.csv")));
                    soh.saveData(ImportExportUtil.importQualitativeValues(maturityCategory, getAssets().open("ref_import_maturities.csv")));

                    soh.close();
                }
                publishProgress(i++);

            } catch (IOException | RuntimeException e) {
                Log.e(TAG, "error on initial import", e);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            dialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
        }

    }

}

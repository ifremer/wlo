package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.TextView;
import com.google.common.collect.Lists;
import fr.ifremer.wlo.models.LocationModel;
import fr.ifremer.wlo.models.referentials.Location;
import fr.ifremer.wlo.preferences.StringPreference;
import fr.ifremer.wlo.storage.DataCache;
import fr.ifremer.wlo.utils.DatePickerFragment;
import fr.ifremer.wlo.utils.TimePickerFragment;
import fr.ifremer.wlo.utils.UIUtils;

import java.util.Calendar;
import java.util.List;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class LocationFormActivity extends WloModelEditionActivity<LocationModel> {

    private static final String TAG = "LocationFormActivity";

    @Override
    protected Integer getContentView() {
        return R.layout.location_form;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return LocationsActivity.class;
    }

    @Override
    protected Class<? extends WloModelEditionActivity> getNextEditionActivity() {
        return VesselFormActivity.class;
    }

    @Override
    protected Class<? extends WloBaseListActivity> getNextListActivity() {
        return VesselsActivity.class;
    }

    @Override
    protected LocationModel createNewModel() {
        return new LocationModel();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // init editors
        String defaultOperator = null;
        if (model.isNew()) {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
            defaultOperator = sharedPref.getString(StringPreference.DEFAULT_OPERATOR.getKey(), null);
        }
        initEditText(R.id.location_form_operator, LocationModel.COLUMN_OPERATOR, defaultOperator);

        List<Location> locations = Lists.newArrayList(DataCache.getAllLocations(this));
        initAutoCompleteTextView(R.id.location_form_location, LocationModel.COLUMN_LOCATION, locations);

        TextView startDateTextView = (TextView) findViewById(R.id.location_form_start_date);
        TextView startTimeTextView = (TextView) findViewById(R.id.location_form_start_time);
        TextView endDateTextView = (TextView) findViewById(R.id.location_form_end_date);
        TextView endTimeTextView = (TextView) findViewById(R.id.location_form_end_time);

        String dateFormat = UIUtils.getDateFormat(this);
        // start date
        Calendar startDate = model.getStartDate();
        if (startDate != null) {
            startDateTextView.setText(String.format(dateFormat, startDate.getTime()));
            startTimeTextView.setText(getString(R.string.time_format, startDate.getTime()));
        }

        // end date
        Calendar endDate = model.getEndDate();
        if (endDate != null) {
            endDateTextView.setText(String.format(dateFormat, endDate.getTime()));
            endTimeTextView.setText(getString(R.string.time_format, endDate.getTime()));
        }

        initEditText(R.id.form_comment, LocationModel.COLUMN_COMMENT);
    }

    public void pickDate(View v) {
        String attribute;
        switch (v.getId()) {
            case R.id.location_form_start_date:
                attribute = LocationModel.COLUMN_START_DATE;
                break;

            case R.id.location_form_end_date:
                attribute = LocationModel.COLUMN_END_DATE;
                break;

            default:
                return;
        }
        DialogFragment newFragment = new DatePickerFragment(model, attribute, (TextView) v);
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void pickTime(View v) {
        String attribute;
        switch (v.getId()) {
            case R.id.location_form_start_time:
                attribute = LocationModel.COLUMN_START_DATE;
                break;

            case R.id.location_form_end_time:
                attribute = LocationModel.COLUMN_END_DATE;
                break;

            default:
                return;
        }
        DialogFragment newFragment = new TimePickerFragment(model, attribute, (TextView) v);
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

}

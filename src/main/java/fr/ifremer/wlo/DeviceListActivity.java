/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;

/**
 * This Activity appears as a dialog. It lists any paired devices and
 * devices detected in the area after discovery. When a device is chosen
 * by the user, the MAC address of the device is sent back to the parent
 * Activity in the result Intent.
 */
public class DeviceListActivity extends ListActivity implements ServiceConnection {
    // Debugging
    private static final String TAG = "DeviceListActivity";

    // Member fields
    protected BluetoothAdapter mBtAdapter;
    protected ArrayAdapter<String> mPairedDevicesArrayAdapter;

    protected Messenger mServiceMessenger = null;
    protected Messenger mMessenger = new Messenger(new IncomingHandler());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.device_list);

        mPairedDevicesArrayAdapter = new ArrayAdapter<String>(this, R.layout.device_name);

        // Find and set up the ListView for paired devices
        ListView pairedListView = getListView();
        pairedListView.setAdapter(mPairedDevicesArrayAdapter);
        pairedListView.setOnItemClickListener(mDeviceClickListener);

        // Get the local Bluetooth adapter
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        // Get a set of currently paired devices
        Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();

        // If there are paired devices, add each one to the ArrayAdapter
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                mPairedDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
            }
        }

        bindService(new Intent(this, BigFinCommunicationService.class), this, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Make sure we're not doing discovery anymore
        if (mBtAdapter != null) {
            mBtAdapter.cancelDiscovery();
        }

        doUnbindService();
    }

    /**
     * Un-bind this Activity to MyService
     */
    protected void doUnbindService() {
        // If we have received the service, and hence registered with it, then now is the time to unregister.
        if (mServiceMessenger != null) {
            try {
                Message msg = Message.obtain(null, BigFinCommunicationService.MESSAGE_UNREGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mServiceMessenger.send(msg);

            } catch (RemoteException e) {
                Log.e(TAG, "Error while sending data to the service");
            }
        }
        // Detach our existing connection.
        unbindService(this);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mServiceMessenger = new Messenger(service);
        try {
            Message msg = Message.obtain(null, BigFinCommunicationService.MESSAGE_REGISTER_CLIENT);
            msg.replyTo = mMessenger;
            mServiceMessenger.send(msg);
        }
        catch (RemoteException e) {
            Log.e(TAG, "Error while sending data to the service");
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mServiceMessenger = null;
    }

    // The on-click listener for all devices in the ListViews
    protected OnItemClickListener mDeviceClickListener = new OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {
            // Cancel discovery because it's costly and we're about to connect
            mBtAdapter.cancelDiscovery();

            // Get the device MAC address, which is the last 17 chars in the View
            String info = ((TextView) v).getText().toString();
            String address = info.substring(info.length() - 17);

            // Attempt to connect to the device
            Message message = Message.obtain(null, BigFinCommunicationService.MESSAGE_CONNECT_DEVICE);
            Bundle bundle = new Bundle();
            bundle.putString(BigFinCommunicationService.DEVICE_ADDRESS, address);
            message.setData(bundle);
            try {
                mServiceMessenger.send(message);

            } catch (RemoteException e) {
                Log.e(TAG, "Error while sending data to the service");
            }

        }
    };

    // The Handler that gets information back from the BluetoothChatService
    class IncomingHandler extends Handler {

        ProgressDialog dialog;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BigFinCommunicationService.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BigFinCommunicationService.STATE_CONNECTED:

                            dialog.dismiss();
                            setResult(RESULT_OK);
                            finish();

                            break;

                        case BigFinCommunicationService.STATE_CONNECTING:
                            dialog = ProgressDialog.show(DeviceListActivity.this, "",
                                                         getString(R.string.connecting_device), true);
                            break;

                    }
                    break;

                case BigFinCommunicationService.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    break;

                case BigFinCommunicationService.MESSAGE_READ:
                    // construct a string from the valid bytes in the buffer
                    break;

                case BigFinCommunicationService.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    String mConnectedDeviceName = msg.getData().getString(BigFinCommunicationService.DEVICE_NAME);
                    Toast.makeText(getApplicationContext(),
                                   getString(R.string.connected_to_device, mConnectedDeviceName),
                                   Toast.LENGTH_SHORT).show();
                    break;

                case BigFinCommunicationService.MESSAGE_CONNECTION_FAILED:
                case BigFinCommunicationService.MESSAGE_CONNECTION_LOST:
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    Toast.makeText(getApplicationContext(), msg.getData().getString(BigFinCommunicationService.TOAST),
                                   Toast.LENGTH_SHORT).show();
                    break;
            }
        }

    };

}

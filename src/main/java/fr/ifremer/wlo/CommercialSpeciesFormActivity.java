package fr.ifremer.wlo;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.wlo.models.CommercialSpeciesModel;
import fr.ifremer.wlo.models.categorization.CategoryModel;
import fr.ifremer.wlo.models.referentials.CommercialSpecies;
import fr.ifremer.wlo.models.referentials.Mensuration;
import fr.ifremer.wlo.models.referentials.Presentation;
import fr.ifremer.wlo.models.referentials.State;
import fr.ifremer.wlo.preferences.MultiSelectItemPreference;
import fr.ifremer.wlo.storage.DataCache;
import fr.ifremer.wlo.storage.WloSqlOpenHelper;
import fr.ifremer.wlo.utils.WloAutoCompleteTextViewWithFavorites;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class CommercialSpeciesFormActivity extends WloModelEditionActivity<CommercialSpeciesModel> {

    private static final String TAG = "CommercialSpeciesFormActivity";

    @Override
    protected Integer getContentView() {
        return R.layout.commercial_species_form;
    }

    @Override
    protected Class<? extends WloBaseActivity> getUpActivity() {
        return CommercialSpeciesActivity.class;
    }

    @Override
    protected Class<? extends WloModelEditionActivity> getNextEditionActivity() {
        return ScientificSpeciesFormActivity.class;
    }

    @Override
    protected Class<? extends WloBaseListActivity> getNextListActivity() {
        return ScientificSpeciesActivity.class;
    }

    @Override
    protected CommercialSpeciesModel createNewModel() {
        return new CommercialSpeciesModel();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // editors

        // init editors
        List<CommercialSpecies> commercialSpecies = Lists.newArrayList(DataCache.getAllCommercialSpecies(this));
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        final Set<String> favoriteIds = sharedPref.getStringSet(MultiSelectItemPreference.COMMERCIAL_SPECIES_FAVORITES.getKey(),
                                                                Sets.<String>newHashSet());
        Collection<CommercialSpecies> favorites = Collections2.filter(commercialSpecies, new Predicate<CommercialSpecies>() {
            @Override
            public boolean apply(CommercialSpecies input) {
                return favoriteIds.contains(input.getId());
            }
        });

        WloAutoCompleteTextViewWithFavorites actvwf = (WloAutoCompleteTextViewWithFavorites) findViewById(R.id.commercial_species_form_fao_code);
        initAutoCompleteTextView(actvwf.getAutoCompleteTextView(), CommercialSpeciesModel.COLUMN_FAO_CODE, commercialSpecies, favorites);
        actvwf.useFavorites(!favorites.isEmpty());

        if (model.getMeasurementMethod() == null) {
            Mensuration defaultMensuration = DataCache.getDefaultMensuration(this);
            model.setMeasurementMethod(defaultMensuration);
        }
        List<Mensuration> mensurations = Lists.newArrayList(DataCache.getAllMensurations(this));
        initAutoCompleteTextView(R.id.commercial_species_form_measurement_method, CommercialSpeciesModel.COLUMN_MEASUREMENT_METHOD, mensurations);

        Spinner precisionSpinner = (Spinner) findViewById(R.id.commercial_species_form_precision);
        ArrayAdapter<Mensuration.Precision> precisions = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                                                                            Mensuration.Precision.values());
        precisionSpinner.setAdapter(precisions);
        precisionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                model.setPrecision((Mensuration.Precision) adapterView.getItemAtPosition(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                model.setPrecision(null);
            }
        });
        precisionSpinner.setSelection(model.getPrecision().ordinal());
        boolean precisionEnabled = model.isNew();
        if (!precisionEnabled) {
            WloSqlOpenHelper soh = new WloSqlOpenHelper(this);
            Cursor cursor = soh.getAllMeasurementsForCommercialSpecies(model.getId());
            int measurementNb = cursor.getCount();
            precisionEnabled = measurementNb == 0;
        }
        precisionSpinner.setEnabled(precisionEnabled);

        Set<String> sortCategories = DataCache.getAllSortCategories(this);
        sortCategories.remove("");
        initEditText(R.id.commercial_species_form_sortCategory, CommercialSpeciesModel.COLUMN_SORT_CATEGORY);
        initAutoCompleteTextView(R.id.commercial_species_form_sortCategory, CommercialSpeciesModel.COLUMN_SORT_CATEGORY, sortCategories);

        List<State> states = Lists.newArrayList(DataCache.getAllStates(this));
        initAutoCompleteTextView(R.id.commercial_species_form_state, CommercialSpeciesModel.COLUMN_STATE, states);

        List<Presentation> presentations = Lists.newArrayList(DataCache.getAllPresentations(this));
        initAutoCompleteTextView(R.id.commercial_species_form_presentation, CommercialSpeciesModel.COLUMN_PRESENTATION, presentations);

        CheckBox speciesMixEditor = (CheckBox) findViewById(R.id.commercial_species_form_species_mix);
        speciesMixEditor.setChecked(model.isSpeciesMix());
        speciesMixEditor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                model.setSpeciesMix(b);
            }
        });
        boolean speciesMixEnabled = model.isNew() || !model.isSpeciesMix();
        if (!speciesMixEnabled) {
            WloSqlOpenHelper soh = new WloSqlOpenHelper(this);
            Cursor cursor = soh.getAllScientificSpecies(model.getId());
            int scientificSpeciesNb = cursor.getCount();
            speciesMixEnabled = scientificSpeciesNb < 2;
        }
        speciesMixEditor.setEnabled(speciesMixEnabled);

        initEditText(R.id.form_comment, CommercialSpeciesModel.COLUMN_COMMENT);

        initCategorySpinner((Spinner) findViewById(R.id.category1_spinner),
                            model.getCategory1(),
                            new Function<CategoryModel, Void>() {
            @Override
            public Void apply(CategoryModel input) {
                model.setCategory1(input);
                return null;
            }
        });
        initCategorySpinner((Spinner) findViewById(R.id.category2_spinner),
                            model.getCategory2(),
                            new Function<CategoryModel, Void>() {
            @Override
            public Void apply(CategoryModel input) {
                model.setCategory2(input);
                return null;
            }
        });
        initCategorySpinner((Spinner) findViewById(R.id.category3_spinner),
                            model.getCategory3(),
                            new Function<CategoryModel, Void>() {
            @Override
            public Void apply(CategoryModel input) {
                model.setCategory3(input);
                return null;
            }
        });

    }

    protected void initCategorySpinner(Spinner spinner,
                                       CategoryModel category,
                                       final Function<CategoryModel, Void> setCategoryFunction) {
        List<Object> categories = new ArrayList<Object>(DataCache.getAllCategories(this));
        categories.add(0, "");
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, categories);
        spinner.setAdapter(adapter);
        spinner.setSelection(adapter.getPosition(category));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CategoryModel category;
                if (position == 0) {
                    category = null;
                } else {
                    category = (CategoryModel) parent.getItemAtPosition(position);
                    category = DataCache.getCategoryById(CommercialSpeciesFormActivity.this, category.getId());
                }
                setCategoryFunction.apply(category);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                setCategoryFunction.apply(null);
            }
        });
    }

    @Override
    protected void saveModel() {
        super.saveModel();
        DataCache.invalidateSortCategories();
    }
}

package fr.ifremer.wlo.utils;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.TextView;
import com.google.common.base.Preconditions;
import fr.ifremer.wlo.models.BaseModel;

import java.lang.reflect.InvocationTargetException;
import java.util.Calendar;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public abstract class AbstractDateTimePickerFragment extends DialogFragment {

    private static final String TAG = "AbstractDateTimePickerFragment";

    protected Calendar date;
    protected TextView view;
    protected BaseModel model;
    protected String attribute;

    public AbstractDateTimePickerFragment(BaseModel model, String attribute, TextView view) {
        Preconditions.checkNotNull(model);
        Preconditions.checkNotNull(attribute);
        this.view = view;
        this.model = model;
        this.attribute = attribute.substring(0, 1).toUpperCase() + attribute.substring(1);

        Class clazz = model.getClass();
        try {
            date = (Calendar) clazz.getMethod("get" + this.attribute).invoke(model);

        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            Log.e(TAG, "Error on get" + this.attribute + " for class " + clazz, e);
        }

        if (date == null) {
            date = Calendar.getInstance();
        }
    }

    protected void setDateInModel() {
        Class clazz = model.getClass();
        try {
            clazz.getMethod("set" + attribute, Calendar.class).invoke(model, date);

        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            Log.e(TAG, "Error on set" + attribute + " for class " + clazz, e);
        }
    }

}

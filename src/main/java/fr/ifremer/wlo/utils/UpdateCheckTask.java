package fr.ifremer.wlo.utils;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import fr.ifremer.wlo.R;
import fr.ifremer.wlo.WloBaseActivity;
import org.nuiton.util.Version;
import org.nuiton.util.VersionUtil;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class UpdateCheckTask extends AsyncTask<String, Integer, String> {

    private static final String TAG = "UpdateCheckTask";

    public static final String UPDATE_URL = "http://wlo.codelutin.com/update/wlo-update.properties";
    public static final String VERSION_PROP = "application.version";
    public static final String URL_PROP = "application.url";

    protected Context context;
    protected boolean showLoading;
    protected ProgressDialog loadingDialog;

    public UpdateCheckTask(Context context) {
        this(context, false);
    }

    public UpdateCheckTask(Context context, boolean showLoading) {
        this.context = context;
        this.showLoading = showLoading;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (showLoading) {
            loadingDialog = new ProgressDialog(context);
            loadingDialog.setIndeterminate(true);
            loadingDialog.setMessage(context.getString(R.string.checking_update));
            loadingDialog.show();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String result;

        if (WloBaseActivity.getDownloadingApkId() != null) {
            result = null;
        } else {
            try {
                // Create a URL for the desired page
                URL url = new URL(UPDATE_URL);

                Properties props = new Properties();
                props.load(url.openStream());
                String version = props.getProperty(VERSION_PROP);

                String currentVersion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;

                Version newVersion = new Version(version);
                if (!newVersion.isSnapshot() && VersionUtil.greaterThan(version, currentVersion)) {
                    result = props.getProperty(URL_PROP);

                } else {
                    result = null;
                }

            } catch (IOException | PackageManager.NameNotFoundException e) {
                Log.e(TAG, "error while getting the version");
                cancel(true);
                result = null;
            }
        }

        return result;
    }

    @Override
    protected void onPostExecute(final String result) {
        super.onPostExecute(result);
        if (showLoading) {
            loadingDialog.dismiss();
        }
        if (result != null && WloBaseActivity.getDownloadingApkId() == null) {

            new AlertDialog.Builder(context)
                    .setTitle(R.string.update_available_title)
                    .setMessage(R.string.update_available_message)
                    .setNegativeButton(R.string.no, UIUtils.getCancelClickListener())
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                            Uri toDownload = Uri.parse(result);
                            DownloadManager.Request request = new DownloadManager.Request(toDownload);
                            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, toDownload.getLastPathSegment());
                            long enqueue = downloadManager.enqueue(request);
                            WloBaseActivity.setDownloadingApkId(enqueue);
                        }
                    })
                    .create()
                    .show();

        } else if (showLoading) {
            Toast.makeText(context, R.string.no_update, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onCancelled(String s) {
        super.onCancelled(s);
        if (showLoading) {
            loadingDialog.dismiss();
            Toast.makeText(context, R.string.update_error, Toast.LENGTH_LONG).show();
        }
    }
}

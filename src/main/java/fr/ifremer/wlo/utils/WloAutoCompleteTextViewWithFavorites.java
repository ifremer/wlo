package fr.ifremer.wlo.utils;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ToggleButton;
import com.google.common.collect.Lists;
import fr.ifremer.wlo.R;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.2
 */
public class WloAutoCompleteTextViewWithFavorites extends FrameLayout {

    private static final String TAG = "WloAutoCompleteTextViewWithFavorites";

    protected WloAutoCompleteTextView autoCompleteTextView;
    protected ToggleButton toggleButton;

    public WloAutoCompleteTextViewWithFavorites(Context context) {
        super(context);
        init(context);
    }

    public WloAutoCompleteTextViewWithFavorites(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public WloAutoCompleteTextViewWithFavorites(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    protected void init(Context context) {
        inflate(context, R.layout.autocomplete_text_view_with_favorites, this);
        autoCompleteTextView = (WloAutoCompleteTextView) findViewById(R.id.autocomplete_text_view);
        toggleButton = (ToggleButton) findViewById(R.id.favorite_button);
        toggleButton.setChecked(false);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String s = autoCompleteTextView.getText().toString();
                autoCompleteTextView.setText(null);
                Adapter adapter = autoCompleteTextView.getAdapter();
                if (FavoriteAdapter.class.isAssignableFrom(adapter.getClass())) {
                    FavoriteAdapter favoriteAdapter = (FavoriteAdapter) autoCompleteTextView.getAdapter();
                    favoriteAdapter.useFavorites(isChecked);
                }
                autoCompleteTextView.setText(s);
            }
        });
    }

    public WloAutoCompleteTextView getAutoCompleteTextView() {
        return autoCompleteTextView;
    }

    public void useFavorites(boolean useFavorites) {
        toggleButton.setChecked(useFavorites);
    }

    public static class FavoriteAdapter<M> extends ArrayAdapter<M> {

        protected Collection<M> allData;
        protected Collection<M> favoriteData;

        public FavoriteAdapter(Context context, Collection<M> allData, Collection<M> favoriteData) {
            super(context, android.R.layout.simple_dropdown_item_1line, Lists.newArrayList(allData));
            this.allData = Lists.newArrayList(allData);
            if (favoriteData != null) {
                this.favoriteData = Lists.newArrayList(favoriteData);
            }
        }

        protected void useFavorites(boolean favorites) {
            if (CollectionUtils.isNotEmpty(favoriteData)) {
                clear();
                if (favorites) {
                    addAll(favoriteData);
                } else {
                    addAll(allData);
                }
                notifyDataSetInvalidated();
            }
        }
    }
}

package fr.ifremer.wlo.utils;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import com.google.common.base.Preconditions;
import fr.ifremer.wlo.R;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.models.referentials.Mensuration;
import fr.ifremer.wlo.preferences.ListItemPreference;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class UIUtils {

    private static final String TAG = "UIUtils";

    public static final SimpleDateFormat UTC_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");

    /**
     * @return A dialog click listener which cancel the dialog
     */
    public static DialogInterface.OnClickListener getCancelClickListener() {
        return new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        };
    }

    /**
     * Method to get a default string if the one given is null
     * @param s the string to check
     * @param defaultId the id of the string to return if s is null
     * @param context the location
     * @return s or the string with id defaultId
     */
    public static String getStringOrDefault(String s, int defaultId, Context context) {
        if (s != null) {
            return s;
        }
        return context.getString(defaultId);
    }

    /**
     * Method to get "undefined" if the one given is null
     * @param model the model to check
     * @param context the location
     * @return s or "undefined"
     */
    public static String getStringOrUndefined(BaseModel model, Context context) {
        String s = model != null ? model.toString(context) : null;
        return getStringOrDefault(s, R.string.undefined, context);
    }

    /**
     * Method to get "undefined" if the one given is null
     * @param s the string to check
     * @param context the location
     * @return s or "undefined"
     */
    public static String getStringOrUndefined(String s, Context context) {
        return getStringOrDefault(s, R.string.undefined, context);
    }

    /**
     * Method to get "undefined" if the one given is null
     * @param cal the calender to check
     * @param context the location
     * @return the formatted date or "undefined"
     */
    public static String getDateOrUndefined(Calendar cal, Context context) {
        if (cal == null) {
            return context.getString(R.string.undefined);
        }
        String dateFormat = getDateFormat(context);
        return String.format(dateFormat, cal);
    }

    public static String getDateFormat(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String defaultFormat = context.getResources().getStringArray(ListItemPreference.DATE_FORMAT.getValuesArrayId())[0];
        String dateFormat = sharedPref.getString(ListItemPreference.DATE_FORMAT.getKey(), defaultFormat);
        return dateFormat;
    }

    public static Calendar getCalendarFromCursor(Cursor cursor, int columnIndex) {
        Calendar cal;
        if (cursor.getType(columnIndex) == Cursor.FIELD_TYPE_NULL) {
            cal = null;
        } else {
            long time = cursor.getLong(columnIndex);
            cal = Calendar.getInstance();
            cal.setTimeInMillis(time);
        }
        return cal;
    }

    public static String getSizeFormat(Mensuration.Precision precision) {
        String sizeFormat;
        if (precision == Mensuration.Precision.MM5) {
            sizeFormat = "%.1f";
        } else {
            sizeFormat = "%.0f";
        }
        return sizeFormat;
    }

    public static String getFormattedSize(int size, Mensuration.Precision precision) {
        Preconditions.checkNotNull(precision);

        // round the size down, otherwise, the string.format method will round it up (eg 25.8 -> 26)
        // cf http://forge.codelutin.com/issues/4805
        int precisionValue = precision.getValue();
        int roundedSize = (size / precisionValue) * precisionValue;

        String format = getSizeFormat(precision);
        int divider = precision.getUnitDivider();
        double dSize = (double) roundedSize / divider;

        String result = String.format(Locale.US, format, dSize);
        return result;
    }
}

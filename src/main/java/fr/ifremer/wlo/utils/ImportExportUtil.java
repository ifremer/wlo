package fr.ifremer.wlo.utils;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ifremer.wlo.R;
import fr.ifremer.wlo.imports.CalcifiedPartTakingRowModel;
import fr.ifremer.wlo.imports.CommercialSpeciesRowModel;
import fr.ifremer.wlo.imports.LocationRowModel;
import fr.ifremer.wlo.imports.MensurationRowModel;
import fr.ifremer.wlo.imports.MetierRowModel;
import fr.ifremer.wlo.imports.PresentationRowModel;
import fr.ifremer.wlo.imports.QualitativeValueRowModel;
import fr.ifremer.wlo.imports.ScientificSpeciesRowModel;
import fr.ifremer.wlo.imports.StateRowModel;
import fr.ifremer.wlo.imports.VesselRowModel;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.models.categorization.CategoryModel;
import fr.ifremer.wlo.models.categorization.QualitativeValueModel;
import fr.ifremer.wlo.models.referentials.CalcifiedPartTaking;
import fr.ifremer.wlo.storage.DataCache;
import fr.ifremer.wlo.storage.WloSqlOpenHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.Range;
import org.nuiton.csv.Export;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class ImportExportUtil {

    private static final String TAG = "ImportExportUtil";

    public static final char CSV_SEPARATOR = ';';

    public static int importCommercialSpecies(Context context, String path) {
        try {
            return importCommercialSpecies(context, new FileInputStream(path));
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File " + path + " not found", e);
        }
        return 0;
    }

    public static int importCommercialSpecies(Context context, InputStream inputStream) {
        CommercialSpeciesRowModel commercialSpeciesRowModel = new CommercialSpeciesRowModel(CSV_SEPARATOR);
        int result = importData(context, commercialSpeciesRowModel, inputStream);
        DataCache.invalidateCommercialSpecies();
        return result;
    }

    public static int exportCommercialSpecies(Context context, String path) throws Exception {
        Preconditions.checkNotNull(path);
        CommercialSpeciesRowModel commercialSpeciesRowModel = new CommercialSpeciesRowModel(CSV_SEPARATOR);
        int result = exportData(context,
                                commercialSpeciesRowModel,
                                DataCache.getAllCommercialSpecies(context),
                                path,
                                R.string.preferences_export_commercial_species_default_name);
        return result;
    }

    public static int importLocations(Context context, String path) {
        try {
            return importLocations(context, new FileInputStream(path));
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File " + path + " not found", e);
        }
        return 0;
    }

    public static int importLocations(Context context, InputStream inputStream) {
        LocationRowModel locationRowModel = new LocationRowModel(CSV_SEPARATOR);
        int result = importData(context, locationRowModel, inputStream);
        DataCache.invalidateLocations();
        return result;
    }

    public static int exportLocations(Context context, String path) throws Exception {
        Preconditions.checkNotNull(path);
        LocationRowModel locationRowModel = new LocationRowModel(CSV_SEPARATOR);
        int result = exportData(context,
                                locationRowModel,
                                DataCache.getAllLocations(context),
                                path,
                                R.string.preferences_export_locations_default_name);
        return result;
    }

    public static int importMensurations(Context context, String path) {
        try {
            return importMensurations(context, new FileInputStream(path));
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File " + path + " not found", e);
        }
        return 0;
    }

    public static int importMensurations(Context context, InputStream inputStream) {
        MensurationRowModel mensurationRowModel = new MensurationRowModel(CSV_SEPARATOR);
        int result = importData(context, mensurationRowModel, inputStream);
        DataCache.invalidateMensurations();
        return result;
    }

    public static int exportMensurations(Context context, String path) throws Exception {
        Preconditions.checkNotNull(path);
        MensurationRowModel mensurationRowModel = new MensurationRowModel(CSV_SEPARATOR);
        int result = exportData(context,
                                mensurationRowModel,
                                DataCache.getAllMensurations(context),
                                path,
                                R.string.preferences_export_mensurations_default_name);
        return result;
    }

    public static int importMetiers(Context context, String path) {
        try {
            return importMetiers(context, new FileInputStream(path));
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File " + path + " not found", e);
        }
        return 0;
    }

    public static int importMetiers(Context context, InputStream inputStream) {
        MetierRowModel metierRowModel = new MetierRowModel(CSV_SEPARATOR);
        int result = importData(context, metierRowModel, inputStream);
        DataCache.invalidateMetiers();
        return result;
    }


    public static int exportMetiers(Context context, String path) throws Exception {
        Preconditions.checkNotNull(path);
        MetierRowModel metierRowModel = new MetierRowModel(CSV_SEPARATOR);
        int result = exportData(context,
                                metierRowModel,
                                DataCache.getAllMetiers(context),
                                path,
                                R.string.preferences_export_metiers_default_name);
        return result;
    }

    public static int importPresentations(Context context, String path) {
        try {
            return importPresentations(context, new FileInputStream(path));
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File " + path + " not found", e);
        }
        return 0;
    }

    public static int importPresentations(Context context, InputStream inputStream) {
        PresentationRowModel presentationRowModel = new PresentationRowModel(CSV_SEPARATOR);
        int result = importData(context, presentationRowModel, inputStream);
        DataCache.invalidatePresentations();
        return result;
    }


    public static int exportPresentations(Context context, String path) throws Exception {
        Preconditions.checkNotNull(path);
        PresentationRowModel presentationRowModel = new PresentationRowModel(CSV_SEPARATOR);
        int result = exportData(context,
                                presentationRowModel,
                                DataCache.getAllPresentations(context),
                                path,
                                R.string.preferences_export_presentations_default_name);
        return result;
    }

    public static int importScientificSpecies(Context context, String path) {
        try {
            return importScientificSpecies(context, new FileInputStream(path));
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File " + path + " not found", e);
        }
        return 0;
    }

    public static int importScientificSpecies(Context context, InputStream inputStream) {
        ScientificSpeciesRowModel scientificSpeciesRowModel  = new ScientificSpeciesRowModel(CSV_SEPARATOR);
        int result = importData(context, scientificSpeciesRowModel, inputStream);
        DataCache.invalidateScientificSpecies();
        return result;
    }

    public static int exportScientificSpecies(Context context, String path) throws Exception {
        Preconditions.checkNotNull(path);
        ScientificSpeciesRowModel scientificSpeciesRowModel = new ScientificSpeciesRowModel(CSV_SEPARATOR);
        int result = exportData(context,
                                scientificSpeciesRowModel,
                                DataCache.getAllScientificSpecies(context),
                                path,
                                R.string.preferences_export_scientific_species_default_name);
        return result;
    }

    public static int importStates(Context context, String path) {
        try {
            return importStates(context, new FileInputStream(path));
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File " + path + " not found", e);
        }
        return 0;
    }

    public static int importStates(Context context, InputStream inputStream) {
        StateRowModel stateRowModel = new StateRowModel(CSV_SEPARATOR);
        int result = importData(context, stateRowModel, inputStream);
        DataCache.invalidateStates();
        return result;
    }

    public static int exportStates(Context context, String path) throws Exception {
        Preconditions.checkNotNull(path);
        StateRowModel stateRowModel = new StateRowModel(CSV_SEPARATOR);
        int result = exportData(context,
                                stateRowModel,
                                DataCache.getAllStates(context),
                                path,
                                R.string.preferences_export_states_default_name);
        return result;
    }

    public static int importVessels(Context context, String path) {
        try {
            return importVessels(context, new FileInputStream(path));
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File " + path + " not found", e);
        }
        return 0;
    }

    public static int importVessels(Context context, InputStream inputStream) {
        VesselRowModel vesselRowModel = new VesselRowModel(CSV_SEPARATOR);
        int result = importData(context, vesselRowModel, inputStream);
        DataCache.invalidateVessels();
        return result;
    }

    public static int exportVessels(Context context, String path) throws Exception {
        Preconditions.checkNotNull(path);
        VesselRowModel vesselRowModel = new VesselRowModel(CSV_SEPARATOR);
        int result = exportData(context,
                                vesselRowModel,
                                DataCache.getAllVessels(context),
                                path,
                                R.string.preferences_export_vessels_default_name);
        return result;
    }

    public static int importCalcifiedPartTakings(Context context, String path) throws Exception {
        try {
            return importCalcifiedPartTakings(context, new FileInputStream(path));
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File " + path + " not found", e);
        }
        return 0;
    }

    public static int importCalcifiedPartTakings(Context context, InputStream inputStream) {
        CalcifiedPartTakingRowModel calcifiedPartTakingRowModel = new CalcifiedPartTakingRowModel(CSV_SEPARATOR, DataCache.getAllScientificSpecies(context));
        Collection<CalcifiedPartTaking> data = importData(calcifiedPartTakingRowModel, inputStream);

        Multimap<String, Range<Integer>> ranges = HashMultimap.create();
        Map<String, Integer> sizeSteps = new HashMap<>();

        for (CalcifiedPartTaking taking : data) {
            // check if the species exists
            if (taking.getParent() == null) {
                throw new NullPointerException(context.getString(R.string.preferences_import_calcified_parts_takings_null_species));
            }

            String parentId = taking.getParentId();

            // check if the steps are the same for the species (at least one step must be defined for each species)
            Integer speciesSizeStep = sizeSteps.get(parentId);
            Integer sizeStep = taking.getSizeStep();
            if (speciesSizeStep != null && sizeStep != null && !speciesSizeStep.equals(sizeStep)) {
                throw new RuntimeException(context.getString(R.string.preferences_import_calcified_parts_different_size_steps));
            }
            if (sizeStep != null) {
                sizeSteps.put(parentId, sizeStep);
            }

            // check if the intervals are not overlapping
            Integer startSize = taking.getStartSize();
            if (startSize == null) {
                startSize = 0;
            }
            Integer endSize = taking.getEndSize();
            if (endSize == null) {
                endSize = Integer.MAX_VALUE;
            }
            Range<Integer> currentRange = Range.between(startSize, endSize);
            for (Range<Integer> range : ranges.get(parentId)) {
                if (range.isOverlappedBy(currentRange)) {
                    throw new RuntimeException(context.getString(R.string.preferences_import_calcified_parts_takings_overlapping));
                }
            }

            ranges.put(parentId, currentRange);
        }

        // check if every species has a size step
        if (sizeSteps.containsValue(null)) {
            throw new RuntimeException(context.getString(R.string.preferences_import_calcified_parts_no_size_step));
        }

        // check if there is no interval out of the ranges
        Set<Range<Integer>> rangesForSpecies = new TreeSet<>(new Comparator<Range<Integer>>() {

            @Override
            public int compare(Range<Integer> lhs, Range<Integer> rhs) {
                return ObjectUtils.compare(lhs.getMinimum(), rhs.getMinimum());
            }
        });
        for (String speciesId : ranges.keySet()) {
            int sizeStep = sizeSteps.get(speciesId);
            rangesForSpecies.clear();
            rangesForSpecies.addAll(ranges.get(speciesId));
            int n = 0;
            for (Range<Integer> range : rangesForSpecies) {
                if (range.getMinimum() - sizeStep > n) {
                    throw new RuntimeException(context.getString(R.string.preferences_import_calcified_parts_takings_value_out_of_ranges));
                }
                n = range.getMaximum();
            }
            if (n < Integer.MAX_VALUE) {
                throw new RuntimeException(context.getString(R.string.preferences_import_calcified_parts_takings_value_out_of_ranges));
            }
        }

        WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
        soh.saveData(data);
        soh.close();

        int result = data.size();
        return result;
    }

    public static int exportCalcifiedPartTakings(Context context, String path) throws Exception {
        Preconditions.checkNotNull(path);
        CalcifiedPartTakingRowModel calcifiedPartTakingRowModel = new CalcifiedPartTakingRowModel(CSV_SEPARATOR, DataCache.getAllScientificSpecies(context));
        WloSqlOpenHelper wloSqlOpenHelper = new WloSqlOpenHelper(context);
        int result = exportData(context,
                                calcifiedPartTakingRowModel,
                                WloSqlOpenHelper.transformCursorIntoCollection(wloSqlOpenHelper.getAllCalcifiedPartTakings(),
                                                                               new Function<Cursor, CalcifiedPartTaking>() {
                                                                                   @Override
                                                                                   public CalcifiedPartTaking apply(Cursor input) {
                                                                                       return new CalcifiedPartTaking(input);
                                                                                   }
                                                                               }),
                               path,
                               R.string.preferences_export_calcified_parts_takings_default_name);
        wloSqlOpenHelper.close();
        return result;
    }

    public static Collection<QualitativeValueModel> importQualitativeValues(CategoryModel categoryModel, String path) {
        try {
            return importQualitativeValues(categoryModel, new FileInputStream(path));
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File " + path + " not found", e);
        }
        return null;
    }

    public static Collection<QualitativeValueModel> importQualitativeValues(CategoryModel categoryModel, InputStream inputStream) {
        QualitativeValueRowModel qualitativeValueRowModel = new QualitativeValueRowModel(CSV_SEPARATOR, categoryModel);
        Collection<QualitativeValueModel> result = importData(qualitativeValueRowModel, inputStream);
        return result;
    }

    protected static <M extends BaseModel> int importData(Context context, ImportModel<M> importModel,
                                                          InputStream inputStream) {

        Collection<M> models = importData(importModel, inputStream);

        if (CollectionUtils.isNotEmpty(models)) {
            WloSqlOpenHelper soh = new WloSqlOpenHelper(context);
            soh.saveData(models);
            soh.close();
        }

        return models.size();
    }

    protected static <M extends BaseModel> Collection<M> importData(ImportModel<M> importModel, InputStream inputStream) {
        Import<M> importer = null;
        Collection<M> models = null;

        try {
            importer = Import.newImport(importModel, inputStream);
            models = Lists.newArrayList(importer.iterator());

        } catch (ClassCastException e) {
            // cf issue #4205
            // nuiton-i18n provocks an error when trying to get the error message
            Log.e(TAG, "error during import", e);
            throw e;

        } finally {
            if (importer != null) {
                importer.close();
            }
        }
        return models;
    }

    protected static <M extends BaseModel> int exportData(Context context,
                                                          ExportModel<M> exportModel,
                                                          Collection<M> data,
                                                          String path,
                                                          int defaultFileName) throws Exception {

        int result = 0;

        File targetFile = new File(path);
        if (targetFile.isDirectory()) {
            String fileName = context.getString(defaultFileName)
                    + String.format("-%1$tY%1$tm%1$td-%1$tH%1$tM%1$tS", new Date()) + ".csv";
            targetFile = new File(targetFile, fileName);
        }

        try {
            Export.exportToFile(exportModel, data, targetFile);
            result = data.size();

        } catch (Exception e) {
            // cf issue #4205
            // nuiton-i18n provocks an error when trying to get the error message
            Log.e(TAG, "error during import", e);
            throw e;
        }
        return result;
    }
}

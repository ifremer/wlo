package fr.ifremer.wlo.utils;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.TextView;
import com.google.common.collect.Maps;
import fr.ifremer.wlo.R;
import fr.ifremer.wlo.models.BaseModel;
import fr.ifremer.wlo.storage.DataCache;

import java.util.Date;
import java.util.Map;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class WloItemListViewBinder implements SimpleCursorAdapter.ViewBinder {

    private static final String TAG = "WloItemListViewBinder";

    public enum DataType {
        DATE, DATETIME, LOCATION, METIER,
        MENSURATION, STATE, PRESENTATION,
        COMMERCIAL_SPECIES, SCIENTIFIC_SPECIES
    }

    protected Map<Integer, DataType> dataTypes = Maps.newHashMap();
    protected String dateFormat;
    protected String dateTimeFormat;
    protected Context context;

    public WloItemListViewBinder(Context context) {
        this(context, null);
    }

    public WloItemListViewBinder(Context context, Map<Integer, DataType> dataTypes) {
        this.context = context;
        dateFormat = UIUtils.getDateFormat(context);
        dateTimeFormat = dateFormat + " " + context.getString(R.string.time_format);
        if (dataTypes != null) {
            this.dataTypes = dataTypes;
        }
    }

    @Override
    public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
        DataType dataType = dataTypes.get(columnIndex);

        TextView textView = (TextView) view;
        if (dataType != null) {
            if (cursor.getType(columnIndex) != Cursor.FIELD_TYPE_NULL) {
                if (DataType.DATE.equals(dataType) || DataType.DATETIME.equals(dataType)) {
                    long time = cursor.getLong(columnIndex);
                    Date date = new Date(time);
                    String format = DataType.DATE.equals(dataType) ? dateFormat : dateTimeFormat;
                    String formattedDate = String.format(format, date);
                    textView.setText(formattedDate);
                    return true;
                }

                Context context = view.getContext();
                String id = cursor.getString(columnIndex);
                BaseModel ref = null;
                switch (dataType) {
                    case LOCATION:
                        ref = DataCache.getLocationById(context, id);
                        break;
                    case METIER:
                        ref = DataCache.getMetierById(context, id);
                        break;
                    case COMMERCIAL_SPECIES:
                        ref = DataCache.getCommercialSpeciesById(context, id);
                        break;
                    case MENSURATION:
                        ref = DataCache.getMensurationById(context, id);
                        break;
                    case STATE:
                        ref = DataCache.getStateById(context, id);
                        break;
                    case PRESENTATION:
                        ref = DataCache.getPresentationById(context, id);
                        break;
                    case SCIENTIFIC_SPECIES:
                        ref = DataCache.getScientificSpeciesById(context, id);
                        break;
                }
                textView.setText(ref != null ? ref.toString(context): "");
                return true;
            }
            if (DataType.SCIENTIFIC_SPECIES.equals(dataType)
                    || DataType.METIER.equals(dataType)) {
                textView.setText(
                        context.getString(R.string.undefined));
                return true;
            }
        }

        return false;
    }
}

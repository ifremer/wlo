package fr.ifremer.wlo.utils;

/*
 * #%L
 * WLO
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.google.common.base.Function;
import fr.ifremer.wlo.models.BaseModel;

import java.util.List;

/**
 * @author kmorin <kmorin@codelutin.com>
 * @since 0.1
 */
public class BaseModelArrayAdapter<M extends BaseModel> extends ArrayAdapter<M> {

    /**
     * The resource indicating what views to inflate to display the content of this
     * array adapter.
     */
    protected int mResource;

    /**
     * If the inflated resource is not a TextView, {@link #mFieldId} is used to find
     * a TextView inside the inflated views hierarchy. This field must contain the
     * identifier that matches the one defined in the resource file.
     */
    protected int mFieldId = 0;

    protected LayoutInflater mInflater;

    /**
     * Function to transform object into String
     */
    protected Function<M, String> toStringFunction;

    public BaseModelArrayAdapter(Context context, int textViewResourceId, Function<M, String> toStringFunction) {
        super(context, textViewResourceId);
        init(textViewResourceId, 0, toStringFunction);
    }

    public BaseModelArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        init(textViewResourceId, 0, null);
    }

    public BaseModelArrayAdapter(Context context, int resource, int textViewResourceId, Function<M, String> toStringFunction) {
        super(context, resource, textViewResourceId);
        init(resource, textViewResourceId, toStringFunction);
    }

    public BaseModelArrayAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
        init(resource, textViewResourceId, null);
    }

    public BaseModelArrayAdapter(Context context, int textViewResourceId, M[] objects, Function<M, String> toStringFunction) {
        super(context, textViewResourceId, objects);
        init(textViewResourceId, 0, toStringFunction);
    }

    public BaseModelArrayAdapter(Context context, int textViewResourceId, M[] objects) {
        super(context, textViewResourceId, objects);
        init(textViewResourceId, 0, null);
    }

    public BaseModelArrayAdapter(Context context, int resource, int textViewResourceId, M[] objects, Function<M, String> toStringFunction) {
        super(context, resource, textViewResourceId, objects);
        init(resource, textViewResourceId, toStringFunction);
    }

    public BaseModelArrayAdapter(Context context, int resource, int textViewResourceId, M[] objects) {
        super(context, resource, textViewResourceId, objects);
        init(resource, textViewResourceId, null);
    }

    public BaseModelArrayAdapter(Context context, int textViewResourceId, List<M> objects, Function<M, String> toStringFunction) {
        super(context, textViewResourceId, objects);
        init(textViewResourceId, 0, toStringFunction);
    }

    public BaseModelArrayAdapter(Context context, int textViewResourceId, List<M> objects) {
        super(context, textViewResourceId, objects);
        init(textViewResourceId, 0, null);
    }

    public BaseModelArrayAdapter(Context context, int resource, int textViewResourceId, List<M> objects, Function<M, String> toStringFunction) {
        super(context, resource, textViewResourceId, objects);
        init(resource, textViewResourceId, toStringFunction);
    }

    public BaseModelArrayAdapter(Context context, int resource, int textViewResourceId, List<M> objects) {
        super(context, resource, textViewResourceId, objects);
        init(resource, textViewResourceId, null);
    }

    protected void init(int resource, int textViewResourceId, Function<M, String> toStringFunction) {
        mInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mResource = resource;
        mFieldId = textViewResourceId;
        this.toStringFunction = toStringFunction;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return createViewFromResource(position, convertView, parent, mResource);
    }

    private View createViewFromResource(int position, View convertView, ViewGroup parent,
                                        int resource) {
        View view;
        TextView text;

        if (convertView == null) {
            view = mInflater.inflate(resource, null);
        } else {
            view = convertView;
        }

        try {
            if (mFieldId == 0) {
                // If no custom field is assigned, assume the whole resource is a TextView
                text = (TextView) view;
            } else {
                // Otherwise, find the TextView field within the layout
                text = (TextView) view.findViewById(mFieldId);
            }
        } catch (ClassCastException e) {
            Log.e("BaseModelArrayAdapter", "You must supply a resource ID for a TextView");
            throw new IllegalStateException(
                    "BaseModelArrayAdapter requires the resource ID to be a TextView", e);
        }

        M item = getItem(position);
        String s;
        if (item == null) {
            s = "";
        } else if (toStringFunction != null) {
            s = toStringFunction.apply(item);
        } else {
            s = item.toString(getContext());
        }
        text.setText(s);

        return view;
    }
}

Faire une nouvelle version
--------------------------

Fournir la version de la base à utiliser, renseigner la propriété *dbVersion*

mvn release:prepare -Darguments="-DdbVersion=2013.06.04"
mvn release:perform -Darguments="-DdbVersion=2013.06.04"

Pour préparer une release complête, lancer la commande :

 mvn release:prepare -Darguments="-DperformFullRelease -DdbVersion=2013.06.04"
 mvn release:perform -Darguments="-DperformFullRelease -DdbVersion=2013.06.04"